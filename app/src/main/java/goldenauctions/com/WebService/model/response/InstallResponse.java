package goldenauctions.com.WebService.model.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import goldenauctions.com.Medoles.Install.InstallClass;


public class InstallResponse extends RootResponse {
    @SerializedName("data")
    @Expose
    public InstallClass data ;


}
