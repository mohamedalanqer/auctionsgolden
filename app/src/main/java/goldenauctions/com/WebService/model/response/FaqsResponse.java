package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Faq;
import goldenauctions.com.Medoles.FaqsCategories;
import goldenauctions.com.Medoles.Paging;


public class FaqsResponse extends RootResponse{
    @SerializedName("FaqsCategories")
    List<FaqsCategories> faqsCategories  ;
    public Paging paging ;


    public List<FaqsCategories> getFaqsCategories() {
        return faqsCategories;
    }

    public void setFaqsCategories(List<FaqsCategories> faqsCategories) {
        this.faqsCategories = faqsCategories;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
