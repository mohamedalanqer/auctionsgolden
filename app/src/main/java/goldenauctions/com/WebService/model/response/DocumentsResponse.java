package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Document;
import goldenauctions.com.Medoles.Paging;


public class DocumentsResponse extends RootResponse{
    @SerializedName("Documents")
    List<Document> documents  ;
    public Paging paging ;

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
