package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Paging;


public class AuctionResponse extends RootResponse{
    @SerializedName("Auction")
    public Auction auction  ;

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }
}
