package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import goldenauctions.com.Medoles.Ticket;


public class TicketResponse extends RootResponse{
    @SerializedName("Ticket")
    Ticket ticket  ;

    public Ticket getTicket() {
        return ticket;
    }

    public void setTickets(Ticket ticket) {
        this.ticket = ticket;
    }
}
