package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Paging;
import goldenauctions.com.Medoles.Ticket;


public class TicketsResponse extends RootResponse{
    @SerializedName("Tickets")
    List<Ticket> Tickets  ;
    public Paging paging ;

    public List<Ticket> getTickets() {
        return Tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        Tickets = tickets;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
