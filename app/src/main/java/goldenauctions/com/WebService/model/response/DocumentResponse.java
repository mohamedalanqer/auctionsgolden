package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Document;
import goldenauctions.com.Medoles.Paging;


public class DocumentResponse extends RootResponse{
    @SerializedName("data")
    Document document ;

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
