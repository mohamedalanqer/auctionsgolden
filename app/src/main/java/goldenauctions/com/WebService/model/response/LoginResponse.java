package goldenauctions.com.WebService.model.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import goldenauctions.com.Medoles.Install.User;


public class LoginResponse extends RootResponse {
    @SerializedName("User")
    @Expose
    public User user ;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
