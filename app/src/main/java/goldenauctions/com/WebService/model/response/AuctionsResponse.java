package goldenauctions.com.WebService.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Notification;
import goldenauctions.com.Medoles.Paging;


public class AuctionsResponse extends RootResponse{
    @SerializedName("Auctions")
    List<Auction> Auctions  ;
    public Paging paging ;

    public List<Auction> getAuctions() {
        return Auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        Auctions = auctions;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
