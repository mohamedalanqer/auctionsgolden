package goldenauctions.com.WebService.model.response;


import com.google.gson.annotations.SerializedName;

import goldenauctions.com.Medoles.Install.Status;

public class RootResponse     {
    @SerializedName("status")
    private Status status;
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

}
