package goldenauctions.com.WebService;


import java.util.HashMap;
import java.util.List;

import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import goldenauctions.com.WebService.model.response.DocumentResponse;
import goldenauctions.com.WebService.model.response.DocumentsResponse;
import goldenauctions.com.WebService.model.response.FaqsResponse;
import goldenauctions.com.WebService.model.response.InstallResponse;
import goldenauctions.com.WebService.model.response.LoginResponse;
import goldenauctions.com.WebService.model.response.NotificationsResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import goldenauctions.com.WebService.model.response.TicketResponse;
import goldenauctions.com.WebService.model.response.TicketsResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("install")
    Call<InstallResponse> GetInstall();

    @POST("auth/login")
    Call<LoginResponse> createLogin(@Body User user);


    @POST("auth/signup")
    Call<LoginResponse> createRegistration(@Body User user);

    @Multipart
    @POST("auth/signup")
    Call<LoginResponse> createRegistration(@PartMap HashMap<String, RequestBody> map, @Part MultipartBody.Part file);


    @Multipart
    @POST("auth/update")
    Call<LoginResponse> UpdateAvatar(@Part MultipartBody.Part file);


    @POST("auth/update")
    Call<LoginResponse> editProfile(@Body User user);

    @FormUrlEncoded
    @POST("auth/update")
    Call<LoginResponse> editLanguageProfile(@Field("app_locale") String lang);

    @Multipart
    @POST("auth/update")
    Call<LoginResponse> editProfile(@Part MultipartBody.Part file);


    @Multipart
    @POST("auth/update")
    Call<LoginResponse> editProfile(@PartMap HashMap<String, RequestBody> map);


    @Multipart
    @POST("auth/update")
    Call<LoginResponse> editProfile(@PartMap HashMap<String, RequestBody> map, @Part MultipartBody.Part file);


    @FormUrlEncoded
    @POST("auth/update")
    Call<LoginResponse> editProfile(@Field("is_notifiable") String is_notifiable);

    @FormUrlEncoded
    @POST("auth/forget_password")
    Call<RootResponse> ForgetPassword(@Field("email") String phone);

    @FormUrlEncoded
    @POST("auth/reset_password")
    Call<RootResponse> ResetPassword(@Field("email") String phone,
                                     @Field("password") String password,
                                     @Field("password_confirmation") String password_confirmation,
                                     @Field("code") String code);


    @POST("auth/logout")
    Call<LoginResponse> logout();


    @FormUrlEncoded
    @POST("auth/change_password")
    Call<RootResponse> UpdateUserPassword(@Field("old_password") String old_password,
                                          @Field("password") String password,
                                          @Field("password_confirmation") String password_confirmation);

    @GET("auth/me")
    Call<LoginResponse> GetProfile();


    @GET("auctions")
    Call<AuctionsResponse> GetAuctions(@Query("page") int page,
                                       @Query("per_page") int per_page);
    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsAdvertisement(@Query("is_advertisement") boolean is_advertisement ,@Query("page") int page,
    @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByCategory(@Query("category_id") int category_id, @Query("page") int page,
                                                 @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByCode(@Query("code") String code, @Query("page") int page,
                                                 @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByPrice(@Query("price_from") String price_from,
                                              @Query("price_to") String price_to,
                                              @Query("page") int page,
                                             @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByDate(@Query("end_at_from") String end_at_from,
                                              @Query("end_at_to") String end_at_to,
                                              @Query("page") int page,
                                              @Query("per_page") int per_page);


    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByCode(@Query("category_id") int category_id,
                                             @Query("code") String code, @Query("page") int page,
                                             @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByPrice(@Query("category_id") int category_id,
                                              @Query("price_from") String price_from,
                                              @Query("price_to") String price_to,
                                              @Query("page") int page,
                                              @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetAuctionsByDate(@Query("category_id") int category_id,
                                             @Query("end_at_from") String end_at_from,
                                             @Query("end_at_to") String end_at_to,
                                             @Query("page") int page,
                                             @Query("per_page") int per_page);


    @GET("auctions/show")
    Call<AuctionResponse> GetAuctionById(@Query("auction_id") int auction_id);


    @FormUrlEncoded
    @POST("auctions/add_bid")
    Call<AuctionResponse> AddNewBid(@Field("auction_id") int auction_id, @Field("price") String price);

    @FormUrlEncoded
    @POST("auctions/toggle_favourite")
    Call<AuctionResponse> FavouriteAuctionsToggle(@Field("auction_id") int auction_id);


    @GET("auctions/favourites")
    Call<AuctionsResponse> GetFollowUpAuctions(@Query("page") int page,
                                               @Query("per_page") int per_page);


    @GET("auctions")
    Call<AuctionsResponse> GetMyPurchases(@Query("my_purchases") boolean my_purchases, @Query("page") int page,
                                          @Query("per_page") int per_page);

    @GET("auctions")
    Call<AuctionsResponse> GetMyBids(@Query("my_bids") boolean my_bids, @Query("page") int page,
                                     @Query("per_page") int per_page);

    @GET("home/documents")
    Call<DocumentsResponse> GetMyDocuments(@Query("page") int page,
                                           @Query("per_page") int per_page);


    @Multipart
    @POST("home/upload_document")
    Call<RootResponse> UploadNewDocument(@Part("expiry_date") String expiry_date,
                                         @Part("document_type_id") int document_type_id,
                                         @Part MultipartBody.Part front_face,
                                         @Part MultipartBody.Part back_face);


    @GET("notifications")
    Call<NotificationsResponse> GetNotifications(@Query("page") int page,
                                                 @Query("per_page") int per_page);

    @POST("notifications/read/all")
    Call<RootResponse> ReadAllNotifications();


    @FormUrlEncoded
    @POST("auth/verify")
    Call<LoginResponse> Verify(@Field("type") int type, @Field("code") String code);

    @GET("auth/resend_verify")
    Call<RootResponse> ResendVerify(@Query("type") int type);

    @FormUrlEncoded
    @POST("orders/send_notification")
    Call<RootResponse> SendNotification(@Field("order_id") String code, @Field("message") String message);


    @Multipart
    @POST("orders/store")
    Call<RootResponse> CreateOrder(@PartMap HashMap<String, RequestBody> map);


    @GET("home/faqs")
    Call<FaqsResponse> GetMyFaqs();

    @FormUrlEncoded
    @POST("home/subscribe")
    Call<RootResponse> CreateSubscriptions(@Field("subscription_id") int subscription_id, @Field("payment_method") int payment_type);


    @Multipart
    @POST("home/subscribe")
    Call<RootResponse> CreateSubscriptions(@Part("subscription_id") int subscription_id, @Part("payment_method") int payment_type, @Part MultipartBody.Part file);


    @POST("transactions/request_refund")
    Call<RootResponse> RequestRefund();




    @GET("tickets")
    Call<TicketsResponse> GetListTickets(@Query("page") int page,
                                         @Query("per_page") int per_page);


    @GET("tickets/show")
    Call<TicketResponse> GetShowTicket(@Query("ticket_id") int ticket_id);

    @FormUrlEncoded
    @POST("tickets/store")
    Call<RootResponse> AddTickets(@Field("title") String title, @Field("message") String message);


    @FormUrlEncoded
    @POST("tickets/response")
    Call<RootResponse> AddResponseTickets(@Field("ticket_id") int title, @Field("response") String message);


}