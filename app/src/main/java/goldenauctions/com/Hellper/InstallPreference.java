package goldenauctions.com.Hellper;

import android.content.Context;

import com.google.gson.Gson;

import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Medoles.Install.InstallClass;


public class InstallPreference {


    public static InstallClass GetInstall(Context context) {
        String jsoninstall = AppPreferences.getString(context, "install");
        Gson gson = new Gson();
        InstallClass installClass = gson.fromJson(jsoninstall, InstallClass.class);
        return installClass;
    }


}
