package goldenauctions.com.Hellper;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import goldenauctions.com.R;

public class DrawableRound  {
    public static void TextToDrawableRound(Context context , ImageView image, String Value  ) {
    int dptoPx = dpToPx(18); //110
        if(TextUtils.equals("0",Value)){
            image.setVisibility(View.GONE);
        }else {
            image.setVisibility(View.VISIBLE);
            TextDrawable.IBuilder iBuilder = TextDrawable.builder().beginConfig()
                    .width(dptoPx)
                    .height(dptoPx).bold().endConfig().round();
            TextDrawable drawable = iBuilder.build(Value, context.getResources().getColor(R.color.colorPrimary));
            image.setImageDrawable(drawable);
            YoYo.with(Techniques.ZoomIn).duration(400).playOn(image);
        }
    }

    public static void TextToDrawableRoundRed(Context context , ImageView image, String Value  ) {
        int dptoPx = dpToPx(18); //110
        if(TextUtils.equals("0",Value)){
            image.setVisibility(View.GONE);
        }else {
            image.setVisibility(View.VISIBLE);
            TextDrawable.IBuilder iBuilder = TextDrawable.builder().beginConfig()
                    .width(dptoPx)
                    .height(dptoPx).bold().endConfig().round();
            TextDrawable drawable = iBuilder.build(Value, context.getResources().getColor(R.color.colorRed));
            image.setImageDrawable(drawable);
            YoYo.with(Techniques.ZoomIn).duration(400).playOn(image);
        }
    }

    public static void TextToDrawableRoundGrey(Context context , ImageView image, String Value, TextView text_dot) {
      /**  int dptoPx = dpToPx(18); //110
        image.setVisibility(View.VISIBLE);
        if(TextUtils.equals("0",Value)){
            image.setVisibility(View.GONE);
            text_dot.setVisibility(View.VISIBLE);
        }else {
            image.setVisibility(View.VISIBLE);
            text_dot.setVisibility(View.GONE);
            TextDrawable.IBuilder iBuilder = TextDrawable.builder().beginConfig()
                    .width(dptoPx)
                    .height(dptoPx).withBorder(1).textColor(context.getResources().getColor(R.color.colorWhite)).bold().endConfig().round();
            TextDrawable drawable = iBuilder.build(Value, context.getResources().getColor(R.color.color_btn_toolbar));
            image.setImageDrawable(drawable);
            YoYo.with(Techniques.ZoomIn).duration(400).playOn(image);
        }*/
    }
    public static int dpToPx(int dp) {
        Log.e("pxToDp", "dp" + dp + " == " + (dp * Resources.getSystem().getDisplayMetrics().density));
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }



}
