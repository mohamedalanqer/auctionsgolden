package goldenauctions.com.Hellper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.Gson;

import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.GuideActivity;
import goldenauctions.com.Ui.Activities.LoginRegisterActivity;
import goldenauctions.com.Ui.Activities.HomeActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;

import static goldenauctions.com.Manager.RootManager.KeyGuide;


public class PreferenceUser {


    public static User User(Context context) {
        User user = new User();
        Gson gson = new Gson();
        String json = AppPreferences.getString(context, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }


    public static void GoToActivityMain(Activity activity, User user) {

        boolean isShowGuide = AppPreferences.getBoolean(activity, KeyGuide, false);
        if (user != null) {
            if (isShowGuide) {

                if (user.getEmail_verified_at() == null) {
                    Intent intent = new Intent(activity, UserActionActivity.class);
                    intent.putExtra(RootManager.ActionIntent, activity.getResources().getString(R.string.verifyAccount));
                    activity.startActivity(intent);
                    activity.finish();
                } /**else if (user.getMobile_verified_at() == null) {
                    Intent intent = new Intent(activity, UserActionActivity.class);
                    intent.putExtra(RootManager.ActionIntent, activity.getResources().getString(R.string.VerifyMobile));
                    activity.startActivity(intent);
                    activity.finish();
                } */else {
                    Intent intent = new Intent(activity, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                    activity.finish();


                }

            } else {
                Intent intent = new Intent(activity, GuideActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
            }
        } else {
            if (isShowGuide) {
                Intent intent = new Intent(activity.getApplicationContext(), LoginRegisterActivity.class);
                activity.startActivity(intent);
                activity.finish();
            } else {
                Intent intent = new Intent(activity, GuideActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
            }
        }

    }

    public static void GoToActivityMainSKip(Activity activity, User user) {

        boolean isShowGuide = AppPreferences.getBoolean(activity, KeyGuide, false);
        if (user != null) {
            if (isShowGuide) {
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();


            } else {
                Intent intent = new Intent(activity, GuideActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
            }
        } else {
            if (isShowGuide) {
                Intent intent = new Intent(activity.getApplicationContext(), LoginRegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
            } else {
                Intent intent = new Intent(activity, GuideActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
            }
        }

    }

}
