package goldenauctions.com.Hellper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import java.io.ByteArrayOutputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import goldenauctions.com.R;

import static android.content.Context.WINDOW_SERVICE;

public class UtilitiesDate {

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate(long milliSeconds) {
        String dateFormat = "dd MMM yyyy HH:mm:ss aa";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Date getDateChat(long milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return calendar.getTime();
    }

    public static String getDate(String fullDate, Locale locale) {
        String dateFormatFrom = "dd/MM/yyyy HH:mm:ss aa";
        SimpleDateFormat fromformatter = new SimpleDateFormat(dateFormatFrom, locale);
        String newDateString = "";
        String dateFormatTo = "dd MMM yyyy";
        SimpleDateFormat formatterTo = new SimpleDateFormat(dateFormatTo);

        try {
            Date date = fromformatter.parse(fullDate);
            newDateString = formatterTo.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDateString;
    }

    public static long convertJsonDate(String jsonDate) {
        Pattern pattern = Pattern
                .compile("^\\/date\\((-?\\d++)(?:([+-])(\\d{2})(\\d{2}))?\\)\\/$", Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(jsonDate);
        if (m.find())
            return Long.parseLong(m.group(1));
        return System.currentTimeMillis();
    }


    public static String convertNumberToMonthText(int month) {
        return new DateFormatSymbols().getShortMonths()[month - 1];
    }

    public static String trimAfterLastOccurrenceOfSymbol(String text, String symbol) {
        String newstr = text;
        if (null != text && text.length() > 0) {
            int endIndex = text.lastIndexOf(symbol);
            if (endIndex != -1) {
                newstr = text.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
            }
        }
        return newstr;
    }

    public static List<Integer> convertHexColorsToIntColors(List<String> colorsHex) {
        List<Integer> colorsInt = new ArrayList<>();

        for (String colorHex : colorsHex) {
            colorsInt.add(Color.parseColor(colorHex));
        }
        return colorsInt;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static String covertImageToBase64(Bitmap imageBitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

    }

    public static int getScreenWidthInDPs(Context context) {
        DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int widthInDP = Math.round(dm.widthPixels / dm.density);
        return widthInDP;
    }

    public static int getScreenHeightInDPs(Context context) {
        DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int heightInDP = Math.round(dm.heightPixels / dm.density);
        return heightInDP;
    }

    public static float convertPxToDp(float px) {
        DisplayMetrics displaymetrics = Resources.getSystem().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, displaymetrics);

    }

    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static boolean isArabic() {
        return Locale.getDefault().getLanguage().equalsIgnoreCase("ar");
    }


    public static String getDurationString(int seconds) {
        Date date = new Date(seconds * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat(seconds >= 3600 ? "HH:mm:ss" : "mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

    public static boolean IsStartAuction(Context context, String starttime, TextView textView, View view) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);
        Log.e("afterTime"," After "+ starttime);
        starttime = convertTime(starttime);
        Log.e("afterTime"," Pefor "+ starttime);
        final Long[] startTime = {0L};
        long milliseconds = 0;


        Date endDate;
        try {
            endDate = formatter.parse(starttime);
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // startTime[0] = System.currentTimeMillis();
        startTime[0]    = UtilitiesDate.getTimeMillsByZoneDubai();
        textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        final boolean[] Stop = {false};
        CountDownTimer mCountDownTimer = null;
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }

        Long serverUptimeSeconds = (milliseconds - startTime[0]) / 1000;
        textView.setTextColor(context.getResources().getColor(R.color.colorBlack));
        textView.setText(starttime + "");

        if (serverUptimeSeconds > 0) {
            String finalStarttime = starttime;
            mCountDownTimer = new CountDownTimer(milliseconds, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    startTime[0] = startTime[0] - 1;
                    Long serverUptimeSeconds =
                            (millisUntilFinished - startTime[0]) / 1000;

                    if (Stop[0] == true) {
                        textView.setText(R.string.StartNow);

                        return;
                    }
                    if (serverUptimeSeconds < 0)
                        Stop[0] = true;

                    textView.setTextColor(context.getResources().getColor(R.color.colorBlack));
                    textView.setText(finalStarttime + "");
                }

                @Override
                public void onFinish() {
                }
            };
            //  mCountDownTimer.start();
            return false;
        } else {

            return true;
        }
    }
    public static boolean IsStartAuctionOut(Context context, String starttime, TextView textView,CountDownTimer mCountDownTimer  ) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);
        Log.e("afterTime"," After "+ starttime);
        starttime = convertTime(starttime);
        Log.e("afterTime"," Pefor "+ starttime);
        final Long[] startTime = {0L};
        long milliseconds = 0;


        Date endDate;
        try {
            endDate = formatter.parse(starttime);
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // startTime[0] = System.currentTimeMillis();
        startTime[0]    = UtilitiesDate.getTimeMillsByZoneDubai();
        textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        final boolean[] Stop = {false};

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }

        Long serverUptimeSeconds = (milliseconds - startTime[0]) / 1000;
        textView.setTextColor(context.getResources().getColor(R.color.colorBlack));
        textView.setText(starttime + "");

        if (serverUptimeSeconds > 0) {
            String finalStarttime = starttime;
            mCountDownTimer = new CountDownTimer(milliseconds, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    startTime[0] = startTime[0] - 1;
                    Long serverUptimeSeconds =
                            (millisUntilFinished - startTime[0]) / 1000;

                    if (Stop[0] == true) {
                        textView.setText(R.string.StartNow);

                        return;
                    }
                    if (serverUptimeSeconds < 0)
                        Stop[0] = true;

                    textView.setTextColor(context.getResources().getColor(R.color.colorBlack));
                    textView.setText(finalStarttime + "");
                }

                @Override
                public void onFinish() {
                }
            };
            mCountDownTimer.start();
            return false;
        } else {

            return true;
        }
    }

    public static void StartCountdownTimer(Context context, String endTime, TextView textView, View view, boolean IsCountdown) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);

        final Long[] startTime = {0L};
        long milliseconds = 0;
        endTime =  convertTime(endTime);


        Date endDate;
        try {
            endDate = formatter.parse(endTime);
            textView.setText(endDate.toString());
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

       // startTime[0] = System.currentTimeMillis();
        startTime[0]    = UtilitiesDate.getTimeMillsByZoneDubai();

        textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        view.setEnabled(true);
        if (IsCountdown) {
            final boolean[] Stop = {false};
            CountDownTimer mCountDownTimer = null;
            if (mCountDownTimer != null) {
                mCountDownTimer.cancel();
            }
            mCountDownTimer = new CountDownTimer(milliseconds, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    startTime[0] = startTime[0] - 1;
                    Long serverUptimeSeconds =
                            (millisUntilFinished - startTime[0]) / 1000;


                    String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
                    //txtViewDays.setText(daysLeft);
                    Log.d("daysLeft", daysLeft);
                    if (!TextUtils.isEmpty(daysLeft)) {
                        daysLeft = daysLeft + "" + context.getResources().getString(R.string.Day) + " ";
                    }

                    String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);
                    //txtViewHours.setText(hoursLeft);
                    Log.d("hoursLeft", hoursLeft);
                    if (!TextUtils.isEmpty(hoursLeft)) {
                        hoursLeft = hoursLeft + "" + context.getResources().getString(R.string.Hours) + " ";
                    }

                    String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
                    //txtViewMinutes.setText(minutesLeft);
                    Log.d("minutesLeft", minutesLeft);
                    if (!TextUtils.isEmpty(minutesLeft)) {
                        minutesLeft = minutesLeft + "" + context.getResources().getString(R.string.Minutes) + " ";
                    }

                    String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
                    //txtViewSecond.setText(secondsLeft);
                    Log.d("secondsLeft", secondsLeft);
                    if (!TextUtils.isEmpty(secondsLeft)) {
                        secondsLeft = secondsLeft + "" + context.getResources().getString(R.string.seconds) + " ";

                    }
                    if (Stop[0] == true) {
                        textView.setTextColor(context.getResources().getColor(R.color.colorRed));
                        //  view.setEnabled(false);
                        return;
                    }
                    if (serverUptimeSeconds < 0)
                        Stop[0] = true;

                    textView.setText(daysLeft + hoursLeft + minutesLeft + secondsLeft);


                }

                @Override
                public void onFinish() {
                }
            };
            mCountDownTimer.start();
        } else {
            Long serverUptimeSeconds =
                    (milliseconds - startTime[0]) / 1000;


            String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
            if (!TextUtils.isEmpty(daysLeft)) {
                daysLeft = daysLeft + "" + context.getResources().getString(R.string.Day) + " ";
            }

            String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);
            if (!TextUtils.isEmpty(hoursLeft)) {
                hoursLeft = hoursLeft + "" + context.getResources().getString(R.string.Hours) + " ";
            }

            String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
            if (!TextUtils.isEmpty(minutesLeft)) {
                minutesLeft = minutesLeft + "" + context.getResources().getString(R.string.Minutes) + " ";
            }

            String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
            if (!TextUtils.isEmpty(secondsLeft)) {
                secondsLeft = secondsLeft + "" + context.getResources().getString(R.string.seconds) + " ";

            }
            if (serverUptimeSeconds < 0) {
                textView.setTextColor(context.getResources().getColor(R.color.colorRed));
                textView.setText(endTime + "");
            } else {
                textView.setText(daysLeft + hoursLeft + minutesLeft + secondsLeft);
            }
        }

    }


    public static boolean IsEndAuctionCheck(String endTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);
        endTime =  convertTime(endTime);

        final Long[] startTime = {0L};
        long milliseconds = 0;


        Date endDate;
        try {
            endDate = formatter.parse(endTime);
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

      //  startTime[0] = System.currentTimeMillis();
        startTime[0]    = UtilitiesDate.getTimeMillsByZoneDubai();
        Long serverUptimeSeconds =
                (milliseconds - startTime[0]) / 1000;

        if (serverUptimeSeconds < 0) {
            return true;
        } else {
            return false;
        }


    }
    public static boolean IsStartAuctionCheck(String starttime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);
        starttime =  convertTime(starttime);

        final Long[] startTime = {0L};
        long milliseconds = 0;


        Date endDate;
        try {
            endDate = formatter.parse(starttime);
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

       // startTime[0] = System.currentTimeMillis();
      //  startTime[0]    = UtilitiesDate.getTimeMillsByZoneDubai();
        startTime[0]    = UtilitiesDate.getTimeMillsByZoneDubai();
        final boolean[] Stop = {false};
        CountDownTimer mCountDownTimer = null;
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }

        Long serverUptimeSeconds = (milliseconds - startTime[0]) / 1000;


        if (serverUptimeSeconds > 0) {
            return false;
        } else {

            return true;
        }
    }


    @SuppressLint("NewApi")
    public static String convertTime(String inputTime) {

       /** DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String ddd = TimeZone.getDefault().getID();
        ZoneId destinationTimeZone = ZoneId.of(ddd);
        // String ddd = TimeZone.getDefault().getID() ;
          Instant now = Instant.now();
         ZoneId zoneId_Montreal = ZoneId.of( "Asia/Dubai" );
         ZonedDateTime zdt_Montreal = ZonedDateTime.ofInstant( now , zoneId_Montreal );
         Log.e("ddddd", zdt_Montreal + " Asia/Dubai");
         ZoneId zoneId_Paris = ZoneId.of( TimeZone.getDefault().getID() ); // Or "Asia/Kolkata", etc.
         ZonedDateTime zdt_Paris = zdt_Montreal.withZoneSameInstant( zoneId_Paris );
         Instant instant = zdt_Paris.toInstant();
         Log.e("ddddd", instant  + " instant");
        String format = LocalDateTime.parse(inputTime, formatter)
                .atOffset(ZoneOffset.UTC)
                .atZoneSameInstant(destinationTimeZone)
                .format(formatter);

        Log.e("ddddd", format  + " format");*/
        return  conff(inputTime);

    }


    @SuppressLint("NewApi")
    public static long getTimeMillsByZoneDubai(){

     /**   LocalDateTime ldt = LocalDateTime.now();
        ZonedDateTime zonedDateTime =ldt.atZone(ZoneId.of("Asia/Dubai"));
          long  lg=  zonedDateTime.toInstant().toEpochMilli();*/
        long  lg =System.currentTimeMillis();
     //   Log.e("timeee", System.currentTimeMillis() + " "+ lg ) ;
        return lg ;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String conff(String timeStr){
        String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        String dateInString = timeStr ;
        LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));
        ZoneId singaporeZoneId = ZoneId.of("Asia/Riyadh");
        ZonedDateTime zdt = ZonedDateTime.of(ldt, singaporeZoneId);


        System.out.println("TimeZone : " + singaporeZoneId);
        ZonedDateTime asiaZonedDateTime =  zdt ;// ldt.atZone(singaporeZoneId);
        System.out.println("Date (Singapore) : " + asiaZonedDateTime);

        DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);



        String ddd = TimeZone.getDefault().getID();
        ZoneId destinationTimeZone = ZoneId.of(ddd);
        ZonedDateTime MyDateTime = asiaZonedDateTime.withZoneSameInstant(destinationTimeZone);

        String MyTimeZoneStr = format.format(MyDateTime) ;
        Log.e("conff","Date Riyadh "+format.format(asiaZonedDateTime)) ;
        Log.e("conff","Date My "+format.format(MyDateTime) ) ;
        Log.e("conff","Date My "+destinationTimeZone.toString() ) ;

        return MyTimeZoneStr;
    }
}
