package goldenauctions.com.Medoles;

public class Product {
    private String image ;

    public Product() {
    }

    public Product(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
