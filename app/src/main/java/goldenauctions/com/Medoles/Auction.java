package goldenauctions.com.Medoles;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import goldenauctions.com.Medoles.Install.City;

public class Auction {
    private int id;
    private int category_id ;
    private String name ;
    private String description ;
    private String lat ;
    private String lng ;
    private String price ;
    @SerializedName("Media")
    private List<Media> mediaList ;
    private String code ;
    private String end_at ;
    private LastBid LastBid ;
    private String bids_count ;
    private boolean is_favourite = false;
    @SerializedName("City")
    private City city ;
    private String minimum_bid ;
    private String terms_conditions ;
    private String start_at ;
    private int status ;

    @SerializedName("AuctionDetails")
    private List<AuctionDetails> auctionDetails ;


    public Auction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEnd_at() {
        return end_at;
    }

    public void setEnd_at(String end_at) {
        this.end_at = end_at;
    }

    public  LastBid getLastBid() {
        return LastBid;
    }

    public void setLastBid( LastBid lastBid) {
        LastBid = lastBid;
    }

    public boolean isIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(boolean is_favourite) {
        this.is_favourite = is_favourite;
    }

    public List<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(List<Media> mediaList) {
        this.mediaList = mediaList;
    }

    public String getBids_count() {
        return bids_count;
    }

    public void setBids_count(String bids_count) {
        this.bids_count = bids_count;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getMinimum_bid() {
        if(TextUtils.isEmpty(minimum_bid)){
            return "1";
        }

        return minimum_bid;
    }

    public void setMinimum_bid(String minimum_bid) {
        this.minimum_bid = minimum_bid;
    }

    public String getTerms_conditions() {
        return terms_conditions;
    }

    public void setTerms_conditions(String terms_conditions) {
        this.terms_conditions = terms_conditions;
    }

    public String getStart_at() {
        return start_at;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<AuctionDetails> getAuctionDetails() {
        return auctionDetails;
    }

    public void setAuctionDetails(List<AuctionDetails> auctionDetails) {
        this.auctionDetails = auctionDetails;
    }
}
