package goldenauctions.com.Medoles.Install;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Status {
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private List<String> message = null;

    @SerializedName("code")
    @Expose
    private Integer code;

    public Status() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getMessage() {
        return message;
    }
    public String  getMessageList() {
       String message_str = "";
        for (int i = 0; i < message.size(); i++) {
            message_str += message.get(i) + "\n";
        }
        return  message_str ;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
