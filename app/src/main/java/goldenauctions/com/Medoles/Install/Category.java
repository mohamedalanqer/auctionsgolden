package goldenauctions.com.Medoles.Install;

import java.util.List;

public class Category {
    private int id ;
    private String name ;
    private String image ;
    private boolean IsSelect  = false;
    private int auction_count ;

    public Category() {
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public Category(int id, String name, String image, boolean isSelect) {
        this.id = id;
        this.name = name;
        this.image = image;
        IsSelect = isSelect;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelect() {
        return IsSelect;
    }

    public void setSelect(boolean select) {
        IsSelect = select;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public int getAuction_count() {
        return auction_count;
    }

    public void setAuction_count(int auction_count) {
        this.auction_count = auction_count;
    }
}
