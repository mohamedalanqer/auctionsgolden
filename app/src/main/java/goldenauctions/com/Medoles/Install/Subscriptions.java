package goldenauctions.com.Medoles.Install;

public class Subscriptions {


    private int id ;
    private String name ;
    private String description ;
    private String price ;
    private String  months ;
    private String image ;

    public Subscriptions() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }
}
