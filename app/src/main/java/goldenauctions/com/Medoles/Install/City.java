package goldenauctions.com.Medoles.Install;

public class City {
     private int id ;
     private String name ;
     private boolean isSelect ;
    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
