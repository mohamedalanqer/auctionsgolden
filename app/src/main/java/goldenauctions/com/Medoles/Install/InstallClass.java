package goldenauctions.com.Medoles.Install;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InstallClass {

    @SerializedName("Cities")
    List<City> Cities  ;
    @SerializedName("Categories")
    List<Category> Categories ;
    @SerializedName("Subscriptions")
    List<Subscriptions> Subscriptions ;
    @SerializedName("BankAccounts")
    List<BankAccount> BankAccounts ;

    @SerializedName("DocumentsTypes")
    List<DocumentType> DocumentsTypes ;

    @SerializedName("Settings")
    Settings settings ;

    public List<City> getCities() {
        return Cities;
    }

    public List<Category> getCategories() {
        return Categories;
    }

    public void setCities(List<City> cities) {
        Cities = cities;
    }

    public void setCategories(List<Category> categories) {
        Categories = categories;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public List<Subscriptions> getSubscriptions() {
        return Subscriptions;
    }

    public void setSubscriptions(List<Subscriptions> subscriptions) {
        Subscriptions = subscriptions;
    }

    public List<BankAccount> getBankAccounts() {
        return BankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        BankAccounts = bankAccounts;
    }

    public List<DocumentType> getDocumentsTypes() {
        return DocumentsTypes;
    }

    public void setDocumentsTypes(List<DocumentType> documentsTypes) {
        DocumentsTypes = documentsTypes;
    }
}



