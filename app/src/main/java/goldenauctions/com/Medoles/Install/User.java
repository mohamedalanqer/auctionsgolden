package goldenauctions.com.Medoles.Install;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("mobile")
    @Expose
    private String mobile;


    @SerializedName("email")
    @Expose
    private String email;


    @SerializedName("mobile_verified_at")
    @Expose
    private String mobile_verified_at;

    @SerializedName("email_verified_at")
    @Expose
    private String email_verified_at;


    @SerializedName("type")
    @Expose
    private int type;



    @SerializedName("avatar")
    @Expose
    private String avatar;


    @SerializedName("City")
    @Expose
    private City City;


    @SerializedName("lat")
    @Expose
    private String lat;

    @SerializedName("lng")
    @Expose
    private String lng;

    @SerializedName("app_locale")
    @Expose
    private String app_locale;

    @SerializedName("notification_count")
    @Expose
    private int notification_count;

    @SerializedName("access_token")
    @Expose
    private String access_token;


    @SerializedName("token_type")
    @Expose
    private String token_type;


    @SerializedName("device_token")
    @Expose
    private String device_token;

    @SerializedName("device_type")
    @Expose
    private String device_type;

    private String password;

    @SerializedName("is_subscribed")
    @Expose
    private boolean is_subscribed;

    @SerializedName("Subscriptions")
    @Expose
    private List<Subscriptions> Subscriptions;

    public User() {
    }

    public User(String name , String email , String mobile , String password,  String app_locale, String device_token, String device_type) {
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.app_locale = app_locale;
        this.device_token = device_token;
        this.device_type = device_type;
    }
    public User(String name , String email , String mobile ,    String app_locale, String device_token, String device_type) {
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.app_locale = app_locale;
        this.device_token = device_token;
        this.device_type = device_type;
    }
    public User(String email , String password, String device_token, String device_type) {
        this.email = email;
        this.password = password;
        this.device_token = device_token;
        this.device_type = device_type;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public String getMobile_verified_at() {
        return mobile_verified_at;
    }

    public void setMobile_verified_at(String mobile_verified_at) {
        this.mobile_verified_at = mobile_verified_at;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public void setEmail_verified_at(String email_verified_at) {
        this.email_verified_at = email_verified_at;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getApp_locale() {
        return app_locale;
    }

    public void setApp_locale(String app_locale) {
        this.app_locale = app_locale;
    }

    public int getNotification_count() {
        return notification_count;
    }

    public void setNotification_count(int notification_count) {
        this.notification_count = notification_count;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public  City getCity() {
        return City;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public void setCity( City city) {
        City = city;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public boolean isIs_subscribed() {
        return is_subscribed;
    }

    public void setIs_subscribed(boolean is_subscribed) {
        this.is_subscribed = is_subscribed;
    }

    public List<goldenauctions.com.Medoles.Install.Subscriptions> getSubscriptions() {
        return Subscriptions;
    }

    public void setSubscriptions(List<goldenauctions.com.Medoles.Install.Subscriptions> subscriptions) {
        Subscriptions = subscriptions;
    }
}
