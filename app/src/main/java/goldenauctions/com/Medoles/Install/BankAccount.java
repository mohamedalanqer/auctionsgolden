package goldenauctions.com.Medoles.Install;

public class BankAccount {
    private int id ;
    private String bank_name ;
    private String account_name ;
    private String account_number ;
    private String account_iban ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getAccount_iban() {
        return account_iban;
    }

    public void setAccount_iban(String account_iban) {
        this.account_iban = account_iban;
    }
}
