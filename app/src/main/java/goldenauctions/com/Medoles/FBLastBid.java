package goldenauctions.com.Medoles;

import com.google.gson.annotations.SerializedName;

import goldenauctions.com.Medoles.Install.User;

public class FBLastBid {
    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("userName")
    private String userName;
    @SerializedName("price")
    private String price;

    public FBLastBid(int id, int user_id, String userName, String price) {
        this.id = id;
        this.user_id = user_id;
        this.userName = userName;
        this.price = price;
    }

    public FBLastBid() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
