package goldenauctions.com.Medoles;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FaqsCategories {
   private int  id ; //": 1,
    private String name ; // ": "شسي",
    @SerializedName("Faqs")
    private List<Faq> faqs ;
    private boolean IsSelect = false; //": "1"

    public FaqsCategories() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Faq> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<Faq> faqs) {
        this.faqs = faqs;
    }

    public boolean isSelect() {
        return IsSelect;
    }

    public void setSelect(boolean select) {
        IsSelect = select;
    }
}
