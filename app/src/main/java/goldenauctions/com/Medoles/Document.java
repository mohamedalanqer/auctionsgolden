package goldenauctions.com.Medoles;

import com.google.gson.annotations.SerializedName;

import goldenauctions.com.Medoles.Install.DocumentType;

public class Document {

    private int id;
    private String document_type_id;
    @SerializedName("DocumentType")
    private DocumentType documentType;
    private String expiry_date;
    private String front_face;
    private String back_face;

    public Document() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDocument_type_id() {
        return document_type_id;
    }

    public void setDocument_type_id(String document_type_id) {
        this.document_type_id = document_type_id;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getFront_face() {
        return front_face;
    }

    public void setFront_face(String front_face) {
        this.front_face = front_face;
    }

    public String getBack_face() {
        return back_face;
    }

    public void setBack_face(String back_face) {
        this.back_face = back_face;
    }
}
