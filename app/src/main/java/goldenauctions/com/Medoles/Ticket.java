package goldenauctions.com.Medoles;

import java.util.List;

public class Ticket {
    private int id; //": 2,
    private int user_id;
    private String title; //": "اااا",
    private String message;
    private String attachment;
    private int status;
    private List<TicketResponses> TicketResponses;
    private String created_at;
    private String updated_at;


    public Ticket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<goldenauctions.com.Medoles.TicketResponses> getTicketResponses() {
        return TicketResponses;
    }

    public void setTicketResponses(List<goldenauctions.com.Medoles.TicketResponses> ticketResponses) {
        TicketResponses = ticketResponses;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
