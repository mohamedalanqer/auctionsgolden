package goldenauctions.com.Medoles;

public class SettingItem {
    private int id ;
    private String name ;
    private int ResImg ;
    private String value ;

    public SettingItem() {
    }
    public SettingItem(int id, String name, int resImg, String value) {
        this.id = id;
        this.name = name;
        ResImg = resImg;
        this.value = value;
    }
    public SettingItem(int id, String name, int resImg) {
        this.id = id;
        this.name = name;
        ResImg = resImg;
    }

    public SettingItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResImg() {
        return ResImg;
    }

    public void setResImg(int resImg) {
        ResImg = resImg;
    }
}
