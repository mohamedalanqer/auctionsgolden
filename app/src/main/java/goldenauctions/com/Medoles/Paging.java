package goldenauctions.com.Medoles;

public class Paging {

    private int total ;
    private int per_page ;
    private int current_page ;
    private int last_page ;
    private int from ;
    private int to ;

    public int getTotal() {
        return total;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public int getLast_page() {
        return last_page;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
}
