package goldenauctions.com.Medoles;

public class TicketResponses {
    private int id ;
    private String response ;
    private int  sender_type;

    public TicketResponses() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getSender_type() {
        return sender_type;
    }

    public void setSender_type(int sender_type) {
        this.sender_type = sender_type;
    }
}
