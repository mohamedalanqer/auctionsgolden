package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Faq;
import goldenauctions.com.Medoles.FaqsCategories;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.FaqsActivity;

public class AdapterFaqsCategory extends RecyclerView.Adapter<AdapterFaqsCategory.CustomView> {
    Context context;
    List<FaqsCategories> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    private AdapterFaqs adapter ;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name ;
        ImageView row_img;
        RecyclerView recyclerView ;


        public CustomView(View v) {
            super(v);
            row_name = v.findViewById(R.id.row_name);
            row_img = v.findViewById(R.id.row_img);
            recyclerView = v.findViewById(R.id.recycler_view);

        }

    }

    public AdapterFaqsCategory(Context context, List<FaqsCategories> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        Log.e("GetNotifications", mylist.get(position).getName() + " getAnswer");
        holder.row_name.setText(mylist.get(position).getName());
        if(mylist.get(position).getFaqs().size() == 0) {
            holder.row_img.setImageDrawable(null);
        }
        if (mylist.get(position).isSelect()) {
            if(mylist.get(position).getFaqs().size() >0) {
                holder.recyclerView.setVisibility(View.VISIBLE);
                holder.row_img.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24);
                setupRecycler(mylist.get(position).getFaqs(), holder);
            }else{
                holder.row_img.setImageResource(R.drawable.ic_baseline_lens_24);
            }
        } else {
            if(mylist.get(position).getFaqs().size() > 0) {
                holder.recyclerView.setVisibility(View.GONE);
                holder.row_img.setImageResource(R.drawable.ic_baseline_arrow_drop_up_24);
            }else {
                holder.row_img.setImageResource(R.drawable.ic_baseline_lens_24);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public void setupRecycler(List<Faq> faqList ,CustomView holder) {
        adapter = new AdapterFaqs( context, faqList, R.layout.row_item_faq, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (faqList.get(position).isSelect()) {
                            faqList.get(position).setSelect(false);
                        } else {
                            faqList.get(position).setSelect(true);
                            for (int i = 0; i < faqList.size(); i++) {
                                if (faqList.get(position).getId() != faqList.get(i).getId()) {
                                    if (faqList.get(i).isSelect()) {
                                        faqList.get(i).setSelect(false);
                                    }
                                }
                            }
                        }


                        adapter.notifyDataSetChanged();
                    }
                });
        LinearLayoutManager   linearLayoutManager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(linearLayoutManager);
        holder.recyclerView.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

