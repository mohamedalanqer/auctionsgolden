package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Notification;
import goldenauctions.com.R;

public class AdapterNotifcation extends RecyclerView.Adapter<AdapterNotifcation.CustomView> {
    Context context;
    List<Notification> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_title, text_body, text_date;
        ImageView image_row, image_bell_row;

        public CustomView(View v) {
            super(v);
            text_title = v.findViewById(R.id.text_title);
            text_body = v.findViewById(R.id.text_body);
            image_row = v.findViewById(R.id.image_row);
            image_bell_row = v.findViewById(R.id.image_bell_row);
            text_date = v.findViewById(R.id.text_date);
        }

    }

    public AdapterNotifcation(Context context, List<Notification> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.text_title.setText(mylist.get(position).getTitle() + "");
        holder.text_body.setText(mylist.get(position).getMessage() + "");
        holder.text_date.setText("" + mylist.get(position).getCreated_at());
        if (TextUtils.equals("0", mylist.get(position).getType())) {
            holder.image_row.setImageResource(R.drawable.logo);
        } else {
            if (mylist.get(position).getRef_id() != null)
                holder.text_title.setText(Html.fromHtml(mylist.get(position).getTitle() + " (<font color=black>" + mylist.get(position).getRef_id() + "</font>)"));

        }


        if (mylist.get(position).getRead_at() == null) {
            holder.image_bell_row.setVisibility(View.VISIBLE);
        } else {
            holder.image_bell_row.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (TextUtils.equals(RootManager.NotificationTypeOrder + "", mylist.get(position).getType()) ||
                            TextUtils.equals(RootManager.NotificationTypeTiket + "", mylist.get(position).getType()))
                        listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

