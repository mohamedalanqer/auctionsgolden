package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.Subscriptions;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;


public class AdapterSubscriptions extends PagerAdapter {
    private List<Subscriptions> storyList;
    Context mContext;
    private LayoutInflater mInflater;
    private OnItemClickListener onItemClickListener;
    private User user = null;

    public AdapterSubscriptions(Context context, List<Subscriptions> storyList, OnItemClickListener onItemClickListener) {
        this.storyList = storyList;
        mContext = context;
        this.onItemClickListener = onItemClickListener;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        user = PreferenceUser.User(context);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getCount() {
        return storyList.size();
    }


    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View itemView = mInflater.inflate(R.layout.row_item_subscriptions, view, false);
        TextView text_name = itemView.findViewById(R.id.text_name);
        TextView text_price = itemView.findViewById(R.id.text_price);
        TextView text_body = itemView.findViewById(R.id.text_body);

        text_name.setText("" + storyList.get(position).getName());
        text_price.setText("" + storyList.get(position).getPrice() + " " + mContext.getResources().getString(R.string.dollar_code));
        text_body.setText("" + storyList.get(position).getDescription());


        text_name.setCompoundDrawables(null, null, null, null);
        if (user != null) {
            if (user.isIs_subscribed()) {
                if (user.getSubscriptions().size() > 0) {
                    for(int i = 0 ;i< user.getSubscriptions().size() ;i++){
                        if(storyList.get(position).getId() == user.getSubscriptions().get(i).getId()){
                            Drawable img = mContext.getResources().getDrawable(R.drawable.ic_baseline_check_circle_24);
                            img.setBounds(0, 0, 60, 60);
                            text_name.setCompoundDrawables(img, null, null, null);
                        }
                    }

                }
            }
        }


        itemView.setOnClickListener(v -> {
            try {
                onItemClickListener.onItemClick(v, position);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        view.addView(itemView);
        return itemView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}