package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Install.BankAccount;
import goldenauctions.com.R;

public class AdapterBankAccount extends RecyclerView.Adapter<AdapterBankAccount.CustomView> {
    Context context;
    List<BankAccount> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_account_number, text_account_iban, text_bank_name , text_name_acount;
        ImageView image_row   ;

        public CustomView(View v) {
            super(v);
            text_account_number = v.findViewById(R.id.text_account_number);
            text_account_iban = v.findViewById(R.id.text_account_iban);
            image_row = v.findViewById(R.id.image_row);
            text_name_acount = v.findViewById(R.id.text_name_acount);
            text_bank_name = v.findViewById(R.id.text_bank_name);
        }

    }

    public AdapterBankAccount(Context context, List<BankAccount> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.text_account_number.setText(mylist.get(position).getAccount_number() + "");
        holder.text_account_iban.setText(mylist.get(position).getAccount_iban() + "");
        holder.text_bank_name.setText("" + mylist.get(position).getBank_name());
        holder.text_name_acount.setText("" + mylist.get(position).getAccount_name());


        holder.text_account_iban.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                        listener.onItemClick(view, position, "iban");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.text_account_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    listener.onItemClick(view, position, "account");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

