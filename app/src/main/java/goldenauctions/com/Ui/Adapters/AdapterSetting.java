package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.SettingItem;
import goldenauctions.com.R;


public class AdapterSetting extends RecyclerView.Adapter<AdapterSetting.CustomView> {
    Context context;
    List<SettingItem> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1;
    private String typeAdapter = "profile";


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_value;
        ImageView row_img;

        public CustomView(View v) {
            super(v);
            row_name = v.findViewById(R.id.row_name);
            row_img = v.findViewById(R.id.row_img);
            row_value = v.findViewById(R.id.row_value);
        }

    }

    public AdapterSetting() {
    }

    public AdapterSetting(Context context, List<SettingItem> mylist, int ItemSelect, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect = ItemSelect;
    }

    public AdapterSetting(Context context, List<SettingItem> mylist, int ItemSelect, String typeAdapter, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect = ItemSelect;
        this.typeAdapter = typeAdapter;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getName() + "");
        holder.row_img.setImageResource(mylist.get(position).getResImg());

        if (mylist.get(position).getValue() != null) {
            holder.row_value.setVisibility(View.VISIBLE);
            holder.row_value.setText("" + mylist.get(position).getValue());
        } else {
            holder.row_value.setVisibility(View.GONE);
            holder.row_value.setText("");
        }

        if(!TextUtils.equals("profile",typeAdapter)){
            if(position == ItemSelect){
                holder.row_img.setBackgroundResource(R.drawable.shape_btn_primary);
                holder.row_img.setColorFilter(context.getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
                holder.row_name.setTextColor(context.getResources().getColor(R.color.colorBlack));

            }else{
                holder.row_img.setBackground(null);
                holder.row_img.setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                holder.row_name.setTextColor(context.getResources().getColor(R.color.colorGrey));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

