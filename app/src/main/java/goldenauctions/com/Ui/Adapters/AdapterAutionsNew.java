package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.UtilitiesDate;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.R;

public class AdapterAutionsNew extends RecyclerView.Adapter<AdapterAutionsNew.CustomView> {
    Context context;
    List<Auction> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    CountDownTimer mCountDownTimer = null;

    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_title, text_code, text_date_time, text_price, text_follow_up;
        ImageView image_row, img_follow_up;


        public CustomView(View v) {
            super(v);
            text_title = v.findViewById(R.id.text_title);
            text_price = v.findViewById(R.id.text_price);
            text_code = v.findViewById(R.id.text_code);
            image_row = v.findViewById(R.id.image_row);
            img_follow_up = v.findViewById(R.id.img_follow_up);
            text_follow_up = v.findViewById(R.id.text_follow_up);
            text_date_time = v.findViewById(R.id.text_date_time);


        }

    }

    public AdapterAutionsNew(Context context, List<Auction> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        Auction auction = mylist.get(position);
        holder.text_title.setText("" + auction.getName());
        if (auction.getMediaList().size() > 0)
            Glide.with(context).load(auction.getMediaList().get(0).getFile()).error(R.drawable.no_image).into(holder.image_row);
        else
            holder.image_row.setImageResource(R.drawable.no_image);

        holder.text_price.setText("" + auction.getPrice() + " " + context.getResources().getString(R.string.dollar_code));


        if (auction.isIs_favourite()) {
            holder.img_follow_up.setImageResource(R.drawable.ic_follow_up);
            holder.text_follow_up.setText(context.getResources().getString(R.string.UnFollow));
            holder.text_follow_up.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.img_follow_up.setImageResource(R.drawable.ic_unfollow);
            holder.text_follow_up.setText(context.getResources().getString(R.string.FollowUp));
            holder.text_follow_up.setTextColor(context.getResources().getColor(R.color.greyLight));
        }

        if (auction.getStart_at() != null) {
            boolean isStart = UtilitiesDate.IsStartAuctionOut(context, auction.getStart_at(), holder.text_date_time, mCountDownTimer);
            if (isStart) {
                if (auction.getEnd_at() != null) {
                    UtilitiesDate.StartCountdownTimer(context, auction.getEnd_at(), holder.text_date_time, holder.text_date_time, true);
                }
            } else {
                holder.text_date_time.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence != null) {
                            String result = charSequence.toString();
                            if (!TextUtils.isEmpty(result)) {
                                if (TextUtils.equals(result, context.getResources().getString(R.string.StartNow))) {
                                    if (auction.getEnd_at() != null) {
                                        UtilitiesDate.StartCountdownTimer(context, auction.getEnd_at(), holder.text_date_time, holder.text_date_time, false);
                                    }
                                }
                            }
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }

        }

        holder.text_code.setText("#" + auction.getCode());
        holder.img_follow_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "UnFollow");

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        if (auction.getLastBid() != null) {
            if (auction.getLastBid().getPrice() != null) {
                holder.text_price.setText(auction.getLastBid().getPrice() + " " + context.getResources().getString(R.string.dollar_code));
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

