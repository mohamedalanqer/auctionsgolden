package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Medoles.Install.Subscriptions;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.TicketResponses;
import goldenauctions.com.R;

public class AdapterMySubScriptions extends RecyclerView.Adapter<AdapterMySubScriptions.CustomView> {
    Context context;
    List<Subscriptions> mylist;
     int Layout;
    private User user;

    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_value;
        ImageView row_img;


        public CustomView(View v) {
            super(v);
             row_name = v.findViewById(R.id.row_name);
            row_value = v.findViewById(R.id.row_value);
            row_img = v.findViewById(R.id.row_img);

        }

    }

    public AdapterMySubScriptions(Context context, List<Subscriptions> mylist, int Layout ) {
        this.context = context;
        this.mylist = mylist;
        this.Layout = Layout;
        this.user = PreferenceUser.User(context);
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {

        holder.row_name.setText(mylist.get(position).getName() + "");
        holder.row_value.setText(mylist.get(position).getPrice() +" "+context.getResources().getString(R.string.dollar_code));



    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

