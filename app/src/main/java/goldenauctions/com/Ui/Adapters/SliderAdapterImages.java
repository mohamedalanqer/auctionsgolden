package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Media;
import goldenauctions.com.R;

public class SliderAdapterImages extends SliderViewAdapter<SliderAdapterImages.SliderAdapterVH> {

    private Context context;
    private List<Media> mSliderItems = new ArrayList<>();
    private OnItemClickTagListener onItemClickTagListener ;

    public SliderAdapterImages(Context context , List<Media> mSliderItems, OnItemClickTagListener onItemClickTagListener) {
        this.context = context;
        this.mSliderItems = mSliderItems ;
        this.onItemClickTagListener = onItemClickTagListener ;
    }

    public void renewItems(List<Media> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(Media sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        Media sliderItem = mSliderItems.get(position);

        Log.e("images", sliderItem.getFile() + "");
        Glide.with(viewHolder.itemView)
                .load(sliderItem.getFile()) .listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                viewHolder.progressbar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                viewHolder.progressbar.setVisibility(View.GONE);
                return false;
            }
        })
                .fitCenter()
                .into(viewHolder.imageViewBackground);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onItemClickTagListener.onItemClick(v,position ,"view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mSliderItems.size();
    }

    class SliderAdapterVH    extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ProgressBar progressbar ;


        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            progressbar = itemView.findViewById(R.id.progressbar);

            this.itemView = itemView;
        }
    }

}