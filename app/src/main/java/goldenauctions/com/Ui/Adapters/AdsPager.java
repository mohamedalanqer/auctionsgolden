package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Product;
import goldenauctions.com.R;

public class AdsPager extends PagerAdapter {
    private Context mContext;
    LayoutInflater mLayoutInflater;
    private List<Auction> myList;
    private OnItemClickTagListener listener;

    public AdsPager(Context context, List<Auction> myList, OnItemClickTagListener listener) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.myList = myList;
        this.listener = listener;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.ads_pager_item, container, false);
        Auction advertisement = myList.get(position);

        ImageView imageView = itemView.findViewById(R.id.img_avatar);
        if (advertisement.getMediaList() != null) {
            if (advertisement.getMediaList().size() > 0)
                Glide.with(mContext).load(advertisement.getMediaList().get(0).getFile()).error(R.drawable.no_image).into(imageView);
            else
                imageView.setImageResource(R.drawable.no_image);
        } else
            imageView.setImageResource(R.drawable.no_image);

        itemView.setOnClickListener(view -> {
            try {
                listener.onItemClick(view, position, "view");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}