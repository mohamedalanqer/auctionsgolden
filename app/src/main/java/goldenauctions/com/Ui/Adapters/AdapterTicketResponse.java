package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.TicketResponses;
import goldenauctions.com.R;

public class AdapterTicketResponse extends RecyclerView.Adapter<AdapterTicketResponse.CustomView> {
    Context context;
    List<TicketResponses> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    private User user;

    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_value;
        ImageView row_img;


        public CustomView(View v) {
            super(v);
             row_name = v.findViewById(R.id.row_name);
            row_value = v.findViewById(R.id.row_value);
            row_img = v.findViewById(R.id.row_img);

        }

    }

    public AdapterTicketResponse(Context context, List<TicketResponses> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.user = PreferenceUser.User(context);
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {

        holder.row_name.setText(mylist.get(position).getResponse() + "");
        holder.row_value.setVisibility(View.GONE);
        holder.row_img.setImageResource(R.drawable.logo);

        if (mylist.get(position).getSender_type() == 1) {
            if (user != null) {
                if (TextUtils.isEmpty(user.getAvatar())) {
                    Glide.with(context).load(R.drawable.ic_white_person_24).into(holder.row_img);
                } else {
                    Glide.with(context).load(user.getAvatar()).error(R.drawable.ic_white_person_24).into(holder.row_img);
                }
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

