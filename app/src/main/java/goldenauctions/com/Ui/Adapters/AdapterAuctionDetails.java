package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.AuctionDetails;
import goldenauctions.com.Medoles.SettingItem;
import goldenauctions.com.R;


public class AdapterAuctionDetails extends RecyclerView.Adapter<AdapterAuctionDetails.CustomView> {
    Context context;
    List<AuctionDetails> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_value;

        public CustomView(View v) {
            super(v);
            row_name = v.findViewById(R.id.row_name);
            row_value = v.findViewById(R.id.row_value);
        }

    }

    public AdapterAuctionDetails() {
    }

    public AdapterAuctionDetails(Context context, List<AuctionDetails> mylist, int ItemSelect, int Layout ) {
        this.context = context;
        this.mylist = mylist;
        this.Layout = Layout;

    }



    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getName() + "");

        if (mylist.get(position).getValue() != null) {
            holder.row_value.setVisibility(View.VISIBLE);
            holder.row_value.setText("" + mylist.get(position).getValue());
        } else {
            holder.row_value.setVisibility(View.GONE);
            holder.row_value.setText("");
        }





    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

