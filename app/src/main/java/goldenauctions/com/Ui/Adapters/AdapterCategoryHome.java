package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.DrawableRound;
import goldenauctions.com.Medoles.Install.Category;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.HomeActivity;

public class AdapterCategoryHome extends RecyclerView.Adapter<AdapterCategoryHome.CustomView> {
    Context context;
    List<Category> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1;
    public   String TageName = "Category";


    public class CustomView extends RecyclerView.ViewHolder {
        LinearLayout card_view;
        TextView row_text ;
        ImageView row_img,img_count;

        public CustomView(View v) {
            super(v);
            card_view = v.findViewById(R.id.layout_body);
            row_text = v.findViewById(R.id.row_name);
            row_img = v.findViewById(R.id.row_img);
            img_count = v.findViewById(R.id.img_count);
        }

    }

    public AdapterCategoryHome(Context context, List<Category> mylist, int ItemSelect, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.ItemSelect = ItemSelect;
        this.Layout = Layout;
        this.TageName = "Category";
    }

    public AdapterCategoryHome(Context context, List<Category> mylist, int ItemSelect ,String TageName, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.ItemSelect = ItemSelect;
        this.Layout = Layout;
        this.TageName = TageName ;
    }


    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_text.setText("" + mylist.get(position).getName());
        if(TextUtils.equals("Category" , TageName)){
            if (ItemSelect == position) {
                holder.card_view.setBackgroundResource(R.color.colorPrimary);
                holder.row_text.setTextColor(context.getResources().getColor(R.color.colorWhite));
            } else {
                holder.card_view.setBackgroundResource(R.color.colorWhite);
                holder.row_text.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }



           // if (mylist.get(position).getAuction_count() > -1) {
                holder.img_count.setVisibility(View.VISIBLE);
                DrawableRound.TextToDrawableRoundRed(context, holder.img_count, mylist.get(position).getAuction_count() + "");
            //}

        }else{
            if (ItemSelect == position) {
                holder.card_view.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary) , PorterDuff.Mode.MULTIPLY);
                holder.row_text.setTextColor(context.getResources().getColor(R.color.colorWhite));
                holder.row_img.setColorFilter(context.getResources().getColor(R.color.colorWhite), PorterDuff.Mode.DST_IN);
            } else {
                holder.card_view.getBackground().setColorFilter(null);
                holder.row_text.setTextColor(context.getResources().getColor(R.color.colorBlack));
                holder.row_img.setColorFilter(context.getResources().getColor(R.color.colorGreyIcon), PorterDuff.Mode.DST_IN);
            }
        }



        if (mylist.get(position).getImage() != null) {
            Glide.with(context).load(mylist.get(position).getImage()).error(R.drawable.logo).into(holder.row_img);
        } else
            holder.row_img.setImageResource(R.drawable.logo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}
