package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Document;
import goldenauctions.com.R;

public class AdapterDocument extends RecyclerView.Adapter<AdapterDocument.CustomView> {
    Context context;
    List<Document> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_title, text_code, text_date_time, text_price;
        ImageView image_row, img_edit;

        public CustomView(View v) {
            super(v);
            text_title = v.findViewById(R.id.text_title);
            text_price = v.findViewById(R.id.text_price);
            text_code = v.findViewById(R.id.text_code);
            image_row = v.findViewById(R.id.image_row);
            img_edit = v.findViewById(R.id.img_edit);
            text_date_time = v.findViewById(R.id.text_date_time);
        }

    }

    public AdapterDocument(Context context, List<Document> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        Document document = mylist.get(position);
        holder.text_title.setText("" + document.getDocumentType().getName());
        if (document.getFront_face() != null)
            Glide.with(context).load(document.getFront_face() + "").error(R.drawable.no_image).into(holder.image_row);
        else {
            if (document.getDocumentType().getImage() != null) {
                Glide.with(context).load(document.getDocumentType().getImage() + "").error(R.drawable.no_image).into(holder.image_row);
            } else {
                holder.image_row.setImageResource(R.drawable.no_image);

            }
        }

        if (document.getExpiry_date() != null) {
            holder.text_date_time.setText("" + document.getExpiry_date() + " ");
        } else {
            holder.text_date_time.setText(""+context.getResources().getString(R.string.not_specified));
        }

        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "edit");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

