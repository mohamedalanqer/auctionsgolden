package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Faq;
import goldenauctions.com.R;

public class AdapterFaqs extends RecyclerView.Adapter<AdapterFaqs.CustomView> {
    Context context;
    List<Faq> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_value;
        ImageView row_img;


        public CustomView(View v) {
            super(v);
            row_name = v.findViewById(R.id.row_name);
            row_value = v.findViewById(R.id.row_value);
            row_img = v.findViewById(R.id.row_img);

        }

    }

    public AdapterFaqs(Context context, List<Faq> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {

        holder.row_name.setText(mylist.get(position).getQuestion() + "");
       // holder.row_value.setText(Html.fromHtml(mylist.get(position).getAnswer() )+ "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.row_value.setText(Html.fromHtml(mylist.get(position).getAnswer() , Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.row_value.setText(Html.fromHtml(mylist.get(position).getAnswer() ));
        }
        if (mylist.get(position).isSelect()) {
            holder.row_value.setVisibility(View.VISIBLE);
            holder.row_img.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24);
        } else {
            holder.row_value.setVisibility(View.GONE);
            holder.row_img.setImageResource(R.drawable.ic_baseline_arrow_drop_up_24);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

