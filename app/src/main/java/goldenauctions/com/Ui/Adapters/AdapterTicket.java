package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Ticket;
import goldenauctions.com.R;

public class AdapterTicket extends RecyclerView.Adapter<AdapterTicket.CustomView> {
    Context context;
    List<Ticket> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_title, text_body , text_status;


        public CustomView(View v) {
            super(v);
            text_title = v.findViewById(R.id.text_title);
            text_body = v.findViewById(R.id.text_body);
            text_status = v.findViewById(R.id.text_status);

        }

    }

    public AdapterTicket(Context context, List<Ticket> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.text_title.setText(mylist.get(position).getTitle() + "");
        holder.text_body.setText(mylist.get(position).getMessage() + "");
        if (TextUtils.equals("1", mylist.get(position).getStatus()+"")) {
            holder.text_status.setEnabled(true);
            holder.text_status.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.text_status.setText(""+context.getResources().getString(R.string.Opened));
            holder.text_status.setBackgroundResource(R.drawable.shape_btn_disactive);
        } else {
            holder.text_status.setEnabled(false);
            holder.text_status.setText(""+context.getResources().getString(R.string.Closed));
            holder.text_status.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.text_status.setBackgroundResource(R.drawable.shape_btn_disactive);
            holder.text_status.getBackground().setColorFilter(context.getResources().getColor(R.color.colorRed) , PorterDuff.Mode.MULTIPLY);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                        listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

