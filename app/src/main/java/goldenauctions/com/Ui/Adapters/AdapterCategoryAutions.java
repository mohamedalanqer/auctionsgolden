package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Medoles.Install.Category;
import goldenauctions.com.R;

public class AdapterCategoryAutions extends RecyclerView.Adapter<AdapterCategoryAutions.CustomView> {
    Context context;
    List<Category> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1;


    public class CustomView extends RecyclerView.ViewHolder {
        LinearLayout card_view;
        TextView row_text;
        ImageView img_drop ;

        public CustomView(View v) {
            super(v);
            card_view = v.findViewById(R.id.layout_body);
            row_text = v.findViewById(R.id.row_name);
            img_drop =v.findViewById(R.id.img_drop);
        }

    }

    public AdapterCategoryAutions(Context context, List<Category> mylist, int ItemSelect, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.ItemSelect = ItemSelect;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_text.setText(""+mylist.get(position).getName());
        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled}, // enabled
        };

        int[] colors = new int[] {
                Color.BLACK,

        };

        if (mylist.get(position).isSelect()) {
            holder.card_view.setBackgroundResource(R.color.colorPrimary);
            holder.row_text.setTextColor(context.getResources().getColor(R.color.colorWhite));
            if(position >0){
                holder.img_drop.setVisibility(View.VISIBLE);
            }
            colors[0] = Color.WHITE ;
        } else {
            holder.card_view.setBackgroundResource(R.color.colorWhite);
            holder.row_text.setTextColor(context.getResources().getColor(R.color.colorBlack));
            holder.img_drop.setVisibility(View.GONE);
            colors[0] = Color.BLACK ;
        }


        ColorStateList myList = new ColorStateList(states, colors);
        TextViewCompat.setCompoundDrawableTintList(holder.row_text, myList) ;


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}
