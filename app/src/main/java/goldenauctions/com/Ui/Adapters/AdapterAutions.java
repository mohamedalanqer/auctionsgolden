package goldenauctions.com.Ui.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.UtilitiesDate;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Notification;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.AuctionsDetialsActivity;

public class AdapterAutions extends RecyclerView.Adapter<AdapterAutions.CustomView> {
    Context context;
    List<Auction> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_title, text_code, text_date_time, text_price, text_count, text_city;
        ImageView image_row;

        public CustomView(View v) {
            super(v);
            text_title = v.findViewById(R.id.text_title);
            text_price = v.findViewById(R.id.text_price);
            text_code = v.findViewById(R.id.text_code);
            image_row = v.findViewById(R.id.image_row);
            text_count = v.findViewById(R.id.text_count);
            text_date_time = v.findViewById(R.id.text_date_time);
            text_city = v.findViewById(R.id.text_city);


        }

    }

    public AdapterAutions(Context context, List<Auction> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        Auction auction = mylist.get(position);
        holder.text_title.setText("" + auction.getName());
        if (auction.getMediaList().size() > 0)
            Glide.with(context).load(auction.getMediaList().get(0).getFile()).error(R.drawable.no_image).into(holder.image_row);
        else
            holder.image_row.setImageResource(R.drawable.no_image);

        holder.text_price.setText("" + auction.getPrice() + " " + context.getResources().getString(R.string.dollar_code));
        if (auction.getStart_at() != null) {
            boolean isStart = UtilitiesDate.IsStartAuction(context, auction.getStart_at(), holder.text_date_time, holder.text_date_time);
             if (isStart) {
                if (auction.getEnd_at() != null) {
                    UtilitiesDate.StartCountdownTimer(context, auction.getEnd_at(), holder.text_date_time, holder.text_date_time, false);
                }
            } else {
                holder.text_date_time.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence != null) {
                            String result = charSequence.toString();
                            if (!TextUtils.isEmpty(result)) {
                                if (TextUtils.equals(result, context.getResources().getString(R.string.StartNow))) {
                                    if (auction.getEnd_at() != null) {
                                        UtilitiesDate.StartCountdownTimer(context, auction.getEnd_at(), holder.text_date_time, holder.text_date_time, false);
                                    }
                                }
                            }
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }

        }
        /**
         if (auction.getEnd_at() != null) {
         UtilitiesDate.StartCountdownTimer(context, auction.getEnd_at(), holder.text_date_time, holder.text_date_time, false);
         }
         */
        holder.text_code.setText("#" + auction.getCode());
        holder.text_count.setText("" + auction.getBids_count() + " " + context.getResources().getString(R.string.Auctions));
        if (auction.getCity() != null)
            holder.text_city.setText("" + auction.getCity().getName());

        if (auction.getLastBid() != null) {
            if (auction.getLastBid().getPrice() != null) {
                holder.text_price.setText(auction.getLastBid().getPrice() + " " + context.getResources().getString(R.string.dollar_code));
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

