package goldenauctions.com.Ui.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Faq;
import goldenauctions.com.Medoles.FaqsCategories;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterFaqs;
import goldenauctions.com.Ui.Adapters.AdapterFaqsCategory;
import goldenauctions.com.Ui.Adapters.RecyclerViewScrollListener;
import goldenauctions.com.WebService.model.response.FaqsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;


public class FaqsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recycler_view;
    LinearLayout layout_empty;
    AdapterFaqsCategory adapter;
    private FrameLayout layout_loading;
    List<FaqsCategories> faqList = new ArrayList<>();
    FrameLayout layout_back;
    TextView toolbarNameTxt;
    RecyclerRefreshLayout swipeRefreshLayout;
    boolean IsLodeMore = false;
    private RecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    private FloatingActionButton fab;
    int page = 1;
    int per_page = 10;

    private TextView text_empty;
    private ConnectionManager connectionManager;

    User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        setContentView(R.layout.activity_ticket);
        user = PreferenceUser.User(this);
        connectionManager = new ConnectionManager(this);

        initView();

    }

    private void SetLodeMore() {
        scrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page_, int totalItemsCount) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (IsLodeMore) {
                            page++;
                            //  FaqsWebSevice(page, per_page);
                        }

                    }
                }, 1000);

            }

            @Override
            public void onReachTop(boolean isFirst) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        recycler_view.addOnScrollListener(scrollListener);
    }

    private void FaqsWebSevice( ) {
        recycler_view.setVisibility(View.VISIBLE);
        layout_loading.setVisibility(View.VISIBLE);
        connectionManager.GetMyFaqs(page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response res) {
                if (res != null) {
                    Response<FaqsResponse> response = res;
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        String message_str = response.body().getStatus().getMessageList();
                        if ("success".equals(response.body().getStatus().getStatus())) {
                                faqList = response.body().getFaqsCategories();
                                if (faqList != null) {
                                    setupRecycler();

                                } else {
                                    recycler_view.setVisibility(View.GONE);
                                    layout_empty.setVisibility(View.VISIBLE);
                                }

                        } else if ("error".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(FaqsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        } else if ("fail".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(FaqsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        }

                    }

                }
            }
        });
    }


    private void initView() {
        recycler_view = findViewById(R.id.recycler_view);
        layout_empty = findViewById(R.id.layout_empty);
        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });
        text_empty = findViewById(R.id.text_empty);
        text_empty.setText(getResources().getString(R.string.EmptyData));
        fab = findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        layout_back = findViewById(R.id.layout_toolbar_menu);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText("" + getString(R.string.common_questions));

        FaqsWebSevice( );

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void setupRecycler() {
        adapter = new AdapterFaqsCategory(FaqsActivity.this, faqList, R.layout.row_item_faq_cat, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (faqList.get(position).isSelect()) {
                            faqList.get(position).setSelect(false);
                        } else {
                            faqList.get(position).setSelect(true);
                            for (int i = 0; i < faqList.size(); i++) {
                                if (faqList.get(position).getId() != faqList.get(i).getId()) {
                                    if (faqList.get(i).isSelect()) {
                                        faqList.get(i).setSelect(false);
                                    }
                                }
                            }
                        }


                        adapter.notifyDataSetChanged();
                    }
                });
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        //SetLodeMore();
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                page = 1;
                FaqsWebSevice( );
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(FaqsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(FaqsActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


}
