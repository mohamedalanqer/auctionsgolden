package goldenauctions.com.Ui.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import goldenauctions.com.CallBack.OnItemClickListener;
import goldenauctions.com.ChatKit.AdapterAllChat;
import goldenauctions.com.ChatKit.ChatActivity;
import goldenauctions.com.ChatKit.ModelChatUser;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static goldenauctions.com.Manager.RootManager.enterTransition;
import static goldenauctions.com.Manager.RootManager.returnTransition;

public class ChatRoomsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerRefreshLayout swipeRefreshLayout;
    RecyclerView recycler_view;
    AdapterAllChat adapterAllChat;
    LinearLayout layout_empty;
    private TextView toolbarNameTxt;
    FrameLayout layout_back;

    FirebaseFirestore db;
    List<ModelChatUser> data = new ArrayList<>();
    private User user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_chat_rooms);

        initSetUp();
    }

    private void initSetUp() {
        db = FirebaseFirestore.getInstance();
        user = PreferenceUser.User(this);
        recycler_view = findViewById(R.id.recycler_view);
        layout_empty = findViewById(R.id.layout_empty);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText(getResources().getString(R.string.Chat));
        layout_back = findViewById(R.id.layout_toolbar_menu);
        layout_back.setOnClickListener(view -> {
            onBackPressed();
        });
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        // start_countdown_timer("2020-10-25 00:18:33",text_date);
        getChatUser();
    }

    private void getChatUser() {
        if (user != null) {

            db.collection("usersChat::").document(user.getId() + "").collection("chat::")

                    .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    data = queryDocumentSnapshots.toObjects(ModelChatUser.class);

                    if (data.size() > 0) {
                        Collections.reverse(data);
                        recycler_view.setVisibility(View.VISIBLE);
                        layout_empty.setVisibility(View.GONE);

                        adapterAllChat = new AdapterAllChat(ChatRoomsActivity.this, data, new OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) throws JSONException, FileNotFoundException {
                                OpenChat(data.get(position));
                            }
                        });
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recycler_view.setLayoutManager(linearLayoutManager);
                        recycler_view.setAdapter(adapterAllChat);
                        adapterAllChat.notifyDataSetChanged();
                    } else {
                        layout_empty.setVisibility(View.VISIBLE);
                        recycler_view.setVisibility(View.GONE);

                    }
                }
            });

        }
    }


    private void OpenChat(ModelChatUser modelChatUser) {
        if (modelChatUser != null) {
            Intent intent = new Intent(ChatRoomsActivity.this, ChatActivity.class);
            intent.putExtra(RootManager.ROOM_ISEnable, true);
            String chatroom = modelChatUser.getChatRoomId();
            if (!TextUtils.isEmpty(chatroom)) {
                String[] splite = chatroom.split("::");
                if (splite.length > 0) {
                    intent.putExtra(RootManager.ROOM_ID, splite[0] + "");
                }
            }
            intent.putExtra(RootManager.ROOM_IMAGE, modelChatUser.getChatRoomPic() + "");
            intent.putExtra(RootManager.ROOM_NAME, modelChatUser.getChatRoomName() + "");
            startActivityForResult(intent, 202);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && requestCode == 202) {
            getChatUser();
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(ChatRoomsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


}