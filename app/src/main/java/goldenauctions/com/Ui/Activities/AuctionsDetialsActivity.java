package goldenauctions.com.Ui.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.cottacush.android.currencyedittext.CurrencyEditText;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.FirebaseApp;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import es.dmoral.toasty.Toasty;
import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.ChatKit.ChatActivity;
import goldenauctions.com.Hellper.NumberTextWatcher;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Hellper.UtilitiesDate;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.AuctionDetails;
import goldenauctions.com.Medoles.FBLastBid;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.LastBid;
import goldenauctions.com.Medoles.Media;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterAuctionDetails;
import goldenauctions.com.Ui.Adapters.SliderAdapterImages;
import goldenauctions.com.Ui.Fragments.LoginFragment;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import retrofit2.Response;

import static goldenauctions.com.Hellper.UtilitiesDate.convertTime;
import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static goldenauctions.com.Manager.RootManager.enterTransition;
import static goldenauctions.com.Manager.RootManager.returnTransition;

public class AuctionsDetialsActivity extends AppCompatActivity implements OnMapReadyCallback, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private RelativeLayout layout_back;
    private SliderView sliderView;
    private SliderAdapterImages adapterImages;
    private List<Media> mediaList = new ArrayList<>();
    private TextView toolbarNameTxt;
    private TextView text_title;
    private int auction_id = -1;
    private Auction auction = null;
    private TextView text_price, text_date_time, text_price_nav, text_time_nav, text_count_nav, text_description, text_status;
    private GoogleMap mMap;
    private FrameLayout layout_loading_white;
    private ImageView img_fav, img_share, img_laws, img_messages;
    private LottieAnimationView LottieFav;
    private ConnectionManager connectionManager;
    private TextView text_follow_up;
    private BottomSheetBehavior bottomSheetBehavior;
    private ImageButton btnMinus, btnPlus;
    private CurrencyEditText editTextPrice;
    private Button btnAction, btnActionSheet;
    private LinearLayout layout_privacy, layout_highest_bid, layout_highest_bid_price;
    private CheckBox checkBox_privacy;
    private TextView text_privacy;
    FirebaseFirestore db;
    CollectionReference ref;
    private User user;
    private double PlusMinusNumber = 1f;
    private SwipeRefreshLayout swipeRefreshLayout;

    private TextView row_view_detials, text_time_title;

    private String getCollectionName() {
        String name = "";
        name = auction_id + "::" + auction_id;
        return name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_auctions_detials);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(enterTransition());
            getWindow().setSharedElementReturnTransition(returnTransition());
        }
        initSetUp();
    }

    private void initSetUp() {
        connectionManager = new ConnectionManager(this);
        user = PreferenceUser.User(this);
        layout_back = findViewById(R.id.end_main);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        layout_back.setOnClickListener(this::onClick);
        sliderView = findViewById(R.id.imageSlider);
        text_title = findViewById(R.id.text_title);
        layout_loading_white = findViewById(R.id.layout_loading_white);
        img_fav = findViewById(R.id.img_fav);
        img_share = findViewById(R.id.img_share);
        img_laws = findViewById(R.id.img_laws);
        img_messages = findViewById(R.id.img_messages);
        LottieFav = findViewById(R.id.LottieFav);
        btnAction = findViewById(R.id.btnAction);
        text_status = findViewById(R.id.text_status);
        btnAction.setOnClickListener(this::onClick);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);

        row_view_detials = findViewById(R.id.row_view_detials);
        row_view_detials.setOnClickListener(this::onClick);
        text_time_title = findViewById(R.id.text_time_title);
        //sheet view
        btnMinus = findViewById(R.id.btnMinus);
        btnPlus = findViewById(R.id.btnPlus);
        editTextPrice = findViewById(R.id.editTextPrice);
        // editTextPrice.addTextChangedListener(new NumberTextWatcher(editTextPrice, "#,###.00" , getResources().getString(R.string.dollar_code)));
        btnActionSheet = findViewById(R.id.btnActionSheet);
        btnActionSheet.setOnClickListener(this::onClick);
        btnMinus.setOnClickListener(this::onClick);
        btnPlus.setOnClickListener(this::onClick);


        layout_privacy = findViewById(R.id.layout_privacy);
        layout_highest_bid = findViewById(R.id.layout_highest_bid);
        layout_highest_bid_price = findViewById(R.id.layout_highest_bid_price);
        layout_highest_bid_price.setVisibility(View.GONE);
        layout_highest_bid.setVisibility(View.GONE);
        checkBox_privacy = findViewById(R.id.checkBox_privacy);
        text_privacy = findViewById(R.id.text_privacy);
        layout_privacy.setOnClickListener(this::onClick);
        text_privacy.setOnClickListener(this::onClick);
        String m1ar = "<font color='black'>" + getResources().getString(R.string.i_agree_to_the2) + " </font> <font color='blue'>" + getResources().getString(R.string.PrivacyPolicy2) + "</font>";
        text_privacy.setText(Html.fromHtml(m1ar));

        text_follow_up = findViewById(R.id.text_follow_up);

        img_fav.setOnClickListener(this::onClick);
        img_share.setOnClickListener(this::onClick);
        img_laws.setOnClickListener(this::onClick);
        img_messages.setOnClickListener(this::onClick);

        text_price = findViewById(R.id.text_price);
        text_date_time = findViewById(R.id.text_date_time);
        text_price_nav = findViewById(R.id.text_price_nav);
        text_time_nav = findViewById(R.id.text_time_nav);
        text_count_nav = findViewById(R.id.text_count_nav);
        text_description = findViewById(R.id.text_description);

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment)).getMapAsync(this);
        View viewMap = findViewById(R.id.viewMap);


        auction_id = getIntent().getIntExtra("ref_id", -1);


        if (getIntent().getStringExtra("auction_str") != null) {
            String auction_str = getIntent().getStringExtra("auction_str");
            auction = new Gson().fromJson(auction_str, Auction.class);
            setValueToView(auction);
            auction_id = auction.getId();
            GetAuctionById(auction_id);
        } else {
            if (auction_id != -1)
                GetAuctionById(auction_id);
        }
        SetUpBottomSheet();


        FirebaseApp.initializeApp(this);
        db = FirebaseFirestore.getInstance();
        ref = db.collection("aution::").document(getCollectionName()).collection(getCollectionName());

        fetchRealTimeMessages();


    }

    private void fetchRealTimeMessages() {
        ref.whereGreaterThanOrEqualTo("creationDate", Timestamp.now())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {


                        for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
                            if (documentChange.getType() == DocumentChange.Type.ADDED) {
                                extractMessage(documentChange.getDocument());
                            }
                        }
                        firstSound = true;
                    }
                });
    }

    boolean firstSound = true;
    Handler myHandler = null;
    Runnable myRunnable = null;

    private void extractMessage(DocumentSnapshot document) {
        //   long time = System.currentTimeMillis();
        // String id = document.getString("id");
        String user_id = document.getString("user_id");
        //  String userName = document.getString("userName");
        String price = document.getString("price");
        Timestamp creationDate = document.getTimestamp("creationDate");


        LinearLayout layout_realTime = findViewById(R.id.layout_realTime);
        layout_realTime.setVisibility(View.VISIBLE);
        TextView text_realTime_Name = findViewById(R.id.text_realTime_Name);
        TextView text_realTime_Price = findViewById(R.id.text_realTime_Price);
        TextView text_realTime_Date = findViewById(R.id.text_realTime_Date);
        ImageView img_realTime_Close = findViewById(R.id.img_realTime_Close);
        text_realTime_Name.setText(" " + getResources().getString(R.string.New_bid));
        text_realTime_Price.setText(" " + price + " " + getResources().getString(R.string.dollar_code));
        if (firstSound) {
            firstSound = false;
            appMp3Manager = new AppMp3Manager(AuctionsDetialsActivity.this);
            appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
            if (myHandler == null)
                myHandler = new Handler();

            if (myRunnable != null) {
                myHandler.removeCallbacks(myRunnable);
            }
            myRunnable = new Runnable() {
                @Override
                public void run() {
                    YoYo.with(Techniques.FadeOutLeft)
                            .duration(700)
                            .playOn(layout_realTime);
                    layout_realTime.setVisibility(View.GONE);
                }
            };
            myHandler.postDelayed(myRunnable, 6000);
            firstSound = false;


        }
        if (user != null) {
            if (!TextUtils.equals(user_id, user.getId() + "")) {
                GetAuctionById(auction_id);
            }
        } else {
            GetAuctionById(auction_id);
        }
        text_realTime_Date.setText(creationDate.toDate() + "");
        YoYo.with(Techniques.FadeInRight)
                .duration(700)
                .playOn(layout_realTime);

        //   layout_realTime.removeCallbacks(layout_realTime);


        img_realTime_Close.setOnClickListener(view -> {
            YoYo.with(Techniques.FadeOutLeft)
                    .duration(700)
                    .playOn(layout_realTime);

            layout_realTime.setVisibility(View.GONE);

        });


        if (user != null) {
            if (TextUtils.equals(user_id, user.getId() + "")) {
                layout_highest_bid.setVisibility(View.VISIBLE);
                layout_highest_bid_price.setVisibility(View.VISIBLE);
                layout_highest_bid_price.setSelected(true);
            } else {
                layout_highest_bid.setVisibility(View.GONE);
                layout_highest_bid_price.setVisibility(View.GONE);
                layout_highest_bid_price.setSelected(false);
            }

        }

    }

    private void SetUpBottomSheet() {
        LinearLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        llBottomSheet.setOnClickListener(view -> {
            return;
        });
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setPeekHeight(30);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

    }

    private boolean isLoadServer = false;

    private void GetAuctionById(int auction_id) {
        connectionManager.GetAuctionById(auction_id, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                if (response.body() != null) {
                    Response<AuctionResponse> responseResponse = response;
                    if (responseResponse.body().getAuction() != null) {
                        auction = responseResponse.body().getAuction();
                        isLoadServer = true;
                        setValueToView(auction);
                    }
                } else {
                    onBackPressed();
                }
            }
        });
    }

    boolean isStart = false;
    boolean isEnd = false;
    private CountDownTimer mCountDownTimer = null;

    private void setValueToView(Auction auction) {
        layout_loading_white.setVisibility(View.GONE);
        auction_id = auction.getId();
        if (auction.getMediaList() != null) {
            if (auction.getMediaList().size() > 0) {
                SetSlideImages(auction.getMediaList());
            }
        }
        toolbarNameTxt.setText("#" + auction.getCode() + "");

        text_title.setText(auction.getName() + "");


        if (!TextUtils.isEmpty(auction.getMinimum_bid())) {
            if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, auction.getMinimum_bid())) {
                PlusMinusNumber = Float.parseFloat(auction.getMinimum_bid());
            }
        }

        if (auction.getAuctionDetails() != null) {
            if (auction.getAuctionDetails().size() == 0) {
                row_view_detials.setText(getResources().getText(R.string.no_more));
                row_view_detials.setEnabled(false);
            } else {
                row_view_detials.setEnabled(true);
                row_view_detials.setText(getResources().getText(R.string.view));

            }
        } else {
            row_view_detials.setText(getResources().getText(R.string.no_more));
            row_view_detials.setEnabled(false);

        }
        text_price.setText(auction.getPrice() + " " + getResources().getString(R.string.dollar_code));
        text_date_time.setText(auction.getEnd_at() + "");
        text_price_nav.setText(auction.getMinimum_bid() + " " + getResources().getString(R.string.dollar_code));
        text_time_nav.setText(auction.getEnd_at() + "");
        text_count_nav.setText(auction.getBids_count() + " " + getResources().getString(R.string.Auctions));
        text_description.setText(auction.getDescription() + "");
        if (auction.getEnd_at() != null) {
            if (!TextUtils.isEmpty(auction.getEnd_at())) {
                String timeEnd = UtilitiesDate.convertTime(auction.getEnd_at());
                text_time_nav.setText(timeEnd + "");
            }
        }
        /**if (auction.getEnd_at() != null)
         UtilitiesDate.StartCountdownTimer(AuctionsDetialsActivity.this, auction.getEnd_at(), text_date_time, btnAction, true);
         */

        //btnAction.setEnabled(true);
        if (isLoadServer) {


            if (auction.getStart_at() != null) {
                boolean isStart = UtilitiesDate.IsStartAuction(AuctionsDetialsActivity.this, auction.getStart_at(), text_date_time, btnAction);
                if (isStart) {
                    if (auction.getEnd_at() != null) {
                        StartCountdownTimer(AuctionsDetialsActivity.this, auction.getEnd_at(), text_date_time, btnAction, true);
                    }
                } else {
                    running = false;
                    text_time_title.setText("" + getResources().getString(R.string.BeginsOn));
                    text_date_time.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            if (charSequence != null) {
                                String result = charSequence.toString();
                                if (!TextUtils.isEmpty(result)) {
                                    if (TextUtils.equals(result, getResources().getString(R.string.StartNow))) {
                                        if (auction.getEnd_at() != null) {
                                            StartCountdownTimer(AuctionsDetialsActivity.this, auction.getEnd_at(), text_date_time, btnAction, true);
                                        }
                                    }
                                }
                            }

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                }

            }
            boolean isEnabe = RootManager.GetStatusById(AuctionsDetialsActivity.this, auction.getStatus(), text_status);
            if (!isEnabe) {
                //  btnAction.setEnabled(false);
            }
        }


        if (auction.getLastBid() != null) {
            if (auction.getLastBid().getPrice() != null) {
                text_price.setText(auction.getLastBid().getPrice() + " " + getResources().getString(R.string.dollar_code));
            }
        }
        if (auction.isIs_favourite()) {
            img_fav.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
            LottieFav.setVisibility(View.VISIBLE);
            LottieFav.playAnimation();
            text_follow_up.setText(getResources().getString(R.string.UnFollow));
        } else {
            img_fav.setColorFilter(null);
            LottieFav.setVisibility(View.GONE);
            LottieFav.playAnimation();
            text_follow_up.setText(getResources().getString(R.string.FollowUp));
        }

        if (user != null) {
            if (auction.getLastBid() != null) {
                if (auction.getLastBid().getUser_id() == user.getId()) {
                    layout_highest_bid.setVisibility(View.VISIBLE);
                    layout_highest_bid_price.setVisibility(View.VISIBLE);
                    layout_highest_bid_price.setSelected(true);
                } else {
                    layout_highest_bid.setVisibility(View.GONE);
                    layout_highest_bid_price.setVisibility(View.GONE);
                    layout_highest_bid_price.setSelected(false);
                }
            } else {
                layout_highest_bid.setVisibility(View.GONE);
                layout_highest_bid_price.setVisibility(View.GONE);
                layout_highest_bid_price.setSelected(false);
            }
        }
        if (mMap != null) {
            Double Userlatitude = 0.0;
            Double Userlongitude = 0.0;
            String lat = auction.getLat();
            String log = auction.getLng();
            if (!TextUtils.isEmpty(lat))
                Userlatitude = Double.parseDouble(lat);
            if (!TextUtils.isEmpty(log))
                Userlongitude = Double.parseDouble(log);

            if (Userlatitude != 0.0 && Userlongitude != 0.0) {
                LatLng locationMark1 = new LatLng(Userlatitude, Userlongitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationMark1, 16));
                if (auction.getLastBid() == null) {
                    LastPrice = auction.getPrice();
                } else {
                    if (auction.getLastBid().getPrice() != null) {
                        LastPrice = auction.getLastBid().getPrice();
                    } else {
                        LastPrice = auction.getPrice();
                    }
                }
                if (!TextUtils.isEmpty(Userlatitude + "") && !TextUtils.isEmpty(Userlongitude + "")) {
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Userlatitude, Userlongitude))
                            .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(LastPrice + " " + getResources().getString(R.string.dollar_code), getApplicationContext()))));
                    marker.setTag(auction.getPrice());
                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {


                            return false;
                        }
                    });


                }

            }

        }


    }

    private void SetSlideImages(List<Media> mediaList) {
        adapterImages = new SliderAdapterImages(this, mediaList, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                if (TextUtils.equals("view", tag)) {
                    AppErrorsManager.showImage(AuctionsDetialsActivity.this, mediaList, position);
                }
            }
        });
        sliderView.setSliderAdapter(adapterImages);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        //  sliderView.setIndicatorSelectedColor(Color.WHITE);
        //   sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (auction != null) {
            Double Userlatitude = 0.0;
            Double Userlongitude = 0.0;


            String lat = auction.getLat();
            String log = auction.getLng();
            if (!TextUtils.isEmpty(lat))
                Userlatitude = Double.parseDouble(lat);
            if (!TextUtils.isEmpty(log))
                Userlongitude = Double.parseDouble(log);

            if (Userlatitude != 0.0 && Userlongitude != 0.0) {
                LatLng locationMark1 = new LatLng(Userlatitude, Userlongitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationMark1, 16));

                if (!TextUtils.isEmpty(Userlatitude + "") && !TextUtils.isEmpty(Userlongitude + "")) {

                    if (auction.getLastBid() == null) {
                        LastPrice = auction.getPrice();
                    } else {
                        if (auction.getLastBid().getPrice() != null) {
                            LastPrice = auction.getLastBid().getPrice();
                        } else {
                            LastPrice = auction.getPrice();
                        }
                    }


                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Userlatitude, Userlongitude))
                            .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(LastPrice + " " + getResources().getString(R.string.dollar_code), getApplicationContext()))));
                    marker.setTag(auction.getPrice());
                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {


                            return false;
                        }
                    });


                }

            }


        }

    }

    public static Bitmap getMarkerBitmapFromView(String title, Context context) {
        View customMarkerView =
                ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.circle_image_on_map, null);
        LinearLayout layout_title = customMarkerView.findViewById(R.id.layout_title);
        TextView text_map_title_name = customMarkerView.findViewById(R.id.text_map_title_name);
        text_map_title_name.setText("" + title);
        final int random = new Random().nextInt(61) + 20; // [0, 60] + 20 => [20, 80]
        if (random % 2 == 0) {
            layout_title.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            text_map_title_name.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }


        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();

        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);


        customMarkerView.draw(canvas);
        return returnedBitmap;

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.end_main:
                onBackPressed();
                break;
            case R.id.img_messages:
                GoToChatActivity();
                break;
            case R.id.img_laws:
                OpenLawsDialog();
                break;
            case R.id.text_privacy:
                OpenLawsDialog();
                break;
            case R.id.img_share:
                ShareAution();
                break;
            case R.id.img_fav:
                AddToFav();
                break;

            case R.id.btnAction:
                AfterOpenSheet();
                break;

            case R.id.btnActionSheet:
                CreateNewAuctionWebService();
                break;
            case R.id.btnPlus:
                PlusOne();
                break;

            case R.id.btnMinus:
                MinusOne();
                break;
            case R.id.row_view_detials:
                ViewDetialsDialog();
                break;

        }
    }

    AdapterAuctionDetails adapterAuctionDetails;

    private void ViewDetialsDialog() {
        if (auction != null) {
            List<AuctionDetails> auctionDetails = new ArrayList<>();
            if (auction.getAuctionDetails() != null) {

                if (auction.getAuctionDetails().size() > 0) {
                    adapterAuctionDetails = new AdapterAuctionDetails(AuctionsDetialsActivity.this, auction.getAuctionDetails(), -1, R.layout.row_item_auction_details);
                    AppErrorsManager.showAuctionsDetialsDialog(AuctionsDetialsActivity.this, adapterAuctionDetails);
                } else {

                    /** auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                     auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                     adapterAuctionDetails = new AdapterAuctionDetails(AuctionsDetialsActivity.this, auctionDetails, -1, R.layout.row_item_auction_details);
                     AppErrorsManager.showAuctionsDetialsDialog(AuctionsDetialsActivity.this, adapterAuctionDetails);
                     */
                }
            } else {
                /**  auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Color), getResources().getString(R.string.Red)));
                 auctionDetails.add(new AuctionDetails(getResources().getString(R.string.Model), "2020"));

                 adapterAuctionDetails = new AdapterAuctionDetails(AuctionsDetialsActivity.this, auctionDetails, -1, R.layout.row_item_auction_details);
                 AppErrorsManager.showAuctionsDetialsDialog(AuctionsDetialsActivity.this, adapterAuctionDetails);
                 */
            }
        }
    }

    private void MinusOne() {
        String newPrice = editTextPrice.getText().toString().trim();
        if (!TextUtils.isEmpty(newPrice)) {
            Double price = editTextPrice.getNumericValue() - PlusMinusNumber;
            if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, LastPrice)) {
                Double floatLastPrice = Double.parseDouble(LastPrice);
                if (floatLastPrice < price)
                    editTextPrice.setText(price + "");
            }

        }

    }

    private void PlusOne() {
        String newPrice = editTextPrice.getText().toString().trim();
        if (!TextUtils.isEmpty(newPrice)) {
            Double price = editTextPrice.getNumericValue() + PlusMinusNumber;
            if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, LastPrice)) {
                Double floatLastPrice = Double.parseDouble(LastPrice);
                if (floatLastPrice < price)
                    editTextPrice.setText(price + "");

            }
        }
    }

    private void CreateNewAuctionWebService() {
        text_privacy.clearFocus();
        text_privacy.setError(null);
        editTextPrice.clearFocus();
        editTextPrice.setError(null);
        String newPrice = editTextPrice.getText().toString().trim();
        if (TextUtils.isEmpty(newPrice)) {
            editTextPrice.requestFocus();
            editTextPrice.setError(getResources().getString(R.string.required_field));
        } else if (!checkBox_privacy.isChecked()) {
            text_privacy.requestFocus();
            text_privacy.setError("");
        } else {
            if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, newPrice)) {

                Double price = editTextPrice.getNumericValue();

                if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, LastPrice)) {
                    Double floatLastPrice = Double.parseDouble(LastPrice);
                    if (floatLastPrice < price) {

                        int price_int = (int) Math.round(price);
                        Log.e("lastpr", price_int + " #" + floatLastPrice + " < " + price);
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        connectionManager.AddNewBid(auction.getId(), price_int + "", new CallbackConncation() {
                            @Override
                            public void onResponse(Response response) {
                                if (response.body() != null) {
                                    IsUpdate = true;
                                    Response<AuctionResponse> auctionResponse = response;
                                    if (auctionResponse.body() != null) {
                                        String message = auctionResponse.body().getStatus().getMessageList();

                                        if (auctionResponse.body().getAuction() != null) {
                                            auction = auctionResponse.body().getAuction();
                                            AddFireBaseAuctions(auction.getLastBid());
                                            setValueToView(auction);
                                        }
                                    }
                                }
                            }
                        });
                    } else {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        AppErrorsManager.showCustomErrorDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.error),
                                getResources().getString(R.string.bid_less_required));
                    }
                }
            } else {
                AppErrorsManager.showCustomErrorDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.error),
                        getResources().getString(R.string.ErrorNewPrice));

            }

        }
    }


    private void AddFireBaseAuctions(LastBid lastBid) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", lastBid.getId() + "");
        hashMap.put("user_id", lastBid.getUser().getId() + "");
        hashMap.put("userName", lastBid.getUser().getName() + "");
        hashMap.put("price", lastBid.getPrice() + "");
        hashMap.put("creationDate", FieldValue.serverTimestamp());

        ref.add(hashMap);
    }

    String LastPrice = "";

    private void AfterOpenSheet() {
        isStart = UtilitiesDate.IsStartAuctionCheck(auction.getStart_at());
        isEnd = UtilitiesDate.IsEndAuctionCheck(auction.getEnd_at());
        if (!isStart) {
            AppErrorsManager.showCustomErrorDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.MsgNotStartAuction));
        } else if (isEnd) {
            AppErrorsManager.showCustomErrorDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.MsgEndAuction));

        } else {
            user = PreferenceUser.User(this);
            if (user != null) {
                if (user.getEmail_verified_at() != null || user.getMobile_verified_at() != null) {

                    OpenBottomSheet();
                } else {
                    AppErrorsManager.showTwoActionDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.info),
                            getResources().getString(R.string.ActiveEmailAfter), getResources().getString(R.string.ActiveNow), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    if (TextUtils.equals("yes", status)) {
                                        Intent intent = new Intent(AuctionsDetialsActivity.this, UserActionActivity.class);
                                        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.VerifyEmail));
                                        startActivity(intent);
                                    }
                                }
                            });
                }
            } else {
                AppErrorsManager.showLoginToContinueDialog(AuctionsDetialsActivity.this, new InstallCallback() {
                    @Override
                    public void onStatusDone(String status) {
                        if (TextUtils.equals("yes", status)) {
                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        }
    }

    private void OpenBottomSheet() {
        if (auction != null) {
            if (auction.getLastBid() == null) {
                if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, auction.getPrice())) {
                    Double price = Double.parseDouble(auction.getPrice());
                    editTextPrice.setText(price + "");
                }
                LastPrice = auction.getPrice();
            } else {
                if (auction.getLastBid().getPrice() != null) {
                    LastPrice = auction.getLastBid().getPrice();
                    editTextPrice.setText("" + LastPrice);
                    if (RootManager.IsTextIsDoble(AuctionsDetialsActivity.this, LastPrice)) {
                        Double price = Double.parseDouble(LastPrice);
                        editTextPrice.setText(price + "");
                    }
                } else {
                    LastPrice = auction.getPrice();
                }
            }
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            PlusOne();
        }
    }


    boolean IsUpdate = false;

    @Override
    public void onBackPressed() {
        if (IsUpdate) {
            Intent intent = getIntent();
            intent.putExtra("IsUpdate", true);
            setResult(202, intent);
            finish();
        } else
            super.onBackPressed();

    }

    private void AddToFav() {
        if (user != null) {
            if (auction != null) {
                LottieFav.playAnimation();
                connectionManager.FavouriteAuctionsToggle(auction.getId(), new CallbackConncation() {
                    @Override
                    public void onResponse(Response response) {
                        if (response.body() != null) {
                            IsUpdate = true;
                            Response<AuctionResponse> auctionResponse = response;
                            if (auctionResponse.body() != null) {
                                String message = auctionResponse.body().getStatus().getMessageList();
                                AppErrorsManager.showCustomErrorDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.info),
                                        message + "");
                                if (auctionResponse.body().getAuction() != null) {
                                    auction = auctionResponse.body().getAuction();
                                    setValueToView(auction);
                                }
                            }
                        }
                    }
                });
            }
        } else {
            AppErrorsManager.showLoginToContinueDialog(AuctionsDetialsActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    private void ShareAution() {
        if (auction != null) {
            Toast.makeText(this, "" + getResources().getString(R.string.wait), Toast.LENGTH_SHORT).show();
            if (auction.getMediaList().size() > 0)
                RootManager.shareImageProfile(auction.getMediaList().get(0).getFile() + "", " : " + auction.getName(), AuctionsDetialsActivity.this);

        }
    }

    private void OpenLawsDialog() {

        if (auction != null) {
            if (!TextUtils.isEmpty(auction.getTerms_conditions())) {
                AppErrorsManager.showTermUseDialog(AuctionsDetialsActivity.this, getResources().getString(R.string.Termsofuse),
                        auction.getTerms_conditions());
            }
        } else {
            Intent intent = new Intent(getApplicationContext(), UserActionActivity.class);
            intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.Termsofuse));
            startActivity(intent);
        }

    }

    private void GoToChatActivity() {
        if (user != null) {
            OpenChat();
        } else {
            AppErrorsManager.showLoginToContinueDialog(AuctionsDetialsActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public void onRefresh() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                if (auction != null)
                    GetAuctionById(auction.getId());

            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    GetAuctionById(auction.getId());
                }
            }, 1000);
        }
    };

    private void OpenChat() {
        if (auction != null) {
            Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
            intent.putExtra(RootManager.ROOM_ISEnable, true);
            intent.putExtra(RootManager.ROOM_ID, auction.getId() + "");
            if (auction.getMediaList().size() > 0)
                intent.putExtra(RootManager.ROOM_IMAGE, auction.getMediaList().get(0).getFile() + "");
            intent.putExtra(RootManager.ROOM_NAME, auction.getName() + "");
            startActivityForResult(intent, 202);

        }

    }

    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


    int cooooo = 0;
    private boolean running = false;

    private void StartCountdownTimer(Context context, String endTime, TextView textView, View view, boolean IsCountdown) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);
        endTime = convertTime(endTime);
        final Long[] startTime = {0L};
        long milliseconds = 0;

        Log.e("StartCountdownTimer", "Firest");

        Date endDate = null;
        try {
            endDate = formatter.parse(endTime);
            textView.setText(endDate.toString());
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //   startTime[0] = System.currentTimeMillis();

        startTime[0] = UtilitiesDate.getTimeMillsByZoneDubai();


        textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        view.setEnabled(true);

        final boolean[] isCounterOldRun = {false};
        final boolean[] Stop = {false};

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            cooooo = 0;
            Log.e("StartCountdownTimer", true + " is != nulll ");
        } else {
            Log.e("StartCountdownTimer", false + " is != nulll ");

        }
        if (IsCountdown) {
            Log.e("StartCountdownTimer", IsCountdown + " IsCountdown");
            String finalEndTime = endTime;
            running = true;
            mCountDownTimer = new CountDownTimer(milliseconds, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (running) {
                        cooooo++;
                        startTime[0] = startTime[0] - 1;


                        Long serverUptimeSeconds =
                                (millisUntilFinished - startTime[0]) / 1000;

                        Log.e("StartCountdownTimer", isCounterOldRun[0] + " inside" + " # " + cooooo + " # " + serverUptimeSeconds);

                        String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
                        //txtViewDays.setText(daysLeft);
                        Log.d("daysLeft", daysLeft);
                        if (!TextUtils.isEmpty(daysLeft)) {
                            daysLeft = daysLeft + "" + context.getResources().getString(R.string.Day) + " ";
                        }

                        String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);
                        //txtViewHours.setText(hoursLeft);
                        Log.d("hoursLeft", hoursLeft);
                        if (!TextUtils.isEmpty(hoursLeft)) {
                            hoursLeft = hoursLeft + "" + context.getResources().getString(R.string.Hours) + " ";
                        }

                        String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
                        //txtViewMinutes.setText(minutesLeft);
                        Log.d("minutesLeft", minutesLeft);
                        if (!TextUtils.isEmpty(minutesLeft)) {
                            minutesLeft = minutesLeft + "" + context.getResources().getString(R.string.Minutes) + " ";
                        }

                        String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
                        //txtViewSecond.setText(secondsLeft);
                        Log.d("secondsLeft", secondsLeft);
                        if (!TextUtils.isEmpty(secondsLeft)) {
                            secondsLeft = secondsLeft + "" + context.getResources().getString(R.string.seconds) + " ";

                        }
                        if (Stop[0] == true) {
                            textView.setTextColor(context.getResources().getColor(R.color.colorRed));
                            textView.setText(finalEndTime + "");
                            text_time_title.setText("" + getResources().getString(R.string.Since_ended));

                            return;
                        }
                        if (serverUptimeSeconds < 0) {
                            Stop[0] = true;
                        } else {
                            textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                            textView.setText(daysLeft + hoursLeft + minutesLeft + secondsLeft);
                            text_time_title.setText("" + getResources().getString(R.string.RemainingTime));
                        }
                    }
                }

                @Override
                public void onFinish() {
                    Log.e("StartCountdownTimer", " onFinish");

                }
            };
            mCountDownTimer.start();

        } else {
            running = false;

            if (mCountDownTimer != null) {
                mCountDownTimer.cancel();

                cooooo = 0;
            }
            Long serverUptimeSeconds =
                    (milliseconds - startTime[0]) / 1000;


            String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
            if (!TextUtils.isEmpty(daysLeft)) {
                daysLeft = daysLeft + "" + context.getResources().getString(R.string.Day) + " ";
            }

            String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);
            if (!TextUtils.isEmpty(hoursLeft)) {
                hoursLeft = hoursLeft + "" + context.getResources().getString(R.string.Hours) + " ";
            }

            String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
            if (!TextUtils.isEmpty(minutesLeft)) {
                minutesLeft = minutesLeft + "" + context.getResources().getString(R.string.Minutes) + " ";
            }

            String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
            if (!TextUtils.isEmpty(secondsLeft)) {
                secondsLeft = secondsLeft + "" + context.getResources().getString(R.string.seconds) + " ";

            }
            if (serverUptimeSeconds < 0) {
                textView.setTextColor(context.getResources().getColor(R.color.colorRed));
                textView.setText(endTime + "");
                text_time_title.setText("" + getResources().getString(R.string.Since_ended));

            } else {
                textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                textView.setText(daysLeft + hoursLeft + minutesLeft + secondsLeft);
                text_time_title.setText("" + getResources().getString(R.string.RemainingTime));
            }
        }


    }
}