package goldenauctions.com.Ui.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.InstallTwoStringCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.Ticket;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterTicket;
import goldenauctions.com.Ui.Adapters.RecyclerViewScrollListener;
import goldenauctions.com.WebService.model.response.RootResponse;
import goldenauctions.com.WebService.model.response.TicketsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;


public class TicketsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recycler_view;
    LinearLayout layout_empty;
    AdapterTicket adapter;
    private FrameLayout layout_loading;
    List<Ticket> ticketList = new ArrayList<>();
    FrameLayout layout_back;
    TextView toolbarNameTxt;
    RecyclerRefreshLayout swipeRefreshLayout;
    boolean IsLodeMore = false;
    private RecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    private FloatingActionButton fab;
    int page = 1;
    int per_page = 10;

    User user = new User();
    private ConnectionManager connectionManager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_ticket);
        user = PreferenceUser.User(this);
        connectionManager = new ConnectionManager(this);
        initView();

    }


    private void initView() {

        recycler_view = findViewById(R.id.recycler_view);
        layout_empty = findViewById(R.id.layout_empty);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            OpenDialogCreateNewTicket();
        });
        layout_back = findViewById(R.id.layout_toolbar_menu);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText("" + getString(R.string.Tickets));
        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });
        TicketsWebSevice(page, per_page);

    }
    private void SetLodeMore() {
        scrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page_, int totalItemsCount) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (IsLodeMore) {
                            page++;
                            TicketsWebSevice(page, per_page);
                        }

                    }
                }, 1000);

            }

            @Override
            public void onReachTop(boolean isFirst) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        recycler_view.addOnScrollListener(scrollListener);
    }



    private void TicketsWebSevice(int page, int per_page) {
        connectionManager.GetListTickets(page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response resp) {
                if (resp != null) {
                    Response<TicketsResponse> response = resp;
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        String message_str = response.body().getStatus().getMessageList();
                        if ("success".equals(response.body().getStatus().getStatus())) {
                            if (page == 1) {
                                ticketList = response.body().getTickets();
                                if (ticketList != null) {
                                    setupRecycler();

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                ticketList.addAll(response.body().getTickets());
                                adapter.notifyDataSetChanged();
                            }

                            if (ticketList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (response.body().getPaging() != null) {
                                    if (ticketList.size() >= response.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if ("error".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(TicketsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        } else if ("fail".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(TicketsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        }

                    }

                }
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (Isupdate) {
            Intent intent = new Intent();
            intent.putExtra("update", "update");
            setResult(999, intent);
            finish();
        } else
            super.onBackPressed();
    }

    public void setupRecycler() {
        adapter = new AdapterTicket(TicketsActivity.this, ticketList, R.layout.row_ticket, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                       String str_ticket =new Gson().toJson(ticketList.get(position));
                        Intent intent = new Intent(getApplicationContext() ,TicketDetialsActivity.class);
                        intent.putExtra("ref_id", ticketList.get(position).getId());
                        intent.putExtra("str_ticket",str_ticket);
                        startActivity(intent);
                    }
                });
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        SetLodeMore();
    }

    boolean Isupdate = false;


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                page = 1;
                TicketsWebSevice(page, per_page);
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(TicketsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(TicketsActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            page = 1;
                            TicketsWebSevice(page, per_page);
                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

    private void OpenDialogCreateNewTicket() {
        AppErrorsManager.showAddTicketDialog(TicketsActivity.this, new InstallTwoStringCallback() {
            @Override
            public void onStatusDone(String v1, String v2) {
                if (!TextUtils.isEmpty(v1) && !TextUtils.isEmpty(v2)) {
                    CreateNewTicketWebService(v1, v2);
                }
            }
        });
    }

    private void CreateNewTicketWebService(String v1, String v2) {
        connectionManager.AddTickets(v1, v2, new CallbackConncation() {
            @Override
            public void onResponse(Response resp) {
                if (resp != null) {
                    Response<RootResponse> response = resp;
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        List<String> message = response.body().getStatus().getMessage();
                        String message_str = response.body().getStatus().getMessageList();
                        if ("success".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialogNotCancel(TicketsActivity.this,
                                    getResources().getString(R.string.info), message_str, new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                            page = 1;
                                            TicketsWebSevice(page, per_page);
                                        }
                                    });
                        } else if ("error".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(TicketsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        } else if ("fail".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(TicketsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(TicketsActivity.this,
                                getResources().getString(R.string.error), response.message() + "");
                    }

                }
            }
        });

    }
}
