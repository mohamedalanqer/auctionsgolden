package goldenauctions.com.Ui.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.R;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class WebViewActivity extends AppCompatActivity {
    private WebView webView;
    private RelativeLayout layout_back;
    private TextView toolbarNameTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_web_view);

        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText("" + getResources().getString(R.string.Online_chat));
        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://tawk.to/chat/5faffb50c52f660e89737bb0/default");

        layout_back = findViewById(R.id.end_main);
        layout_back.setOnClickListener(view -> {
            onBackPressed();
        });
        webView.getSettings().setJavaScriptEnabled(true);

        toolbarNameTxt.setText("" + getResources().getString(R.string.Online_chat));
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript("enable();", null);
        } else {
            webView.loadUrl("javascript:enable();");
        }

        WebSettings webSettings = webView.getSettings();
        String lang = AppLanguage.getLanguage(this);
        webSettings.setUserAgentString(String.valueOf(lang));

        AppLanguage.setContentLang(this);
        toolbarNameTxt.setText("" + getResources().getString(R.string.Online_chat));


    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(WebViewActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    AppErrorsManager.showCustomErrorDialogTop(WebViewActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            webView.loadUrl("http://golden-auctions.com/livechat.html");


                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };



    @Override
    protected void onRestart() {
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        AppLanguage.setContentLang(this);
        super.onBackPressed();
    }
}