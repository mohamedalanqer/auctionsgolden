package goldenauctions.com.Ui.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.WebService.model.response.InstallResponse;
import goldenauctions.com.WebService.model.response.LoginResponse;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private static String ref_id = "";
    private static String ref_type = "";
    private ConnectionManager connectionManager;
    private ImageView image_logo ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_splash);
        connectionManager = new ConnectionManager(this);
      //  image_logo = findViewById(R.id.image_logo);


        // Glide.with(this).load(R.drawable.logo_gif).into(image_logo);

        initSetUp();

    }


    private void initSetUp() {
        ref_id = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (!TextUtils.isEmpty(bundle.getString("type"))) {
                ref_type = bundle.getString("type");
                if (TextUtils.equals("2", bundle.getString("type")) || TextUtils.equals("3", bundle.getString("type"))) {
                    if (!TextUtils.isEmpty(bundle.getString("ref_id"))) {
                        ref_id = bundle.getString("ref_id");

                    }
                }
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // getUserDetials.GoToActivityMain(SplashActivity.this, null);
                GetInstallWebService();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void GetInstallWebService() {
        connectionManager.GetInstall(new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                Response<InstallResponse> responseResponse = response;
                InstallClass installClass = responseResponse.body().data;
                Gson gson = new Gson();
                String install = gson.toJson(installClass);
                AppPreferences.saveString(SplashActivity.this, "install", install);
                User user = PreferenceUser.User(SplashActivity.this);
                if (user != null) {
                    GetProfileWebServie();

                } else {
                    PreferenceUser.GoToActivityMain(SplashActivity.this, null);
                }
            }
        });
    }


    private void GetProfileWebServie() {
        connectionManager.GetProfile(new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                Response<LoginResponse> responseResponse = response;
                if (RootManager.RESPONSE_CODE_OK == response.code()) {

                    if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "success")) {
                        User user = responseResponse.body().getUser();
                        Gson json = new Gson();
                        String userJson = json.toJson(user);
                        AppPreferences.saveString(SplashActivity.this, "userJson", userJson);
                        if (!userJson.isEmpty()) {
                            if (user != null) {
                                if(user.getApp_locale() != null){
                                 if(TextUtils.equals(user.getApp_locale(),"en")){
                                     AppLanguage.saveLanguage(getApplicationContext(), "en");
                                 }else if(TextUtils.equals(user.getApp_locale(),"ar")){
                                     AppLanguage.saveLanguage(getApplicationContext(), "ar");
                                 }
                                }
                                if (!TextUtils.isEmpty(ref_id)) {
                                    if (TextUtils.equals("2", ref_type)) {
                                        Intent intent = new Intent(SplashActivity.this, AuctionsDetialsActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        int id = Integer.parseInt(ref_id);
                                        intent.putExtra("ref_id", id);
                                        PendingIntent pendingIntent = PendingIntent.getActivity(SplashActivity.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                                        startActivity(intent);
                                        finish();
                                    } else if (TextUtils.equals("3", ref_type)) {
                                        Intent intent = new Intent(SplashActivity.this, TicketDetialsActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        int id = Integer.parseInt(ref_id);
                                        intent.putExtra("ref_id", id);
                                        Log.e("ref_id", ref_id);
                                        PendingIntent pendingIntent = PendingIntent.getActivity(SplashActivity.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    PreferenceUser.GoToActivityMain(SplashActivity.this, user);

                                }

                            } else {
                                PreferenceUser.GoToActivityMain(SplashActivity.this, null);
                            }
                        }
                    } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                        PreferenceUser.GoToActivityMain(SplashActivity.this, null);
                    } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                        PreferenceUser.GoToActivityMain(SplashActivity.this, null);
                    }
                }


            }
        });
    }
}