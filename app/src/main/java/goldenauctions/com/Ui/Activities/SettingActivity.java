package goldenauctions.com.Ui.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.SettingItem;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterSetting;
import goldenauctions.com.WebService.model.response.RootResponse;
import retrofit2.Response;

public class SettingActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private RelativeLayout layout_back;
    private TextView toolbarNameTxt;
    private AdapterSetting adapter;
    private RecyclerView recycler_view;
    private ImageView img_avatar;
    private TextView textNameProfile, textEmailProfile;
    private RecyclerRefreshLayout swipeRefreshLayout;
    private User user = new User();
    private ConnectionManager connectionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_setting);
        initSetUp();
    }

    private void initSetUp() {
        layout_back = findViewById(R.id.end_main);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.GONE);
        layout_back.setOnClickListener(
                view -> {
                    onBackPressed();
                }
        );

        connectionManager = new ConnectionManager(this);
        user = PreferenceUser.User(this);
        recycler_view = findViewById(R.id.recyclerView);
        textNameProfile = findViewById(R.id.textNameProfile);
        textEmailProfile = findViewById(R.id.textEmailProfile);
        img_avatar = findViewById(R.id.img_avatar);
        setupRecycler();
        img_avatar.setImageResource(R.drawable.ic_person_24);
        if (user != null) {
            textNameProfile.setText("" + user.getName());
            textEmailProfile.setText("" + user.getEmail());
            if (user.getAvatar() != null) {
                if (!TextUtils.isEmpty(user.getAvatar()))
                    if (isAttached)
                        Glide.with(this).load(user.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
            }
        }
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
    }

    boolean isAttached = true;

    @Override
    public void onAttachedToWindow() {
        isAttached = true;
        super.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        isAttached = false;
        super.onDetachedFromWindow();
    }

    public void setupRecycler() {
        List<SettingItem> settingItems = new ArrayList<>();
        String lang = AppLanguage.getLanguage(this);


        settingItems.add(new SettingItem(1, getResources().getString(R.string.Insurance_packages), R.drawable.ic_pay_list));
        settingItems.add(new SettingItem(2, getResources().getString(R.string.Insurance_refund_request), R.drawable.ic_nsurance_request_list));

        //  settingItems.add(new SettingItem(8, getResources().getString(R.string.EditProfile), R.drawable.ic_docs_list));
        settingItems.add(new SettingItem(3, getResources().getString(R.string.PrivacyPolicy), R.drawable.ic_user_profile));
        settingItems.add(new SettingItem(4, getResources().getString(R.string.Document_folder), R.drawable.ic_docs_list));
        settingItems.add(new SettingItem(5, getResources().getString(R.string.Online_chat), R.drawable.ic_comment));
        settingItems.add(new SettingItem(6, getResources().getString(R.string.Contact_us), R.drawable.ic_phone_list));
        settingItems.add(new SettingItem(7, getResources().getString(R.string.logout), R.drawable.ic_logout_list));


        adapter = new AdapterSetting(this, settingItems, -1, "setting", R.layout.row_item_setting, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                SettingItem settingItem = settingItems.get(position);
                if (TextUtils.equals("view", tag)) {
                    adapter.ItemSelect = position;
                    int id = settingItem.getId();
                    if (id == 1) {
                        GoToInsurancePackages();
                    } else if (id == 2) {
                        OpenDialogRequestRefund();
                    } else if (id == 3) {
                        GoToPrivacyActivity();
                    } else if (id == 4) {
                        GoToMyDocumentsActivity();
                    } else if (id == 5) {
                        GoToRoomsChat();
                    } else if (id == 6) {
                        GoToTicketActivity();
                    } else if (id == 7) {
                        logOut();
                    }

                }

                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GoToRoomsChat() {
        Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
        startActivity(intent);
    }

    private void GoToMyDocumentsActivity() {
        Intent intent = new Intent(this, DocumentsActivity.class);
        startActivity(intent);
    }

    private void EditProfile() {
        Intent intent = new Intent(this, UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.EditProfile));
        startActivity(intent);
    }

    private void GoToTicketActivity() {
        Intent intent = new Intent(this, TicketsActivity.class);
        startActivity(intent);
    }

    private void GoToInsurancePackages() {
        Intent intent = new Intent(this, UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.Insurance_packages));
        startActivity(intent);
    }

    private void GoToPrivacyActivity() {
        Intent intent = new Intent(this, UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.PrivacyPolicy));
        startActivity(intent);
    }

    private void OpenDialogRequestRefund() {
        if (user != null) {
                if (user.getSubscriptions().size() > 0) {
                    AppErrorsManager.showTwoActionDialog(SettingActivity.this, getResources().getString(R.string.request_refund),
                            getResources().getString(R.string.request_refund_message), getResources().getString(R.string.request_refund),
                            new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    if (TextUtils.equals("yes", status)) {
                                        RequestRefund();
                                    }
                                }
                            });
                }else{
                    AppErrorsManager.showCustomErrorDialog(SettingActivity.this,getResources().getString(R.string.request_refund) ,
                            getResources().getString(R.string.cantSendRequestRefund));
                }
        }
    }

    private void RequestRefund() {
        connectionManager.RequestRefund(new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                if (response != null) {
                    if (response.body() != null) {
                        Response<RootResponse> rootResponseResponse = response;
                        if (rootResponseResponse.body().getStatus() != null) {
                            AppErrorsManager.showCustomErrorDialog(SettingActivity.this, getResources().getString(R.string.info),
                                    rootResponseResponse.body().getStatus().getMessageList() + "");
                        }
                    }
                }
            }
        });
    }

    public void logOut() {
        User user = PreferenceUser.User(this);
        if (user != null) {
            AppErrorsManager.showSuccessDialog(this, getString(R.string.logout), getString(R.string.do_you_want_logout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    connectionManager.logout(new CallbackConncation() {
                        @Override
                        public void onResponse(Response response) {
                            Gson json = new Gson();
                            AppPreferences.saveString(SettingActivity.this, "userJson", "0");
                            Intent intent = new Intent(SettingActivity.this, SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        } else {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
        }


    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && resultCode == 202) {
            if (user != null) {
                textNameProfile.setText("" + user.getName());
                if (user.getAvatar() != null) {
                    if (!TextUtils.isEmpty(user.getAvatar()))
                        if (isAttached)
                            Glide.with(this).load(user.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
                    textNameProfile.setText("" + user.getName());


                }
            }

        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
                User user = PreferenceUser.User(SettingActivity.this);
                if (user != null) {
                    textNameProfile.setText("" + user.getName());
                    textEmailProfile.setText("" + user.getEmail());
                    if (user.getAvatar() != null) {
                        if (!TextUtils.isEmpty(user.getAvatar()))
                            if (isAttached)
                                Glide.with(SettingActivity.this).load(user.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
                    }
                }

            }
        }, 2000);
    }
}