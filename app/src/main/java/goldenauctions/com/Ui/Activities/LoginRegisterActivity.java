package goldenauctions.com.Ui.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Fragments.LoginFragment;
import goldenauctions.com.Ui.Fragments.RegisterFragment;

public class LoginRegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout layout_back ;
    private TextView title_fragment ;
    private TextView toolbarNameTxt ;
    private LinearLayout layout_status_bar ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_login);
        initSetUp();
    }

    private void initSetUp() {
        layout_back = findViewById(R.id.end_main);
        title_fragment = findViewById(R.id.title_fragment);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        layout_back.setOnClickListener(this::onClick);
        openFragment(new LoginFragment());

        layout_status_bar = findViewById(R.id.layout_status_bar);
        int Height = getStatusBarHeight();
        layout_status_bar.setVisibility(View.VISIBLE);
        layout_status_bar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Height));
    }
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    String NameFragment = "Login";

    public void openFragment(Fragment fragment) {
        if (fragment instanceof LoginFragment) {
            NameFragment = "Login";
            YoYo.with(Techniques.BounceInLeft)
                    .duration(700)
                    .playOn(findViewById(R.id.fragment));
            title_fragment.setText(R.string.Please_login);
            toolbarNameTxt.setText(R.string.Login);
        } else {
            NameFragment = "Register";
            YoYo.with(Techniques.BounceInRight)
                    .duration(700)
                    .playOn(findViewById(R.id.fragment));
            title_fragment.setText(R.string.Please_register);
            toolbarNameTxt.setText(R.string.CreateAccount);
        }
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();

    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.end_main:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.equals("Login", NameFragment)) {
            openFragment(new LoginFragment());
        } else {
            super.onBackPressed();
        }
    }
}