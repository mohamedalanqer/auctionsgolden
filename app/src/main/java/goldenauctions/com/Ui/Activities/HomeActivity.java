package goldenauctions.com.Ui.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.booking.rtlviewpager.RtlViewPager;
import com.bumptech.glide.Glide;
import com.dinuscxj.refresh.RefreshView;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import java.util.Locale;

import es.dmoral.toasty.Toasty;
import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Hellper.DrawableRound;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.ViewPagerAdapter;
import goldenauctions.com.Ui.Fragments.FollowUpFragment;
import goldenauctions.com.Ui.Fragments.HomeFragment;
import goldenauctions.com.Ui.Fragments.MyBidsFragment;
import goldenauctions.com.Ui.Fragments.MyPurchasesFragment;
import goldenauctions.com.Ui.Fragments.ProfileFragment;
import goldenauctions.com.WebService.model.response.LoginResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static goldenauctions.com.Manager.RootManager.enterTransition;
import static goldenauctions.com.Manager.RootManager.returnTransition;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FrameLayout layout_loading;
    private RelativeLayout Layout_notification;
    private ImageView image_count_notifications;
    private RtlViewPager viewPager;
    private static int curantitem = 0;
    private BottomNavigationView bottomNavigationView;
    private TextView toolbarNameTxt;
    private User user = new User();
    private ConnectionManager connectionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(enterTransition());
            getWindow().setSharedElementReturnTransition(returnTransition());
        }
        initSetup();
    }

    private void initSetup() {
        user = PreferenceUser.User(this);
        if (user != null) {
            Log.e("userlang", user.getApp_locale() + " lang");
        }
        connectionManager = new ConnectionManager(this);
        image_count_notifications = findViewById(R.id.image_count_notifications);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        initSetupDrawer();
        toolbarNameTxt.setText(getResources().getString(R.string.menu_home));
        viewPager = findViewById(R.id.vp);
        viewPager.setOffscreenPageLimit(3);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        setupViewPager(viewPager);
        viewPager.setCurrentItem(curantitem);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.nav_home:
                                viewPager.setCurrentItem(0);

                                break;
                            case R.id.nav_auctions:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.nav_follow_ups:
                                viewPager.setCurrentItem(2);
                                break;
                            case R.id.nav_cart:
                                viewPager.setCurrentItem(3);
                                break;
                            case R.id.nav_profile:
                                viewPager.setCurrentItem(4);
                                break;


                        }
                        return false;
                    }
                });

        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setOnClickListener(view -> {
            GoToNotifications();
        });

        RefreshNotification();
        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });
    }

    private void RefreshNotification() {
        if (user != null) {
            if (user.getNotification_count() > 0) {
                image_count_notifications.setVisibility(View.VISIBLE);
                DrawableRound.TextToDrawableRound(HomeActivity.this, image_count_notifications, user.getNotification_count() + "");
            }
        } else {
            image_count_notifications.setVisibility(View.GONE);
            image_count_notifications.setImageDrawable(null);

        }
    }

    private void GoToNotifications() {
        if (user != null) {
            Intent intent = new Intent(HomeActivity.this, NotificationsActivity.class);
            startActivityForResult(intent, 888);
        } else {
            AppErrorsManager.showLoginToContinueDialog(HomeActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }


    }


    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;

    private void initSetupDrawer() {
        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        toggle.setDrawerIndicatorEnabled(false);

        int resMenu = R.drawable.ic_menu_24;
        drawer.addDrawerListener(toggle);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), resMenu, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);


        toggle.syncState();
        toggle.setToolbarNavigationClickListener(v -> {
            if (curantitem == 0) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);

                }
            } else {
                drawer.closeDrawer(GravityCompat.START);
                onBackPressed();
            }

        });
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        if (user == null) {
            Menu menu = navigationView.getMenu();
            MenuItem tools = menu.findItem(R.id.nav_logout);
            tools.setTitle(R.string.Login);
        }

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        LinearLayout layout_name = navigationView.getHeaderView(0).findViewById(R.id.layout_name);
        TextView textNameProfile = navigationView.getHeaderView(0).findViewById(R.id.textNameProfile);
        ImageView img_avatar = navigationView.getHeaderView(0).findViewById(R.id.img_avatar);
        ImageView textIconEditProfile = navigationView.getHeaderView(0).findViewById(R.id.textIconEditProfile);

        if (user != null) {
            textNameProfile.setText("" + user.getName());
            if (user.getAvatar() != null) {
                if (!TextUtils.isEmpty(user.getAvatar())) {
                    Glide.with(this).load(user.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
                }
            }
        } else {
            textNameProfile.setText("" + getResources().getString(R.string.visitor));
            textIconEditProfile.setVisibility(View.GONE);
        }
        layout_name.setOnClickListener(view -> {
            GoToEditProfile();
        });

    }

    private void GoToEditProfile() {
        if (user != null) {
            Intent intent = new Intent(this, UserActionActivity.class);
            intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.EditProfile));
            startActivityForResult(intent, 208);
        } else {
            AppErrorsManager.showLoginToContinueDialog(HomeActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }


    }

    private void GoToPrivacy() {
        Intent intent = new Intent(getApplicationContext(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.PrivacyPolicy));
        startActivity(intent);
    }

    private void GoToTerms() {
        Intent intent = new Intent(getApplicationContext(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.Termsofuse));
        startActivity(intent);
    }

    private MenuItem prevMenuItem;

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), getString(R.string.menu_home));
        adapter.addFrag(new MyBidsFragment(), getString(R.string.menu_auctions));
        adapter.addFrag(new FollowUpFragment(), getString(R.string.menu_follow_ups));
        adapter.addFrag(new MyPurchasesFragment(), getString(R.string.menu_my_purchases));
        adapter.addFrag(new ProfileFragment(), getString(R.string.Profile));

        final String[] tabStrings = {getString(R.string.menu_home),
                getString(R.string.menu_auctions),
                getString(R.string.menu_follow_ups),
                getString(R.string.menu_my_purchases),
                getString(R.string.Profile)};

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                curantitem = i;
                toolbarNameTxt.setText(tabStrings[i]);
                if (i == 0) {
                    int resMenu = R.drawable.ic_menu_24;
                    Drawable drawable = ResourcesCompat.getDrawable(getResources(), resMenu, HomeActivity.this.getTheme());
                    toggle.setHomeAsUpIndicator(drawable);
                } else {
                    int resMenu = R.drawable.ic_back_home;
                    Drawable drawable = ResourcesCompat.getDrawable(getResources(), resMenu, HomeActivity.this.getTheme());
                    toggle.setHomeAsUpIndicator(drawable);
                }
                if (prevMenuItem != null)
                    prevMenuItem.setChecked(false);
                else
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);

                bottomNavigationView.getMenu().getItem(i).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    public void refreshActivty() {
        curantitem = viewPager.getCurrentItem();
        startActivity(getIntent());
        finish();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_home) {
            initSetup();
        } else if (item.getItemId() == R.id.nav_notifications) {
            GoToNotifications();
        } else if (item.getItemId() == R.id.nav_documents) {
            GoToMyDocumentsActivity();
        } else if (item.getItemId() == R.id.nav_subscription) {
            GoToInsurancePackages();
        } else if (item.getItemId() == R.id.nav_add_ads) {
            GoToAddAds();
        } else if (item.getItemId() == R.id.nav_chat) {
            GoToRoomsChat();
        } else if (item.getItemId() == R.id.nav_fag) {
            GoToFaqs();
        } else if (item.getItemId() == R.id.nav_share) {
            ShareApps();
        } else if (item.getItemId() == R.id.nav_rate) {
            RateAppStore();
        } else if (item.getItemId() == R.id.nav_lang) {
            openDialogLanguage();
        } else if (item.getItemId() == R.id.nav_logout) {
            logOut();
        } else if (item.getItemId() == R.id.nav_privacy) {
            GoToPrivacy();
        } else if (item.getItemId() == R.id.nav_terms) {
            GoToTerms();
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void GoToAddAds() {
        Intent intent = new Intent(this, UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.add_your_ad));
        startActivity(intent);
    }

    private void GoToFaqs() {
        Intent intent = new Intent(getApplicationContext(), FaqsActivity.class);
        startActivity(intent);
    }

    private void GoToRoomsChat() {
        if (user != null) {
            Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showLoginToContinueDialog(HomeActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    private void GoToInsurancePackages() {
        if (user != null) {
            Intent intent = new Intent(this, UserActionActivity.class);
            intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.Insurance_packages));
            startActivity(intent);
        } else {
            AppErrorsManager.showLoginToContinueDialog(HomeActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    private void GoToMyDocumentsActivity() {
        if (user != null) {
            Intent intent = new Intent(this, DocumentsActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showLoginToContinueDialog(HomeActivity.this, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    public void logOut() {

        User user = PreferenceUser.User(this);
        if (user != null) {
            AppErrorsManager.showSuccessDialog(this, getString(R.string.logout), getString(R.string.do_you_want_logout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    connectionManager.logout(new CallbackConncation() {
                        @Override
                        public void onResponse(Response response) {
                            Gson json = new Gson();
                            AppPreferences.saveString(HomeActivity.this, "userJson", "0");
                            Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        } else {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
        }


    }

    private void ShareApps() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "https://play.google.com/store/apps/details?id=goldenauctions.com";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + " " +
                getResources().getString(R.string.share_message));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    public void openDialogLanguage() {
        LayoutInflater factory = LayoutInflater.from(getApplicationContext());
        @SuppressLint("InflateParams") final View dialogView = factory.inflate(R.layout.select_language_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        LinearLayout fontLayout = dialogView.findViewById(R.id.layout);

        deleteDialog.setView(dialogView);
        dialogView.findViewById(R.id.arabicBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditLanguageProfileWebService("ar");
                deleteDialog.dismiss();
            }
        });
        dialogView.findViewById(R.id.englishBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditLanguageProfileWebService("en");
                deleteDialog.dismiss();

            }
        });

        deleteDialog.show();
    }

    private void EditLanguageProfileWebService(String lang) {
        if (user != null) {
            connectionManager.editLanguageProfile(lang, new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    Response<LoginResponse> responseResponse = response;
                    User user = responseResponse.body().getUser();
                    // Log.e("ussss",user.getApp_locale() + " lang ");
                    Gson json = new Gson();
                    String userJson = json.toJson(user);
                    AppPreferences.saveString(HomeActivity.this, "userJson", userJson);
                    if (!userJson.isEmpty()) {
                        if (TextUtils.equals("ar", lang)) {
                            changeLang("ar");
                            AppLanguage.saveLanguage(getApplicationContext(), "ar");
                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
                        } else {
                            changeLang("en");
                            AppLanguage.saveLanguage(getApplicationContext(), "en");
                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
                        }

                    }


                }
            });
        } else {
            if (TextUtils.equals("ar", lang)) {
                changeLang("ar");
                AppLanguage.saveLanguage(getApplicationContext(), "ar");
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
            } else {
                changeLang("en");
                AppLanguage.saveLanguage(getApplicationContext(), "en");
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_in_left);
            }
        }
    }

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;

        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }


    private void RateAppStore() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void onBackPressed() {
        if (curantitem > 0) {
            curantitem = 0;
            viewPager.setCurrentItem(curantitem);
        } else {
            AppErrorsManager.showLogOutsDialogNotCancel(HomeActivity.this, getResources().getString(R.string.exit),
                    getResources().getString(R.string.wantexitapplication), new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            if (TextUtils.equals("Exit", status)) {
                                ExitApp();
                            }
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 888 && resultCode == 888) {
            if (data.getExtras() != null) {
                if (data.getExtras().getBoolean("IsUpdate", false) == true) {
                    // update notifications
                    image_count_notifications.setVisibility(View.GONE);
                    DrawableRound.TextToDrawableRound(HomeActivity.this, image_count_notifications, 0 + "");
                    if (user != null) {
                        user.setNotification_count(0);
                        Gson json = new Gson();
                        String userJson = json.toJson(user);
                        AppPreferences.saveString(HomeActivity.this, "userJson", userJson);
                    }

                }
            }
        } else if (requestCode == 208 && resultCode == 208) {
            if (data != null) {
                if (data.getBooleanExtra("isUpdate", false) == true) {
                    user = PreferenceUser.User(this);
                    initSetupDrawer();
                }
            }
        }
    }


    private void ExitApp() {
        super.onBackPressed();

    }


    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(HomeActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (user != null) {
                        user.setNotification_count(user.getNotification_count() + 1);
                        Gson json = new Gson();
                        String userJson = json.toJson(user);
                        AppPreferences.saveString(HomeActivity.this, "userJson", userJson);
                        RefreshNotification();
                    }
                    AppErrorsManager.showCustomErrorDialogTop(HomeActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            refreshActivty();

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };

    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

}