package goldenauctions.com.Ui.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.gson.Gson;
import com.stfalcon.chatkit.utils.DateFormatter;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.InstallTwoStringCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Install.Category;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Notification;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterAutions;
import goldenauctions.com.Ui.Adapters.AdapterAutionsNew;
import goldenauctions.com.Ui.Adapters.AdapterCategoryAutions;
import goldenauctions.com.Ui.Adapters.AdapterCategoryHome;
import goldenauctions.com.Ui.Adapters.AdapterFollowUp;
import goldenauctions.com.Ui.Adapters.AdapterNotifcation;
import goldenauctions.com.Ui.Adapters.RecyclerViewScrollListener;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import goldenauctions.com.WebService.model.response.NotificationsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static goldenauctions.com.Manager.RootManager.enterTransition;
import static goldenauctions.com.Manager.RootManager.returnTransition;

public class AuctionsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerRefreshLayout swipeRefreshLayout;
    RecyclerView recycler_view, recycler_view_category;
    RelativeLayout layout_empty;
    private TextView text_empty;
    AdapterAutionsNew adapter;
    private TextView toolbarNameTxt;
    FrameLayout layout_back;

    List<Auction> auctionList = new ArrayList<>();
    boolean IsLodeMore = false;
    private RecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    private ConnectionManager connectionManager;
    int page = 1;
    int per_page = 10;

    private int category_id = -1;
    private String category_name = "";

    private LinearLayout layout_tag;
    private TextView text_1, text_2, text_3, text_4;


    private List<Category> categoryList = new ArrayList<>();
    private AdapterCategoryAutions adapterCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_auctions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(enterTransition());
            getWindow().setSharedElementReturnTransition(returnTransition());
        }
        initSetUp();
    }

    private void initSetUp() {
        category_id = getIntent().getIntExtra("category_id", -1);
        category_name = getIntent().getStringExtra("category_name");
        connectionManager = new ConnectionManager(this);
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view_category = findViewById(R.id.recycler_view_category);


        setViewTagFilter();

        layout_empty = findViewById(R.id.layout_empty);
        text_empty = findViewById(R.id.text_empty);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText(getResources().getString(R.string.Auctions));
        layout_back = findViewById(R.id.layout_toolbar_menu);
        layout_back.setOnClickListener(view -> {
            onBackPressed();
        });
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        if (category_id != -1) {
            GetAuctionsWebSevice(category_id, page, per_page);
            if (!TextUtils.isEmpty(category_name)) {
                toolbarNameTxt.setText(category_name + "");
            }
        } else {
            GetAuctionsWebSevice(page, per_page);
        }


        InstallClass installClass = InstallPreference.GetInstall(this);
        if (installClass != null) {
            categoryList.add(new Category(1, getResources().getString(R.string.Auctions), "", true));
            categoryList.add(new Category(2, getResources().getString(R.string.Code), "", false));
            categoryList.add(new Category(3, getResources().getString(R.string.Price), "", false));
            categoryList.add(new Category(4, getResources().getString(R.string.EndDate), "", false));

            if (categoryList != null) {
                if (categoryList.size() > 0) {
                    setupRecyclerCategory();
                }
            }
        }

    }

    private void setViewTagFilter() {
        layout_tag = findViewById(R.id.layout_tag);
        text_1 = findViewById(R.id.text_1);
        text_2 = findViewById(R.id.text_2);
        text_3 = findViewById(R.id.text_3);
        text_4 = findViewById(R.id.text_4);
        layout_tag.setVisibility(View.GONE);
        text_1.setVisibility(View.INVISIBLE);
        text_2.setVisibility(View.INVISIBLE);
        text_3.setVisibility(View.INVISIBLE);
        text_4.setVisibility(View.INVISIBLE);
        text_1.setOnClickListener(view -> {
            ResetDataFilter();
        });
        text_2.setOnClickListener(view -> {
            ResetDataFilter();

        });
        text_3.setOnClickListener(view -> {
            ResetDataFilter();

        });
        text_4.setOnClickListener(view -> {
            ResetDataFilter();
        });
    }

    private void ResetDataFilter() {
        page = 1;
        for (int i = 0; i < categoryList.size(); i++) {
            if (i == 0) {
                categoryList.get(i).setSelect(true);
            } else {
                categoryList.get(i).setSelect(false);
            }
        }
        adapterCategory.notifyDataSetChanged();
        SetTextTageNull();
        GetAuctionsWebSevice(category_id, page, per_page);
    }

    private void SetTextTageNull() {
        layout_tag.setVisibility(View.GONE);
        text_1.setVisibility(View.INVISIBLE);
        text_2.setVisibility(View.INVISIBLE);
        text_3.setVisibility(View.INVISIBLE);
        text_4.setVisibility(View.INVISIBLE);
        text_1.setText("");
        text_2.setText("");
        text_3.setText("");
        text_4.setText("");
    }

    private void setupRecyclerCategory() {
        adapterCategory = new AdapterCategoryAutions(this, categoryList, -1, R.layout.row_category_autions, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (TextUtils.equals("view", tag)) {
                            AdapterCategoryAutions.ItemSelect = position;
                            for (int i = 0; i < categoryList.size(); i++) {
                                categoryList.get(i).setSelect(false);
                            }
                            GetDataByCategoryFilter(position);
                        }
                    }
                });
        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 4);
        recycler_view_category.setLayoutManager(linearLayoutManager);
        recycler_view_category.setAdapter(adapterCategory);
    }

    private String DateStart = "";
    private String DateEnd = "";
    private String PriceStart = "";
    private String PriceEnd = "";
    private String Code = "";

    private void GetDataByCategoryFilter(int position) {
        switch (categoryList.get(position).getId()) {
            case 1: // Auctions
                page = 1;
                categoryList.get(position).setSelect(true);
                adapterCategory.notifyDataSetChanged();
                SetTextTageNull();
                GetAuctionsWebSevice(category_id, page, per_page);
                break;
            case 2: // Code
                AppErrorsManager.showFilterCodetDialog(AuctionsActivity.this, new InstallTwoStringCallback() {
                    @Override
                    public void onStatusDone(String v1, String v2) {
                        if (TextUtils.equals("code", v1)) {
                            if (!TextUtils.isEmpty(v2)) {
                                Code = v2;
                                page = 1;
                                categoryList.get(position).setSelect(true);
                                adapterCategory.notifyDataSetChanged();
                                SetTextTageNull();
                                layout_tag.setVisibility(View.VISIBLE);
                                text_2.setVisibility(View.VISIBLE);
                                text_2.setText(" #" + Code);
                                GetAuctionsByCodeWebSevice(Code, page, per_page);
                            }
                        }
                    }
                });
                break;

            case 3: // Price
                AppErrorsManager.showFilterPricetDialog(AuctionsActivity.this, new InstallTwoStringCallback() {
                    @Override
                    public void onStatusDone(String v1, String v2) {
                        if (!TextUtils.isEmpty(v1) && !TextUtils.isEmpty(v2)) {
                            PriceStart = v1;
                            PriceEnd = v2;
                            page = 1;
                            categoryList.get(position).setSelect(true);
                            adapterCategory.notifyDataSetChanged();
                            SetTextTageNull();
                            layout_tag.setVisibility(View.VISIBLE);
                            text_3.setVisibility(View.VISIBLE);
                            text_3.setText(PriceStart + " " + getResources().getString(R.string.dollar_code) + " - " + PriceEnd + " " + getResources().getString(R.string.dollar_code));
                            GetAuctionsByPriceWebSevice(PriceStart, PriceEnd, page, per_page);

                        }
                    }
                });
                break;

            case 4: // EndDate
                AppErrorsManager.showFilterDatetDialog(AuctionsActivity.this, new InstallTwoStringCallback() {
                    @Override
                    public void onStatusDone(String v1, String v2) {
                        if (!TextUtils.isEmpty(v1) && !TextUtils.isEmpty(v2)) {
                            DateStart = v1;
                            DateEnd = v2;
                            page = 1;
                            categoryList.get(position).setSelect(true);
                            adapterCategory.notifyDataSetChanged();
                            SetTextTageNull();
                            layout_tag.setVisibility(View.VISIBLE);
                            text_4.setVisibility(View.VISIBLE);
                            text_4.setText(DateStart + " - " + DateEnd);
                            GetAuctionsByDateWebSevice(DateStart, DateEnd, page, per_page);

                        }
                    }
                });

                break;
        }

    }


    /**
     * public void setupRecycler() {
     * adapter = new AdapterAutions(AuctionsActivity.this, auctionList, R.layout.row_auctions, new
     * OnItemClickTagListener() {
     *
     * @Override public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
     * if (TextUtils.equals(tag, "view")) {
     * String ref_id = auctionList.get(position).getId() + "";
     * if (!TextUtils.isEmpty(ref_id)) {
     * GoToDetailsAuctionActivty(view, auctionList.get(position));
     * }
     * }
     * }
     * });
     * linearLayoutManager = new LinearLayoutManager(this);
     * recycler_view.setLayoutManager(linearLayoutManager);
     * recycler_view.setAdapter(adapter);
     * <p>
     * SetLodeMore();
     * }
     */

    private void GoToDetailsAuctionActivty(View view, Auction auction) {
        String auction_str = new Gson().toJson(auction, Auction.class);
        Intent intent = new Intent(AuctionsActivity.this, AuctionsDetialsActivity.class);
        intent.putExtra("ref_id", auction.getId());
        intent.putExtra("auction_str", auction_str);
        Pair<View, String> p1 = Pair.create((View) view.findViewById(R.id.image_row), "ImageProduct");
        Pair<View, String> p2 = Pair.create((View) view.findViewById(R.id.text_title), "NameProduct");
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, p1, p2);
        startActivityForResult(intent, 202, options.toBundle());

    }


    public void setupRecycler() {
        adapter = new AdapterAutionsNew(AuctionsActivity.this, auctionList, R.layout.row_follow_up, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (TextUtils.equals(tag, "view")) {
                            String ref_id = auctionList.get(position).getId() + "";
                            if (!TextUtils.isEmpty(ref_id)) {
                                GoToDetailsAuctionActivty(view, auctionList.get(position));
                            }
                        } else if (TextUtils.equals("UnFollow", tag)) {
                            FavouriteAuctionsToggle(auctionList.get(position), position);
                        }
                    }
                });
        linearLayoutManager = new LinearLayoutManager(AuctionsActivity.this);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

        SetLodeMore();
    }


    private void FavouriteAuctionsToggle(Auction auction, int pos) {
        if (auction != null) {
            connectionManager.FavouriteAuctionsToggle(auction.getId(), new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    if (response.body() != null) {
                        Response<AuctionResponse> auctionResponse = response;
                        if (auctionResponse.body() != null) {
                            String message = auctionResponse.body().getStatus().getMessageList();
                            Toasty.success(AuctionsActivity.this, getResources().getString(R.string.info) + " : " + message);
                            if (auctionResponse.body().getAuction() != null) {
                                auctionList.get(pos).setIs_favourite(auctionResponse.body().getAuction().isIs_favourite());
                                adapter.notifyDataSetChanged();



                            }
                        }
                    }
                }
            });
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && resultCode == 202) {
            page = 1;
            if (category_id != -1) {
                GetAuctionsWebSevice(category_id, page, per_page);
            } else {
                GetAuctionsWebSevice(page, per_page);
            }
        }
    }

    private void SetLodeMore() {
        scrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page_, int totalItemsCount) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (IsLodeMore) {
                            page++;
                            /**  if (category_id != -1) {
                             GetAuctionsWebSevice(category_id, page, per_page);
                             } else {
                             GetAuctionsWebSevice(page, per_page);
                             }*/
                            int id = -1;
                            for (int i = 0; i < categoryList.size(); i++) {
                                if (categoryList.get(i).isSelect()) {
                                    id = categoryList.get(i).getId();
                                }
                            }
                            switch (id) {
                                case 1:
                                    GetAuctionsWebSevice(category_id, page, per_page);
                                    break;
                                case 2: // Code
                                    GetAuctionsByCodeWebSevice(Code, page, per_page);
                                    break;
                                case 3: // Price
                                    GetAuctionsByPriceWebSevice(PriceStart, PriceEnd, page, per_page);
                                    break;
                                case 4: // EndDate
                                    GetAuctionsByDateWebSevice(DateStart, DateEnd, page, per_page);
                                    break;
                                default:
                                    GetAuctionsWebSevice(category_id, page, per_page);
                                    break;
                            }

                        }

                    }
                }, 1000);

            }

            @Override
            public void onReachTop(boolean isFirst) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        recycler_view.addOnScrollListener(scrollListener);
    }

    boolean Isupdate = false;

    private void GetAuctionsWebSevice(int page, int per_page) {
        connectionManager.GetAuctions(page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {

                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {

                        Response<AuctionsResponse> responseResponse = response;
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                            if (page == 1) {
                                auctionList = responseResponse.body().getAuctions();
                                if (auctionList != null) {
                                    if (auctionList.size() > 0) {
                                        setupRecycler();
                                    } else {
                                        layout_empty.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                auctionList.addAll(responseResponse.body().getAuctions());
                                adapter.notifyDataSetChanged();
                            }

                            if (auctionList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (responseResponse.body().getPaging() != null) {
                                    if (auctionList.size() >= responseResponse.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + message_str);
                            IsLodeMore = false;
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            text_empty.setText("" + message_str);
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            IsLodeMore = false;
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                        text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                    recycler_view.setVisibility(View.GONE);
                    layout_empty.setVisibility(View.VISIBLE);
                    IsLodeMore = false;
                }


            }
        });

    }


    private void GetAuctionsWebSevice(int category_id, int page, int per_page) {
        connectionManager.GetAuctionsByCategory(category_id, page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {


                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {

                        Response<AuctionsResponse> responseResponse = response;
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                            if (page == 1) {
                                auctionList = responseResponse.body().getAuctions();
                                if (auctionList != null) {
                                    if (auctionList.size() > 0) {
                                        setupRecycler();
                                    } else {
                                        layout_empty.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                auctionList.addAll(responseResponse.body().getAuctions());
                                adapter.notifyDataSetChanged();
                            }

                            if (auctionList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (responseResponse.body().getPaging() != null) {
                                    if (auctionList.size() >= responseResponse.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + message_str);
                            IsLodeMore = false;
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            text_empty.setText("" + message_str);
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            IsLodeMore = false;
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                        text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                    recycler_view.setVisibility(View.GONE);
                    layout_empty.setVisibility(View.VISIBLE);
                    IsLodeMore = false;
                }
            }
        });
        /**  auctionList.add(new Auction());
         auctionList.add(new Auction());
         auctionList.add(new Auction());
         auctionList.add(new Auction());
         if (auctionList != null) {
         if (auctionList.size() > 0) {
         setupRecycler();
         } else {
         layout_empty.setVisibility(View.VISIBLE);
         }
         }*/
    }

    private void GetAuctionsByCodeWebSevice(String code, int page, int per_page) {
        connectionManager.GetAuctionsByCode(category_id ,code, page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {


                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {

                        Response<AuctionsResponse> responseResponse = response;
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                            if (page == 1) {
                                auctionList = responseResponse.body().getAuctions();
                                if (auctionList != null) {
                                    if (auctionList.size() > 0) {
                                        setupRecycler();
                                    } else {
                                        layout_empty.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                auctionList.addAll(responseResponse.body().getAuctions());
                                adapter.notifyDataSetChanged();
                            }

                            if (auctionList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (responseResponse.body().getPaging() != null) {
                                    if (auctionList.size() >= responseResponse.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + message_str);
                            IsLodeMore = false;
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            text_empty.setText("" + message_str);
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            IsLodeMore = false;
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                        text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                    recycler_view.setVisibility(View.GONE);
                    layout_empty.setVisibility(View.VISIBLE);
                    IsLodeMore = false;
                }
            }
        });

    }

    private void GetAuctionsByPriceWebSevice(String StartPrice, String EndPrice, int page, int per_page) {
        connectionManager.GetAuctionsByPrice(category_id ,StartPrice, EndPrice, page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {


                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {

                        Response<AuctionsResponse> responseResponse = response;
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                            if (page == 1) {
                                auctionList = responseResponse.body().getAuctions();
                                if (auctionList != null) {
                                    if (auctionList.size() > 0) {
                                        setupRecycler();
                                    } else {
                                        layout_empty.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                auctionList.addAll(responseResponse.body().getAuctions());
                                adapter.notifyDataSetChanged();
                            }

                            if (auctionList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (responseResponse.body().getPaging() != null) {
                                    if (auctionList.size() >= responseResponse.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + message_str);
                            IsLodeMore = false;
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            text_empty.setText("" + message_str);
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            IsLodeMore = false;
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                        text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                    recycler_view.setVisibility(View.GONE);
                    layout_empty.setVisibility(View.VISIBLE);
                    IsLodeMore = false;
                }
            }
        });

    }

    private void GetAuctionsByDateWebSevice(String StartDate, String EndDate, int page, int per_page) {
        connectionManager.GetAuctionsByDate(category_id ,StartDate, EndDate, page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {


                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {

                        Response<AuctionsResponse> responseResponse = response;
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                            if (page == 1) {
                                auctionList = responseResponse.body().getAuctions();
                                if (auctionList != null) {
                                    if (auctionList.size() > 0) {
                                        setupRecycler();
                                    } else {
                                        layout_empty.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                auctionList.addAll(responseResponse.body().getAuctions());
                                adapter.notifyDataSetChanged();
                            }

                            if (auctionList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (responseResponse.body().getPaging() != null) {
                                    if (auctionList.size() >= responseResponse.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + message_str);
                            IsLodeMore = false;
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            text_empty.setText("" + message_str);
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            IsLodeMore = false;
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                        text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(AuctionsActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                    recycler_view.setVisibility(View.GONE);
                    layout_empty.setVisibility(View.VISIBLE);
                    IsLodeMore = false;
                }
            }
        });

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                page = 1;
                /**  if (category_id != -1) {
                 GetAuctionsWebSevice(category_id, page, per_page);
                 } else {
                 GetAuctionsWebSevice(page, per_page);
                 }*/
                ResetDataFilter();

            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
        if (Isupdate) {
            Intent intent = getIntent();
            intent.putExtra("IsUpdate", true);
            setResult(888, intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(AuctionsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    page = 1;

                    AppErrorsManager.showCustomErrorDialogTop(AuctionsActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {


                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

}