package goldenauctions.com.Ui.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Medoles.Notification;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterNotifcation;
import goldenauctions.com.Ui.Adapters.RecyclerViewScrollListener;
import goldenauctions.com.WebService.model.response.NotificationsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class NotificationsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerRefreshLayout swipeRefreshLayout;
    RecyclerView recycler_view;
    LinearLayout layout_empty;
    AdapterNotifcation adapterNotifcation;
    private TextView toolbarNameTxt;
    FrameLayout layout_back;

    List<Notification> notifications = new ArrayList<>();
    boolean IsLodeMore = false;
    private RecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    private ConnectionManager connectionManager;
    int page = 1;
    int per_page = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_notifications);
        initSetUp();
    }

    private void initSetUp() {
        connectionManager = new ConnectionManager(this);
        recycler_view = findViewById(R.id.recycler_view);
        layout_empty = findViewById(R.id.layout_empty);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText(getResources().getString(R.string.notifications));
        layout_back = findViewById(R.id.layout_toolbar_menu);
        layout_back.setOnClickListener(view -> {
            onBackPressed();
        });
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        NotifactionsWebSevice(page, per_page);
    }


    public void setupRecyclerNotifcations() {
        adapterNotifcation = new AdapterNotifcation(NotificationsActivity.this, notifications, R.layout.row_notifcation, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (TextUtils.equals(tag, "view")) {
                            String ref_id = notifications.get(position).getRef_id();
                            String ref_type = notifications.get(position).getType();
                            if (!TextUtils.isEmpty(ref_id)) {
                                if (TextUtils.equals("2", ref_type)) {
                                    Intent intent = new Intent(getApplicationContext(), AuctionsDetialsActivity.class);
                                    int id = Integer.parseInt(ref_id);
                                    intent.putExtra("ref_id", id);
                                    startActivity(intent);
                                } else if (TextUtils.equals("3", ref_type)) {
                                    Intent intent = new Intent(NotificationsActivity.this, TicketDetialsActivity.class);
                                    int id = Integer.parseInt(ref_id);
                                    intent.putExtra("ref_id", id);
                                    startActivity(intent);
                                }
                            }
                        }
                    }
                });
        linearLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapterNotifcation);
        SetAllNotifcationRead();
        SetLodeMore();
    }

    private void SetLodeMore() {
        scrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page_, int totalItemsCount) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (IsLodeMore) {
                            page++;
                            NotifactionsWebSevice(page, per_page);
                        }

                    }
                }, 1000);

            }

            @Override
            public void onReachTop(boolean isFirst) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        recycler_view.addOnScrollListener(scrollListener);
    }

    boolean Isupdate = false;

    private void NotifactionsWebSevice(int page, int per_page) {
        connectionManager.GetNotifications(page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                Response<NotificationsResponse> responseResponse = response;
                String message_str = responseResponse.body().getStatus().getMessageList();
                if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                    if (page == 1) {
                        notifications = responseResponse.body().getNotifications();
                        if (notifications != null) {
                            if (notifications.size() > 0) {
                                setupRecyclerNotifcations();
                            } else {
                                layout_empty.setVisibility(View.VISIBLE);
                            }

                        } else {
                            layout_empty.setVisibility(View.VISIBLE);
                        }
                    } else {
                        notifications.addAll(responseResponse.body().getNotifications());
                        adapterNotifcation.notifyDataSetChanged();
                    }

                    if (notifications.size() == 0) {
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    } else {
                        recycler_view.setVisibility(View.VISIBLE);
                        layout_empty.setVisibility(View.GONE);

                        if (responseResponse.body().getPaging() != null) {
                            if (notifications.size() >= responseResponse.body().getPaging().getTotal()) {
                                IsLodeMore = false;
                            } else {
                                IsLodeMore = true;
                            }
                        } else {
                            IsLodeMore = false;
                        }
                    }
                }
            }
        });

    }

    private void SetAllNotifcationRead() {

        connectionManager.ReadAllNotifications(new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                Isupdate = true;
            }
        });


    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                page = 1;
                NotifactionsWebSevice(page, per_page);
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
        if (Isupdate) {
            Intent intent = getIntent();
            intent.putExtra("IsUpdate", true);
            setResult(888, intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(NotificationsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    page = 1;
                    NotifactionsWebSevice(page, per_page);


                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

}