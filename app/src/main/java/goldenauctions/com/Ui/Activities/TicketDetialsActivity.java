package goldenauctions.com.Ui.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.Ticket;
import goldenauctions.com.Medoles.TicketResponses;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterTicketResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import goldenauctions.com.WebService.model.response.TicketResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;


public class TicketDetialsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private FrameLayout layout_loading;
    FrameLayout layout_back;
    TextView toolbarNameTxt;
    RecyclerRefreshLayout swipeRefreshLayout;
    int TicketID = -1;
    TextView text_title, text_body, text_status;
    private LinearLayout layout_card;
    TextView text_reply;

    List<TicketResponses> ticketResponses = new ArrayList<>();
    RecyclerView recycler_view;
    AdapterTicketResponse adapter;
    private LinearLayout layout_reply;
    private EditText edit_body_msg;

    User user = new User();
    int ref_id = -1;
    private ConnectionManager connectionManager;
    private Ticket ticket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_ticket_details);
        user = PreferenceUser.User(this);
        connectionManager = new ConnectionManager(this);
        ref_id = getIntent().getIntExtra("ref_id", -1);
        String str_ticket = "";
        str_ticket = getIntent().getStringExtra("str_ticket");
        if (!TextUtils.isEmpty(str_ticket)) {
            ticket = new Gson().fromJson(str_ticket, Ticket.class);
        }
        if (ref_id != -1) {
            TicketID = ref_id;

        }

        initView();

    }

    private void initView() {

        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });

        layout_back = findViewById(R.id.layout_toolbar_menu);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        layout_card = findViewById(R.id.layout_card);
        layout_card.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText("" + getString(R.string.TicketsDetails));
        text_title = findViewById(R.id.text_title);
        text_body = findViewById(R.id.text_body);
        text_status = findViewById(R.id.text_status);
        text_reply = findViewById(R.id.text_reply);
        layout_reply = findViewById(R.id.layout_reply);
        TextView close_dialog = findViewById(R.id.close_dialog);
        edit_body_msg = findViewById(R.id.edit_dialog_body);
        recycler_view = findViewById(R.id.recycler_view);

        close_dialog.setOnClickListener(view -> {
            if (TextUtils.isEmpty(edit_body_msg.getText().toString().trim())) {
                edit_body_msg.setError(getResources().getString(R.string.required_field));
                edit_body_msg.requestFocus();
            } else {

                String body = edit_body_msg.getText().toString().trim();
                CreateReplyTicketWebService(body);
            }
        });
        if (ticket != null) {
            setValueToView(ticket);
        } else {
            if (TicketID != -1)
                TicketsWebSevice(TicketID);
        }
    }

    private void setValueToView(Ticket ticket) {
        if (ticket != null) {
            TicketID = ticket.getId();
            layout_card.setVisibility(View.VISIBLE);
            text_title.setText(ticket.getTitle() + "");
            text_body.setText(ticket.getMessage() + "");
            if (TextUtils.equals("1", ticket.getStatus() + "")) {
                text_status.setEnabled(true);
                text_status.setTextColor(getResources().getColor(R.color.white));
                text_status.setText("" + getResources().getString(R.string.Opened));
                text_status.setBackgroundResource(R.drawable.shape_btn_disactive);
                layout_reply.setVisibility(View.VISIBLE);
            } else {
                text_status.setEnabled(false);
                text_status.setText("" + getResources().getString(R.string.Closed));
                text_status.setTextColor(getResources().getColor(R.color.colorRed));
                text_status.setBackgroundResource(R.drawable.shape_btn_disactive);
                layout_reply.setVisibility(View.GONE);
            }
            if (ticket.getTicketResponses() != null) {
                if (ticket.getTicketResponses().size() > 0) {
                    ticketResponses = ticket.getTicketResponses();
                    text_reply.setVisibility(View.GONE);
                    setupRecycler();
                } else {
                    recycler_view.setVisibility(View.GONE);
                    text_reply.setVisibility(View.VISIBLE);
                    text_reply.setText("" + getResources().getString(R.string.Not_yet_answered));
                }
            } else {
                text_reply.setVisibility(View.VISIBLE);
                text_reply.setText("" + getResources().getString(R.string.Not_yet_answered));

            }
        }
    }


    private void TicketsWebSevice(int TicketID) {
        layout_loading.setVisibility(View.VISIBLE);
        connectionManager.GetShowTicket(TicketID, new CallbackConncation() {
            @Override
            public void onResponse(Response resp) {
                if (resp != null) {
                    Response<TicketResponse> response = resp;
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        String message_str = response.body().getStatus().getMessageList();
                        if ("success".equals(response.body().getStatus().getStatus())) {
                            ticket = response.body().getTicket();
                            setValueToView(ticket);
                        } else if ("error".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(TicketDetialsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        } else if ("fail".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(TicketDetialsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        }

                    }

                }
            }
        });

    }


    public void setupRecycler() {
        recycler_view.setVisibility(View.VISIBLE);
        adapter = new AdapterTicketResponse(TicketDetialsActivity.this, ticketResponses, R.layout.row_item_res_ticket, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {


                        adapter.notifyDataSetChanged();
                    }
                });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void CreateReplyTicketWebService(String v) {
        layout_loading.setVisibility(View.VISIBLE);

        connectionManager.AddResponseTickets(TicketID, v, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                if (response != null) {
                    Response<RootResponse> rootResponseResponse = response;
                    if (rootResponseResponse.body() != null) {
                        edit_body_msg.setText("");
                        TicketsWebSevice(ticket.getId());
                        Isupdate = true;
                    }
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (Isupdate) {
            Intent intent = new Intent();
            intent.putExtra("update", "update");
            setResult(999, intent);
            finish();
        } else
            super.onBackPressed();
    }


    boolean Isupdate = false;


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                TicketsWebSevice(TicketID);
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");

            appMp3Manager = new AppMp3Manager(TicketDetialsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (TicketID != -1)
                        TicketsWebSevice(TicketID);

                    AppErrorsManager.showCustomErrorDialogTop(TicketDetialsActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


}
