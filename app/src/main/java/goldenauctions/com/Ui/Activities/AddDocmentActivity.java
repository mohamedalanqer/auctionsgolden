package goldenauctions.com.Ui.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.FilePath;
import goldenauctions.com.Hellper.ImagePicker;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Document;
import goldenauctions.com.Medoles.Install.Category;
import goldenauctions.com.Medoles.Install.DocumentType;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterCategoryHome;
import goldenauctions.com.Ui.Fragments.DatePickerFragment;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.DocumentResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import okhttp3.MultipartBody;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class AddDocmentActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    FrameLayout layout_back;
    TextView toolbarNameTxt;
    private ConnectionManager connectionManager;
    private FrameLayout layout_loading;
    SwipeRefreshLayout swipeRefreshLayout;
    private TextView DateTxt, edit_type;
    private ImageView img_type, image_front, image_backend;
    File fileSchemaFront;
    File fileSchemaBack;
    public static final int REQUEST_IMAGE_FRONT = 100;
    public static final int REQUEST_IMAGE_BACK = 101;
    private TextView btn_action;
    private LinearLayout layout_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_add_docment);
        connectionManager = new ConnectionManager(this);
        initView();
    }

    private int document_type_id = -1;
    List<Document> documentList = new ArrayList<>();

    private void initView() {
        document_type_id = getIntent().getIntExtra("document_type_id", -1);


        layout_back = findViewById(R.id.layout_toolbar_menu);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText("" + getString(R.string.CreateNewDocument));
        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });

        layout_type = findViewById(R.id.layout_type);
        layout_type.setVisibility(View.GONE);
        DateTxt = findViewById(R.id.DateTxt);
        edit_type = findViewById(R.id.edit_type);
        img_type = findViewById(R.id.img_type);
        btn_action = findViewById(R.id.btn_action);
        btn_action.setOnClickListener(this::onClick);
        image_front = findViewById(R.id.image_front);
        image_backend = findViewById(R.id.image_backend);
        image_front.setOnClickListener(this::onClick);
        image_backend.setOnClickListener(this::onClick);

        edit_type.setOnClickListener(this::onClick);
        DateTxt.setOnClickListener(this::onClick);
        InstallClass installClass = InstallPreference.GetInstall(this);
        categoryList = new ArrayList<>();
        if (installClass != null) {
            if (installClass.getDocumentsTypes() != null) {
                List<DocumentType> documentTypes = installClass.getDocumentsTypes();
                if (documentTypes.size() > 0) {
                    for (int i = 0; i < documentTypes.size(); i++) {
                        if (document_type_id != -1) {
                            if (documentTypes.get(i).getId() == document_type_id) {
                                edit_type.setText("" + documentTypes.get(i).getName());
                                toolbarNameTxt.setText("" + documentTypes.get(i).getName());
                                if (documentTypes.get(i).getImage() != null) {
                                    Glide.with(this).load(documentTypes.get(i).getImage()).error(R.drawable.logo).into(img_type);
                                } else
                                    img_type.setImageResource(R.drawable.logo);
                            }

                        }
                        categoryList.add(new Category(documentTypes.get(i).getId(), documentTypes.get(i).getName(), documentTypes.get(i).getImage()));
                    }
                }

            }
        }

        if (getIntent().getStringExtra("documentList_str") != null) {
            String documentList_str = getIntent().getStringExtra("documentList_str");
            Log.e("documentList_str", documentList_str + "   documentList_str");
            if (!TextUtils.isEmpty(documentList_str)) {
                documentList = new Gson().fromJson(documentList_str, new TypeToken<List<Document>>() {
                }.getType());
                Log.e("documentList_str", documentList.size() + "   documentList");
                if (documentList.size() > 0) {
                    for (int i = 0; i < documentList.size(); i++) {
                        Log.e("documentList_str", documentList.size() + "   documentList");
                        if (TextUtils.equals(documentList.get(i).getDocument_type_id(), "" + document_type_id)) {
                            Document document = documentList.get(i);
                            SetValueToView(document);
                        }
                    }
                }
            }
        }
    }

    private void SetValueToView(Document document) {
        btn_action.setText("" + getResources().getString(R.string.Save));
        if (document.getExpiry_date() != null) {
            String exp = document.getExpiry_date();
            exp = exp.replace("\"", "");
            DateTxt.setText(exp + "");
        }
        if (!TextUtils.isEmpty(document.getFront_face())) {
            Glide.with(this).load(document.getFront_face() + "").error(R.drawable.no_image).into(image_front);
        }

        if (!TextUtils.isEmpty(document.getBack_face())) {
            Glide.with(this).load(document.getBack_face() + "").error(R.drawable.no_image).into(image_backend);
        }
        toolbarNameTxt.setText("" + document.getDocumentType().getName());

    }


    AdapterCategoryHome categoryHome;
    List<Category> categoryList = new ArrayList<>();
    private int ItemSelect = -1;

    private void OpenDialogCreateNewDocument() {
        if (categoryList.size() > 0) {

            ItemSelect = -1;
            categoryHome = new AdapterCategoryHome(AddDocmentActivity.this, categoryList, ItemSelect, "doc", R.layout.row_category_doc, new OnItemClickTagListener() {
                @Override
                public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                    if (TextUtils.equals("view", tag)) {
                        ItemSelect = position;
                        AdapterCategoryHome.ItemSelect = position;
                        categoryHome.notifyDataSetChanged();

                    }
                }
            });
            AppErrorsManager.showAddDocumentsDialog(AddDocmentActivity.this, categoryHome, new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        if (ItemSelect != -1) {
                            document_type_id = categoryList.get(ItemSelect).getId();
                            edit_type.setText("" + categoryList.get(ItemSelect).getName());
                            if (categoryList.get(ItemSelect).getImage() != null) {
                                Glide.with(AddDocmentActivity.this).load(categoryList.get(ItemSelect).getImage()).error(R.drawable.logo).into(img_type);
                            } else
                                img_type.setImageResource(R.drawable.logo);
                        } else {
                            AppErrorsManager.showErrorDialog(AddDocmentActivity.this, getResources().getString(R.string.PleaseSelectTypeDocumentToContinue));
                        }
                    }
                }
            });

        }


    }


    boolean Isupdate = false;

    @Override
    public void onBackPressed() {
        if (Isupdate) {
            Intent intent = new Intent();
            intent.putExtra("update", "update");
            setResult(999, intent);
            finish();
        } else
            super.onBackPressed();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(AddDocmentActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(AddDocmentActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_type:
                OpenDialogCreateNewDocument();
                break;
            case R.id.DateTxt:
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.image_front:
                onImageOnlyPick(REQUEST_IMAGE_FRONT);
                break;

            case R.id.image_backend:
                onImageOnlyPick(REQUEST_IMAGE_BACK);
                break;
            case R.id.btn_action:
                CreateNewDocumentWebService();
                break;

        }
    }

    private void CreateNewDocumentWebService() {
        String expiry_date = DateTxt.getText().toString();
        if (document_type_id == -1) {
            AppErrorsManager.showCustomErrorDialog(AddDocmentActivity.this, getResources().getString(R.string.error),
                    getResources().getString(R.string.PleaseSelectTypeDocumentToContinue));
        } else {
            if (TextUtils.equals(getResources().getString(R.string.Save), btn_action.getText().toString())) {
                MultipartBody.Part image_File_Front = null;
                MultipartBody.Part image_File_Back = null;
                if (fileSchemaFront != null) {
                    image_File_Front = FilePath.GetFilePartFromFile(fileSchemaFront, "front_face");
                }
                if (fileSchemaBack != null) {
                    image_File_Back = FilePath.GetFilePartFromFile(fileSchemaBack, "back_face");
                }
                connectionManager.UploadNewDocument(expiry_date, document_type_id, image_File_Front, image_File_Back, new CallbackConncation() {
                    @Override
                    public void onResponse(Response response) {
                        if (response.body() != null) {
                            Isupdate = true;
                            Response<RootResponse> documentResponseResponse = response;
                            if (documentResponseResponse.body() != null) {
                                String message = documentResponseResponse.body().getStatus().getMessageList();
                                AppErrorsManager.showCustomErrorDialogNotCancel(AddDocmentActivity.this, getResources().getString(R.string.info),
                                        message + "", new InstallCallback() {
                                            @Override
                                            public void onStatusDone(String status) {
                                                onBackPressed();
                                            }
                                        });

                            }
                        }
                    }
                });

            } else {
                if (fileSchemaFront == null) {
                    AppErrorsManager.showCustomErrorDialog(AddDocmentActivity.this, getResources().getString(R.string.error),
                            getResources().getString(R.string.Please_choose_afront_image));
                } else if (fileSchemaBack == null) {
                    AppErrorsManager.showCustomErrorDialog(AddDocmentActivity.this, getResources().getString(R.string.error),
                            getResources().getString(R.string.Please_choose_background_image));
                } else {
                    MultipartBody.Part image_File_Front = FilePath.GetFilePartFromFile(fileSchemaFront, "front_face");
                    MultipartBody.Part image_File_Back = FilePath.GetFilePartFromFile(fileSchemaBack, "back_face");
                    connectionManager.UploadNewDocument(expiry_date, document_type_id, image_File_Front, image_File_Back, new CallbackConncation() {
                        @Override
                        public void onResponse(Response response) {
                            if (response.body() != null) {
                                Isupdate = true;
                                Response<RootResponse> documentResponseResponse = response;
                                if (documentResponseResponse.body() != null) {
                                    String message = documentResponseResponse.body().getStatus().getMessageList();
                                    AppErrorsManager.showCustomErrorDialogNotCancel(AddDocmentActivity.this, getResources().getString(R.string.info),
                                            message + "", new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    onBackPressed();
                                                }
                                            });

                                }
                            }
                        }
                    });
                }
            }
        }
    }

    public void onImageOnlyPick(int req_img) {
        PackageManager pm = getPackageManager();
        int hasPerm2 = pm.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
        if (hasPerm2 == PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(this);
            startActivityForResult(chooseImageIntent, req_img);
        } else {
            ImagePicker.checkAndRequestPermissions(this);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getImageFromGalleryFile(Intent data, int req_img) {
        if (data == null) return;
        Uri uri = data.getData();
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            Random r = new Random();
            if (req_img == REQUEST_IMAGE_FRONT) {
                fileSchemaFront = FilePath.converBitmaptoFile(bitmap, "front_face" + r.nextInt(10000 - 65) + 65, this);
                Glide.with(this).load(uri).into(image_front);
            } else if (req_img == REQUEST_IMAGE_BACK) {
                fileSchemaBack = FilePath.converBitmaptoFile(bitmap, "back_face" + r.nextInt(10000 - 65) + 65, this);
                Glide.with(this).load(uri).into(image_backend);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_FRONT) {
            if (resultCode == Activity.RESULT_OK) {
                getImageFromGalleryFile(data, REQUEST_IMAGE_FRONT);
            }
        } else if (requestCode == REQUEST_IMAGE_BACK) {
            if (resultCode == Activity.RESULT_OK) {
                getImageFromGalleryFile(data, REQUEST_IMAGE_BACK);
            }
        }
    }

}