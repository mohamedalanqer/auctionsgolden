package goldenauctions.com.Ui.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Fragments.ChangePasswordFragment;
import goldenauctions.com.Ui.Fragments.EditProfileFragment;
import goldenauctions.com.Ui.Fragments.ForgetPasswordFragment;
import goldenauctions.com.Ui.Fragments.InfoFragment;
import goldenauctions.com.Ui.Fragments.InsurancePackagesFragment;
import goldenauctions.com.Ui.Fragments.SendAdsFragment;
import goldenauctions.com.Ui.Fragments.VerifyFragment;

public class UserActionActivity extends AppCompatActivity {

    private ImageView img_logo;
    private LinearLayout layout_hidden_top;
    private LinearLayout layout_logo;
    private RelativeLayout layout_back;
    private LinearLayout layout_sheet;
    private ImageView image_back;
    private FrameLayout layout_loading;
    private LinearLayout layout_status_bar;
    private boolean isUpdate = false;

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    private String Action = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_user_action);
        layout_status_bar = findViewById(R.id.layout_status_bar);
        int Height = getStatusBarHeight();
        layout_status_bar.setVisibility(View.VISIBLE);
        layout_status_bar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Height));
        initSetUp();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void initSetUp() {
        img_logo = findViewById(R.id.img_logo);
        layout_back = findViewById(R.id.end_main);
        layout_logo = findViewById(R.id.layout_logo);
        image_back = findViewById(R.id.image_toolbar_start);
        layout_hidden_top = findViewById(R.id.layout_hidden_top);
        layout_sheet = findViewById(R.id.layout_sheet);
        layout_back.setOnClickListener(view -> {
            onBackPressed();
        });

        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(v -> {
            return;
        });

        Action = getIntent().getStringExtra(RootManager.ActionIntent);
        if (TextUtils.equals(Action, RootManager.ActionForgetPassword)) {
            openFragment(new ForgetPasswordFragment());
        } else if (TextUtils.equals(Action, getResources().getString(R.string.PrivacyPolicy))) {
            Bundle bundle = new Bundle();
            bundle.putString(RootManager.ActionIntent, getResources().getString(R.string.PrivacyPolicy));
            InfoFragment fragment = new InfoFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (TextUtils.equals(Action, getResources().getString(R.string.Termsofuse))) {
            Bundle bundle = new Bundle();
            bundle.putString(RootManager.ActionIntent, getResources().getString(R.string.Termsofuse));
            InfoFragment fragment = new InfoFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (TextUtils.equals(Action, getResources().getString(R.string.AboutApp))) {
            Bundle bundle = new Bundle();
            bundle.putString(RootManager.ActionIntent, getResources().getString(R.string.AboutApp));
            InfoFragment fragment = new InfoFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (TextUtils.equals(Action, getResources().getString(R.string.ChangePassword))) {
            openFragment(new ChangePasswordFragment());
        } else if (TextUtils.equals(Action, getResources().getString(R.string.verifyAccount))) {
            Bundle bundle = new Bundle();
            bundle.putInt("TypeVerify", RootManager.verifyAccount);
            VerifyFragment fragment = new VerifyFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (TextUtils.equals(Action, getResources().getString(R.string.VerifyMobile))) {
            Bundle bundle = new Bundle();
            bundle.putInt("TypeVerify", RootManager.VerificationTypeMobile);
            VerifyFragment fragment = new VerifyFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (TextUtils.equals(Action, getResources().getString(R.string.VerifyEmail))) {
            Bundle bundle = new Bundle();
            bundle.putInt("TypeVerify", RootManager.VerificationTypeEmail);
            VerifyFragment fragment = new VerifyFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (TextUtils.equals(Action, getResources().getString(R.string.Insurance_packages))) {
            openFragment(new InsurancePackagesFragment());
        } else if (TextUtils.equals(Action, getResources().getString(R.string.EditProfile))) {
            openFragment(new EditProfileFragment());
        } else if (TextUtils.equals(Action, getResources().getString(R.string.add_your_ad))) {
            openFragment(new SendAdsFragment());
        }

    }


    public void SetStyleWhite(boolean ShowLogo) {
        layout_status_bar.setVisibility(View.GONE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        layout_logo.setBackgroundResource(R.color.colorWhite);
        image_back.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.MULTIPLY);
        layout_sheet.setBackgroundResource(R.color.colorWhite);
        if (!ShowLogo) {
            img_logo.setVisibility(View.GONE);
            layout_hidden_top.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200));

        } else {
            img_logo.setVisibility(View.VISIBLE);
            layout_hidden_top.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        }
    }

    public void openFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();

    }

    private void openFragmentWithBundle(Fragment fragment, String data, int id) {
        FragmentManager manager = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        bundle.putInt("id", id);
        fragment.setArguments(bundle);
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (isUpdate) {
            Intent intent = getIntent();
            intent.putExtra("isUpdate", isUpdate);
            setResult(208, intent);
            finish();
        } else {
            super.onBackPressed();

        }

    }
}