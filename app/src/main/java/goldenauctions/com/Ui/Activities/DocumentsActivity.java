package goldenauctions.com.Ui.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.InstallTwoStringCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppMp3Manager;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Document;
import goldenauctions.com.Medoles.Install.Category;
import goldenauctions.com.Medoles.Install.DocumentType;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.Ticket;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Adapters.AdapterCategoryHome;
import goldenauctions.com.Ui.Adapters.AdapterDocument;
import goldenauctions.com.Ui.Adapters.AdapterTicket;
import goldenauctions.com.Ui.Adapters.RecyclerViewScrollListener;
import goldenauctions.com.WebService.model.response.DocumentsResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import goldenauctions.com.WebService.model.response.TicketsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;


public class DocumentsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recycler_view;
    LinearLayout layout_empty;
    AdapterDocument adapter;
    private FrameLayout layout_loading;
    List<Document> documentList = new ArrayList<>();
    FrameLayout layout_back;
    TextView toolbarNameTxt;
    RecyclerRefreshLayout swipeRefreshLayout;
    boolean IsLodeMore = false;
    private RecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    private FloatingActionButton fab;
    int page = 1;
    int per_page = 10;

    User user = new User();
    private ConnectionManager connectionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_documents);
        user = PreferenceUser.User(this);
        connectionManager = new ConnectionManager(this);
        initView();

    }


    private void initView() {

        recycler_view = findViewById(R.id.recycler_view);
        layout_empty = findViewById(R.id.layout_empty);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            OpenDialogCreateNewDocument();
        });
        layout_back = findViewById(R.id.layout_toolbar_menu);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText("" + getString(R.string.Document_folder));
        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });
        GetMyDocumentsWebSevice(page, per_page);

    }

    private void SetLodeMore() {
        scrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page_, int totalItemsCount) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (IsLodeMore) {
                            page++;
                            GetMyDocumentsWebSevice(page, per_page);
                        }

                    }
                }, 1000);

            }

            @Override
            public void onReachTop(boolean isFirst) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        recycler_view.addOnScrollListener(scrollListener);
    }


    private void GetMyDocumentsWebSevice(int page, int per_page) {
        connectionManager.GetMyDocuments(page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response resp) {
                if (resp != null) {
                    Response<DocumentsResponse> response = resp;
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        String message_str = response.body().getStatus().getMessageList();
                        if ("success".equals(response.body().getStatus().getStatus())) {
                            if (page == 1) {
                                documentList = response.body().getDocuments();
                                if (documentList != null) {
                                    setupRecycler();

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                documentList.addAll(response.body().getDocuments());
                                adapter.notifyDataSetChanged();
                            }

                            if (documentList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (response.body().getPaging() != null) {
                                    if (documentList.size() >= response.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if ("error".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(DocumentsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        } else if ("fail".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(DocumentsActivity.this,
                                    getResources().getString(R.string.error), message_str);
                        }

                    }

                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (Isupdate) {
            Intent intent = new Intent();
            intent.putExtra("update", "update");
            setResult(999, intent);
            finish();
        } else
            super.onBackPressed();
    }

    public void setupRecycler() {
        adapter = new AdapterDocument(DocumentsActivity.this, documentList, R.layout.row_document, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (TextUtils.equals("edit", tag))
                            GoToCreateDocumentActivity(documentList.get(position).getDocumentType().getId());
                    }
                });
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        SetLodeMore();
    }

    boolean Isupdate = false;


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                page = 1;
                GetMyDocumentsWebSevice(page, per_page);
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(DocumentsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(DocumentsActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

    AdapterCategoryHome categoryHome;
    List<Category> categoryList = new ArrayList<>();
    private int ItemSelect = -1;

    private void OpenDialogCreateNewDocument() {
        InstallClass installClass = InstallPreference.GetInstall(this);
        categoryList = new ArrayList<>();
        if (installClass != null) {
            if (installClass.getDocumentsTypes() != null) {
                List<DocumentType> documentTypes = installClass.getDocumentsTypes();
                if (documentTypes.size() > 0) {
                    for (int i = 0; i < documentTypes.size(); i++) {
                        categoryList.add(new Category(documentTypes.get(i).getId(), documentTypes.get(i).getName(), documentTypes.get(i).getImage()));
                    }
                    ItemSelect = -1;
                    categoryHome = new AdapterCategoryHome(DocumentsActivity.this, categoryList, ItemSelect, "doc", R.layout.row_category_doc, new OnItemClickTagListener() {
                        @Override
                        public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                            if (TextUtils.equals("view", tag)) {
                                ItemSelect = position;
                                AdapterCategoryHome.ItemSelect = position;
                                categoryHome.notifyDataSetChanged();

                            }
                        }
                    });
                    AppErrorsManager.showAddDocumentsDialog(DocumentsActivity.this, categoryHome, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            if (TextUtils.equals("yes", status)) {
                                if (ItemSelect != -1) {
                                    if (categoryList.size() > ItemSelect) {
                                        GoToCreateDocumentActivity(categoryList.get(ItemSelect).getId());
                                    }
                                } else {
                                    AppErrorsManager.showErrorDialog(DocumentsActivity.this, getResources().getString(R.string.PleaseSelectTypeDocumentToContinue));
                                }
                            }
                        }
                    });
                }
            }
        }

    }


    private void GoToCreateDocumentActivity(int itemSelect) {

        Intent intent = new Intent(getApplicationContext(), AddDocmentActivity.class);
        intent.putExtra("document_type_id", itemSelect);
        if (documentList.size() > 0) {
            String documentList_str = new Gson().toJson(documentList);
            intent.putExtra("documentList_str", documentList_str);
        }
        startActivityForResult(intent, 999);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 999 && resultCode == 999) {
            page = 1;
            GetMyDocumentsWebSevice(page, per_page);
        }
    }
}
