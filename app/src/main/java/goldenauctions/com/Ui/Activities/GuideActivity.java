package goldenauctions.com.Ui.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;

import static goldenauctions.com.Manager.RootManager.KeyGuide;

public class GuideActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView btn_next;
    private TextView text_skip, titleViewFirst, subTitleViewFirst, text_dot_1, text_dot_2;
    private ImageView img_slide;
    private int page = 0;
    private User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_guide);
        initSetUp();
    }

    private void initSetUp() {
        btn_next = findViewById(R.id.btn_next);
        text_skip = findViewById(R.id.text_skip);
        btn_next.setOnClickListener(this);
        text_skip.setOnClickListener(this);
        titleViewFirst = findViewById(R.id.titleViewFirst);
        subTitleViewFirst = findViewById(R.id.subTitleViewFirst);
        text_dot_1 = findViewById(R.id.text_dot_1);
        text_dot_2 = findViewById(R.id.text_dot_2);
        img_slide = findViewById(R.id.img_slide);
        GoToNext();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                GoToNext();
                break;
            case R.id.text_skip:
                page = 2;
                GoToNext();
                break;
        }
    }

    private void GoToNext() {
        YoYo.with(Techniques.BounceInLeft)
                .duration(700)
                .playOn(img_slide);
        YoYo.with(Techniques.BounceInLeft)
                .duration(700)
                .playOn(titleViewFirst);
        YoYo.with(Techniques.BounceInLeft)
                .duration(700)
                .playOn(subTitleViewFirst);
        YoYo.with(Techniques.BounceInLeft)
                .duration(700)
                .playOn(text_dot_1);
        YoYo.with(Techniques.BounceInLeft)
                .duration(700)
                .playOn(text_dot_2);
        titleViewFirst.setText(getResources().getString(R.string.str_slide_title1));
        subTitleViewFirst.setText(getResources().getString(R.string.str_slide_subtitle1));
        img_slide.setImageResource(R.drawable.img_slide_1);
        text_dot_1.setText("222");
        text_dot_2.setText("2");
        text_dot_2.setTextColor(getResources().getColor(R.color.greyLight));
        text_dot_1.setTextColor(getResources().getColor(R.color.colorPrimary));

        text_dot_2.getBackground().setColorFilter(getResources().getColor(R.color.greyLight), PorterDuff.Mode.MULTIPLY);
        text_dot_1.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        page++;
        if (page == 1) {
            titleViewFirst.setText(getResources().getString(R.string.str_slide_title1));
            subTitleViewFirst.setText(getResources().getString(R.string.str_slide_subtitle1));
            img_slide.setImageResource(R.drawable.img_slide_1);
            text_dot_1.setText("222");
            text_dot_2.setText("2");
            text_dot_2.setTextColor(getResources().getColor(R.color.greyLight));
            text_dot_1.setTextColor(getResources().getColor(R.color.colorPrimary));
            text_dot_2.getBackground().setColorFilter(getResources().getColor(R.color.greyLight), PorterDuff.Mode.SRC_IN);
            text_dot_1.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        } else if (page == 2) {
            titleViewFirst.setText(getResources().getString(R.string.str_slide_title2));
            subTitleViewFirst.setText(getResources().getString(R.string.str_slide_subtitle2));
            img_slide.setImageResource(R.drawable.img_slide_2);
            text_dot_1.setText("2");
            text_dot_2.setText("222");

            text_dot_1.setTextColor(getResources().getColor(R.color.greyLight));
            text_dot_2.setTextColor(getResources().getColor(R.color.colorPrimary));


            text_dot_2.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            text_dot_1.getBackground().setColorFilter(getResources().getColor(R.color.greyLight), PorterDuff.Mode.SRC_IN);

        } else {
            user = PreferenceUser.User(GuideActivity.this);
            GoToLoginRegisterActivity();
        }

    }

    private void GoToLoginRegisterActivity() {
        AppPreferences.saveOrEditBoolean(this, KeyGuide, true);
        if (user != null) {
            PreferenceUser.GoToActivityMain(GuideActivity.this, user);

        } else {
            PreferenceUser.GoToActivityMain(GuideActivity.this, null);
        }


    }

}