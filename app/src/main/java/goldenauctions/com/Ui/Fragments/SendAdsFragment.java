package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.appbar.MaterialToolbar;

import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Install.Settings;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.TicketsActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.RootResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ConncationSocial;

public class SendAdsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private TextView text_name_fragment;
    private SwipeRefreshLayout swipeRefreshLayout;
    EditText edit_dialog_title;
    EditText edit_dialog_body;
    private ConnectionManager connectionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_ads, container, false);
        connectionManager = new ConnectionManager(getActivity());
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        text_name_fragment = getActivity().findViewById(R.id.toolbarNameTxt);
        text_name_fragment.setVisibility(View.VISIBLE);
        text_name_fragment.setTextColor(getResources().getColor(R.color.black));
        swipeRefreshLayout = getActivity().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        MaterialToolbar toolbar = getActivity().findViewById(R.id.toolbar);


        ((UserActionActivity) getActivity()).SetStyleWhite(false);

        edit_dialog_title = view.findViewById(R.id.edit_dialog_title);
        edit_dialog_body = view.findViewById(R.id.edit_dialog_body);
        TextView close_dialog = view.findViewById(R.id.close_dialog);
        close_dialog.setOnClickListener(this::onClick);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);


            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_dialog:
                SendAdsWebService();
                break;


        }
    }

    private void SendAdsWebService() {
        if (TextUtils.isEmpty(edit_dialog_title.getText().toString().trim())) {
            edit_dialog_title.setError(getResources().getString(R.string.required_field));
            edit_dialog_title.requestFocus();
        }
        if (TextUtils.isEmpty(edit_dialog_body.getText().toString().trim())) {
            edit_dialog_body.setError(getResources().getString(R.string.required_field));
            edit_dialog_body.requestFocus();
        } else {
            String title = edit_dialog_title.getText().toString().trim();
            String body = edit_dialog_body.getText().toString().trim();
            CreateNewTicketWebService(title, body);
        }
    }

    private void CreateNewTicketWebService(String v1, String v2) {
        connectionManager.AddTickets(v1, v2, new CallbackConncation() {
            @Override
            public void onResponse(Response resp) {
                if (resp != null) {
                    Response<RootResponse> response = resp;
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        List<String> message = response.body().getStatus().getMessage();
                        String message_str = response.body().getStatus().getMessageList();
                        if ("success".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                    getResources().getString(R.string.info), message_str, new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                            Intent intent = new Intent(getActivity(), TicketsActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }
                                    });
                        } else if ("error".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(getActivity(),
                                    getResources().getString(R.string.error), message_str);
                        } else if ("fail".equals(response.body().getStatus().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(getActivity(),
                                    getResources().getString(R.string.error), message_str);
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(getActivity(),
                                getResources().getString(R.string.error), response.message() + "");
                    }

                }
            }
        });

    }
}