package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickListener;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Install.Subscriptions;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.SettingItem;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.Ui.Adapters.AdapterMySubScriptions;
import goldenauctions.com.Ui.Adapters.AdapterSetting;
import goldenauctions.com.Ui.Adapters.AdapterSubscriptions;
import goldenauctions.com.Ui.Adapters.KKViewPager;

public class InsurancePackagesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private TextView text_name_fragment;
    private Button btn_action;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConnectionManager connectionManager;
    private FrameLayout layout_loading;
    private TextView toolbarNameTxt;
    private KKViewPager view_pager;
    private DotsIndicator dotsIndicator;
    private User user = null;


    private AdapterMySubScriptions adapterMySubScriptions;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_insurance_packages, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        user = PreferenceUser.User(getActivity());
        connectionManager = new ConnectionManager(getActivity());
        ((UserActionActivity) getActivity()).SetStyleWhite(false);
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        toolbarNameTxt = getActivity().findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setTextColor(getResources().getColor(R.color.colorBlack));
        toolbarNameTxt.setText(getResources().getString(R.string.Insurance_packages));
        btn_action = view.findViewById(R.id.btn_action);
        btn_action.setVisibility(View.INVISIBLE);
        layout_loading.setOnClickListener(view1 -> {
            return;
        });


        swipeRefreshLayout = getActivity().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        view_pager = view.findViewById(R.id.kk_pager);
        dotsIndicator = view.findViewById(R.id.worm_dots_indicator);
        List<Subscriptions> storyList = new ArrayList<>();
        InstallClass installClass = InstallPreference.GetInstall(getActivity());

        storyList = installClass.getSubscriptions();
        if (storyList != null) {
            if (storyList.size() > 0) {
                btn_action.setVisibility(View.VISIBLE);
                SetValueToViewPager(storyList);
            }
        }


    }

    private int idSelect = -1;

    private void SetValueToViewPager(final List<Subscriptions> storyList) {
        if (storyList.size() > 0) {
            idSelect = storyList.get(0).getId();
        }
        final AdapterSubscriptions adapterComment = new AdapterSubscriptions(getActivity(), storyList, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) throws JSONException, FileNotFoundException {
                idSelect = storyList.get(position).getId();
            }
        });
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                idSelect = storyList.get(position).getId();

            }

            @Override
            public void onPageSelected(int position) {
                idSelect = storyList.get(position).getId();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        view_pager.setAnimationEnabled(true);
        view_pager.setFadeEnabled(true);
        view_pager.setFadeFactor(0.6f);
        view_pager.setAdapter(adapterComment);
        dotsIndicator.setViewPager(view_pager);


    }





    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);


            }
        }, 2000);
    }

}