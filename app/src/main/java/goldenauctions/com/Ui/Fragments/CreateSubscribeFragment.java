package goldenauctions.com.Ui.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.FilePath;
import goldenauctions.com.Hellper.ImagePicker;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.BankAccount;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Install.Subscriptions;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.HomeActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.Ui.Adapters.AdapterBankAccount;
import goldenauctions.com.WebService.model.response.RootResponse;
import okhttp3.MultipartBody;
import retrofit2.Response;

import static android.content.Context.CLIPBOARD_SERVICE;

public class CreateSubscribeFragment extends Fragment implements View.OnClickListener {

    private ImageView img_cash, img_bank;
    private LinearLayout layout_cash, layout_bank, layout_payment_document;
    private ImageView img_payment_document;
    private Button btn_subscription;
    private int payment_type = RootManager.payment_type_Cash;
    File fileSchema;
    int idSubscription = -1;
    private LinearLayout layout_body;
    AdapterBankAccount adapter;
    private List<BankAccount> bankAccountList = new ArrayList<>();
    private TextView text_bank_list;
    private TextView toolbarNameTxt;
    private ConnectionManager connectionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_subscribe, container, false);
        connectionManager = new ConnectionManager(getActivity());
        initView(view);
        return view;
    }

    private void initView(View view) {
        img_cash = view.findViewById(R.id.img_cash);
        img_bank = view.findViewById(R.id.img_bank);
        layout_cash = view.findViewById(R.id.layout_cash);
        layout_bank = view.findViewById(R.id.layout_bank);
        img_payment_document = view.findViewById(R.id.img_payment_document);
        layout_payment_document = view.findViewById(R.id.layout_payment_document);
        btn_subscription = view.findViewById(R.id.btn_subscription);

        text_bank_list = view.findViewById(R.id.text_bank_list);
        text_bank_list.setOnClickListener(this::onClick);

        layout_cash.setOnClickListener(this::onClick);
        layout_bank.setOnClickListener(this::onClick);
        img_payment_document.setOnClickListener(this::onClick);
        btn_subscription.setOnClickListener(this::onClick);
        ((UserActionActivity) getActivity()).SetStyleWhite(false);
        toolbarNameTxt = getActivity().findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setTextColor(getResources().getColor(R.color.colorBlack));
        toolbarNameTxt.setText(getResources().getString(R.string.create_subscription));

        if (getArguments() != null) {
            idSubscription = getArguments().getInt("id");
            List<Subscriptions> storyList = new ArrayList<>();
            InstallClass installClass = InstallPreference.GetInstall(getActivity());

            storyList = installClass.getSubscriptions();
            Subscriptions subscriptions = new Subscriptions();
            if (storyList != null) {
                for (int i = 0; i < storyList.size(); i++) {
                    if (storyList.get(i).getId() == idSubscription) {
                        subscriptions = storyList.get(i);
                    }
                }

            }

            bankAccountList = installClass.getBankAccounts();


            TextView row_text_price_sub = view.findViewById(R.id.row_text_price_sub);
            TextView row_text_name_sub = view.findViewById(R.id.row_text_name_sub);
            ImageView row_img_price_sub = view.findViewById(R.id.row_img_price_sub);
            CardView card_view_item_customer = view.findViewById(R.id.card_view_item_customer);

            if (subscriptions != null) {
                row_text_name_sub.setText("" + subscriptions.getName() + "");
                row_text_price_sub.setText("" + subscriptions.getPrice() + " " + getResources().getString(R.string.dollar_code));

                String image = subscriptions.getImage();
                if (image != null) {
                    Glide.with(getActivity()).load("" + image).into(row_img_price_sub);
                }
            } else {
                card_view_item_customer.setVisibility(View.GONE);
            }
        }


    }


    private void toggelPaymentSelect(ImageView imageViewSelect, ImageView ImageUnSelect) {
        if (imageViewSelect.getTag().equals("0")) {
            ImageUnSelect.setImageDrawable(null);
            imageViewSelect.setTag("1");
            imageViewSelect.setImageResource(R.drawable.ic_true);
            ImageUnSelect.setTag("0");
        }
        if (TextUtils.equals(img_bank.getTag().toString() + "", "1")) {
            payment_type = RootManager.payment_type_Bank;
            layout_payment_document.setVisibility(View.VISIBLE);

        } else {
            payment_type = RootManager.payment_type_Cash;
            layout_payment_document.setVisibility(View.GONE);
            fileSchema = null;
            img_payment_document.setImageResource(R.drawable.receipt);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_cash:
                toggelPaymentSelect(img_cash, img_bank);
                break;
            case R.id.layout_bank:
                toggelPaymentSelect(img_bank, img_cash);

                break;

            case R.id.img_payment_document:
                onImageOnlyPick();

                break;

            case R.id.btn_subscription:
                CreateSubscribeWebService();
                break;

            case R.id.text_bank_list:
                if (bankAccountList != null) {
                    if (bankAccountList.size() > 0) {

                        setupRecycler(bankAccountList);
                    }
                }
                break;
        }

    }

    public void onImageOnlyPick() {
        PackageManager pm = getActivity().getPackageManager();
        int hasPerm2 = pm.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getActivity().getPackageName());
        if (hasPerm2 == PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
            startActivityForResult(chooseImageIntent, REQUEST_IMAGE);
        } else {
            ImagePicker.checkAndRequestPermissions(getActivity());
        }
    }

    private void CreateSubscribeWebService() {
        if (payment_type == RootManager.payment_type_Cash) {
            connectionManager.CreateSubscriptions(idSubscription, payment_type, new CallbackConncation() {
                @Override
                public void onResponse(Response res) {
                    if (res != null) {
                        Response<RootResponse> response = res;

                        if (response.code() == RootManager.RESPONSE_CODE_OK) {
                            String message_str = response.body().getStatus().getMessageList();
                            if ("success".equals(response.body().getStatus().getStatus())) {

                                AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                        getResources().getString(R.string.info), message_str + "", new InstallCallback() {
                                            @Override
                                            public void onStatusDone(String status) {
                                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                ActivityCompat.finishAffinity(getActivity()); //with v4 support library

                                            }
                                        });

                            } else if ("error".equals(response.body().getStatus().getStatus())) {
                                AppErrorsManager.showCustomErrorDialog(getActivity(),
                                        getResources().getString(R.string.error), message_str);
                            } else if ("fail".equals(response.body().getStatus().getStatus())) {
                                AppErrorsManager.showCustomErrorDialog(getActivity(),
                                        getResources().getString(R.string.error), message_str);
                            }

                        } else {
                            AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                    getResources().getString(R.string.error), response.message() + "", new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                        }
                                    });
                        }

                    }

                }
            });


        } else if (payment_type == RootManager.payment_type_Bank) {
            if (fileSchema != null) {
                MultipartBody.Part payment_document = FilePath.GetFilePartFromFile(fileSchema, "payment_detail");
                connectionManager.CreateSubscriptions(idSubscription, payment_type, payment_document, new CallbackConncation() {
                    @Override
                    public void onResponse(Response res) {
                        if (res != null) {
                            Response<RootResponse> response = res;

                            if (response.code() == RootManager.RESPONSE_CODE_OK) {
                                String message_str = response.body().getStatus().getMessageList();
                                if ("success".equals(response.body().getStatus().getStatus())) {

                                    AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                            getResources().getString(R.string.info), message_str + "", new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(intent);
                                                    ActivityCompat.finishAffinity(getActivity()); //with v4 support library

                                                }
                                            });

                                } else if ("error".equals(response.body().getStatus().getStatus())) {
                                    AppErrorsManager.showCustomErrorDialog(getActivity(),
                                            getResources().getString(R.string.error), message_str);
                                } else if ("fail".equals(response.body().getStatus().getStatus())) {
                                    AppErrorsManager.showCustomErrorDialog(getActivity(),
                                            getResources().getString(R.string.error), message_str);
                                }

                            } else {
                                AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                        getResources().getString(R.string.error), response.message() + "", new InstallCallback() {
                                            @Override
                                            public void onStatusDone(String status) {
                                            }
                                        });
                            }

                        }

                    }
                });
            } else {
                AppErrorsManager.showErrorDialog(getActivity(), getResources().getString(R.string.pleaseAddProfileImage));
            }
        }
    }

    public static final int REQUEST_IMAGE = 100;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getImageFromGalleryFile(Intent data) {

        if (data == null) return;
        Uri uri = data.getData();
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            Random r = new Random();
            fileSchema = FilePath.converBitmaptoFile(bitmap, "avatar" + r.nextInt(10000 - 65) + 65, getActivity());
            Glide.with(this).load(uri).into(img_payment_document);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setupRecycler(List<BankAccount> bankAccountList) {
        adapter = new AdapterBankAccount(getActivity(), bankAccountList, R.layout.row_bank_list, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                ClipboardManager myClipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                if (TextUtils.equals("account", tag)) {
                    String text = bankAccountList.get(position).getAccount_number();
                    ClipData myClip = ClipData.newPlainText("text", text);
                    myClipboard.setPrimaryClip(myClip);
                    Toast.makeText(getActivity(), getResources().getString(R.string.Copied), Toast.LENGTH_SHORT).show();

                } else if (TextUtils.equals("iban", tag)) {
                    String text = bankAccountList.get(position).getAccount_iban();
                    ClipData myClip = ClipData.newPlainText("text", text);
                    myClipboard.setPrimaryClip(myClip);
                    Toast.makeText(getActivity(), getResources().getString(R.string.Copied), Toast.LENGTH_SHORT).show();

                }

            }
        });

        OpenDialogBank();

    }

    private void OpenDialogBank() {
        AppErrorsManager.showBanksListDialog(getActivity(), adapter, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {


            }
        });

    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                getImageFromGalleryFile(data);


            }
        }
    }


}
