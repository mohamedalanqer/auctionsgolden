package goldenauctions.com.Ui.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.RootResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.isEmailValid;

public class ForgetPasswordFragment extends Fragment {

    private TextView text_name_fragment;
    private ImageView img_logo;
    private EditText editTextEmail;
    private Button btn_action, btn_skip;
    private ConnectionManager connectionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);

        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        text_name_fragment = view.findViewById(R.id.text_name_fragment);
        img_logo = getActivity().findViewById(R.id.img_logo);
        img_logo.setImageResource(R.drawable.ic_forget);
        text_name_fragment.setText("" + getResources().getString(R.string.ForgetPassword));
        connectionManager = new ConnectionManager(getActivity());
        editTextEmail = view.findViewById(R.id.editTextEmail);
        btn_action = view.findViewById(R.id.btn_action);
        btn_skip = view.findViewById(R.id.btn_skip);
        btn_skip.setOnClickListener(view1 -> {
            ToggleForgetChickUp("");

        });
     //   ((UserActionActivity) getActivity()).SetStyleWhite(false);

        btn_action.setOnClickListener(view1 -> {
            String email = editTextEmail.getText().toString().trim();
            editTextEmail.setError(null);
            editTextEmail.clearFocus();
            if (TextUtils.isEmpty(email)) {
                editTextEmail.setError(getResources().getString(R.string.required_field));
                editTextEmail.findFocus();
            } else if (!isEmailValid(email)) {
                editTextEmail.setError(getString(R.string.valid_email_address));
                editTextEmail.requestFocus();
            } else {

                connectionManager.ForgetPassword(email, new CallbackConncation() {
                    @Override
                    public void onResponse(Response response) {
                        Response<RootResponse> responseResponse = response;
                        if (response.body() != null) {
                            AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(), getResources().getString(R.string.info),
                                    responseResponse.body().getStatus().getMessageList(), new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                            ToggleForgetChickUp(email);
                                        }
                                    });
                        }
                    }
                });
            }
        });

    }

    private void ToggleForgetChickUp(String email) {
        Fragment fragment = new ResetPasswrodFragment();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        fragment.setArguments(bundle);
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();
    }
}