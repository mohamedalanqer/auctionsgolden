package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.SplashActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.RootResponse;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private TextView toolbarNameTxt;

    private FrameLayout layout_loading;
    private EditText edittext_old_password, edittext_new_password, edittext_re_password;
    private Button btn_action;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConnectionManager connectionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());
        toolbarNameTxt = getActivity().findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setText("" + getResources().getString(R.string.ChangePassword));
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setTextColor(getResources().getColor(R.color.colorBlack));
        ((UserActionActivity) getActivity()).SetStyleWhite(false);

        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_action = view.findViewById(R.id.btn_action);
        edittext_new_password = view.findViewById(R.id.edittext_new_password);
        edittext_re_password = view.findViewById(R.id.edittext_re_password);
        edittext_old_password = view.findViewById(R.id.edittext_old_password);
        layout_loading.setOnClickListener(view1 -> {
            return;
        });


        btn_action.setOnClickListener(view1 -> {
            ChangePasswordWebService();
        });

        swipeRefreshLayout = getActivity().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);

    }


    private void ChangePasswordWebService() {
        String password = edittext_old_password.getText().toString().trim();
        String password_new = edittext_new_password.getText().toString().trim();
        String password_confirmation = edittext_re_password.getText().toString().trim();

        edittext_old_password.clearFocus();
        edittext_old_password.setError(null);
        edittext_new_password.clearFocus();
        edittext_new_password.setError(null);
        edittext_re_password.clearFocus();
        edittext_re_password.setError(null);


        if (TextUtils.isEmpty(password)) {
            edittext_old_password.setError(getString(R.string.required_field));
            edittext_old_password.requestFocus();
        } else if (TextUtils.isEmpty(password_new)) {
            edittext_new_password.setError(getString(R.string.required_field));
            edittext_new_password.requestFocus();
        } else if (TextUtils.isEmpty(password_confirmation)) {
            edittext_re_password.setError(getString(R.string.required_field));
            edittext_re_password.requestFocus();
        } else {
            connectionManager.UpdateUserPassword(password, password_new, password_confirmation, new CallbackConncation() {
                @Override
                public void onResponse(Response res) {
                    if (res != null) {
                        Response<RootResponse> response = res;
                        if (response != null)
                            if (response.body() != null) {
                                if (response.body().getStatus() != null) {
                                    String str_message = response.body().getStatus().getMessageList();
                                    if (TextUtils.isEmpty(str_message)) {
                                        str_message = getResources().getString(R.string.Update_successfully);
                                    }
                                    AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(), getResources().getString(R.string.info),
                                            str_message, new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    Intent intent = new Intent(getActivity(), SplashActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);
                                                    getActivity().finish();

                                                }
                                            });

                                }
                            }
                    }
                }
            });
        }


    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                edittext_new_password.setText(null);
                edittext_old_password.setText(null);
                edittext_re_password.setText(null);

                edittext_old_password.clearFocus();
                edittext_old_password.setError(null);
                edittext_new_password.clearFocus();
                edittext_new_password.setError(null);
                edittext_re_password.clearFocus();
                edittext_re_password.setError(null);


            }
        }, 2000);
    }

}