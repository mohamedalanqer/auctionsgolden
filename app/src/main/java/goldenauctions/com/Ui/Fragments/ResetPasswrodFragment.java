package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.Hellper.PinEditText;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.SplashActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.isEmailValid;

public class ResetPasswrodFragment extends Fragment {
    private TextView text_name_fragment;
    private ImageView img_logo;
    private ConnectionManager connectionManager;
    private FrameLayout layout_loading;

    private String email_intent = "";
    private EditText editTextEmail, edittext_new_password, edittext_re_password;
    private PinEditText pinEntry;
    private Button btn_action;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reset_passwrod, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {

        text_name_fragment = view.findViewById(R.id.text_name_fragment);
        img_logo = getActivity().findViewById(R.id.img_logo);
        img_logo.setImageResource(R.drawable.ic_forget);
        text_name_fragment.setText("" + getResources().getString(R.string.Restore_password));

        connectionManager = new ConnectionManager(getActivity());
        connectionManager = new ConnectionManager(getActivity());
        pinEntry = view.findViewById(R.id.txt_pin_entry);
        editTextEmail = view.findViewById(R.id.editTextEmail);
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_action = view.findViewById(R.id.btn_action);
        edittext_new_password = view.findViewById(R.id.edittext_new_password);
        edittext_re_password = view.findViewById(R.id.edittext_re_password);
        layout_loading.setOnClickListener(view1 -> {
            return;
        });
        if (getArguments() != null)
            email_intent = getArguments().getString("email");

        if (!TextUtils.isEmpty(email_intent)) {
            editTextEmail.setVisibility(View.VISIBLE);
            editTextEmail.setText(email_intent);

        }

        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {

                }
            });
        }
        pinEntry.setOnClickListener(view1 -> {
            pinEntry.setText(null);
        });

        btn_action.setOnClickListener(view1 -> {
            ResetPasswordWebService();
        });
    }


    private void ResetPasswordWebService() {
        String password = edittext_new_password.getText().toString().trim();
        String password_confirmation = edittext_re_password.getText().toString().trim();
        String code = pinEntry.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();


        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.required_field));
            editTextEmail.requestFocus();
        }  else if (!isEmailValid(email)) {
            editTextEmail.setError(getString(R.string.valid_email_address));
            editTextEmail.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            edittext_new_password.setError(getString(R.string.required_field));
            edittext_new_password.requestFocus();
        } else if (TextUtils.isEmpty(password_confirmation)) {
            edittext_re_password.setError(getString(R.string.required_field));
            edittext_re_password.requestFocus();
        } else if (TextUtils.isEmpty(code)) {
            pinEntry.setError(getString(R.string.required_field));
            pinEntry.requestFocus();
        } else  {

            layout_loading.setVisibility(View.VISIBLE);
            connectionManager.ResetPassword(email, password, password_confirmation, code, new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    GoToLoginActivity();

                }
            });
        }


    }

    private void GoToLoginActivity() {
        Intent intent = new Intent(getActivity(), SplashActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}