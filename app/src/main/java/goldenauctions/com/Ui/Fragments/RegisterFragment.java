package goldenauctions.com.Ui.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.LoginResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.isEmailValid;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private EditText editTextUserName, editTextEmail, editTextMobile, editTextPassword;
    private TextView title_text, text_password;
    private LinearLayout layout_privacy;
    private CheckBox checkBox_privacy;
    private TextView text_privacy;
    private Button btnAction;
    private ConnectionManager connectionManager;
    private String newToken = "";
    private User user = new User();
    private FrameLayout layout_loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());

        editTextUserName = view.findViewById(R.id.editTextUserName);
        editTextEmail = view.findViewById(R.id.editTextEmail);
        editTextMobile = view.findViewById(R.id.editTextMobile);
        editTextPassword = view.findViewById(R.id.editTextPassword);

        layout_privacy = view.findViewById(R.id.layout_privacy);
        checkBox_privacy = view.findViewById(R.id.checkBox_privacy);
        text_password = view.findViewById(R.id.text_password);
        btnAction = view.findViewById(R.id.btnAction);
        btnAction.setOnClickListener(this::onClick);
        text_privacy = view.findViewById(R.id.text_privacy);
        layout_privacy.setOnClickListener(this::onClick);
        text_privacy.setOnClickListener(this::onClick);

        layout_loading = getActivity().findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(v -> {
            return;
        });

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });


    }

    private void GoToPrivacyActivity() {
        Intent intent = new Intent(getActivity(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.PrivacyPolicy));
        startActivity(intent);
    }

    private void createAccountWebService() {
        String name = editTextUserName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String mobile = editTextMobile.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();


        editTextUserName.clearFocus();
        editTextUserName.setError(null);
        editTextEmail.clearFocus();
        editTextEmail.setError(null);
        editTextMobile.clearFocus();
        editTextMobile.setError(null);
        editTextPassword.clearFocus();
        editTextPassword.setError(null);
        text_privacy.clearFocus();
        text_privacy.setError(null);
        if (TextUtils.isEmpty(name)) {
            editTextUserName.setError(getString(R.string.required_field));
            editTextUserName.requestFocus();
        } else if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.required_field));
            editTextEmail.requestFocus();
        } else if (!isEmailValid(email)) {
            editTextEmail.setError(getString(R.string.valid_email_address));
            editTextEmail.requestFocus();
        } else if (TextUtils.isEmpty(mobile)) {
            editTextMobile.setError(getString(R.string.required_field));
            editTextMobile.requestFocus();
        } else if (mobile.length() == 10 && !mobile.startsWith("05")) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits));
        } else if (mobile.length() > 9 && mobile.length() != 10) {
            if (mobile.startsWith("00966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
            } else if (mobile.startsWith("966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

            } else if (mobile.startsWith("+966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

            } else {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));

            }
            editTextMobile.setError(getResources().getString(R.string.required_field));
            editTextMobile.requestFocus();
        } else if (mobile.length() < 9) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits) + "");
            editTextMobile.setError(getResources().getString(R.string.required_field));
            editTextMobile.requestFocus();

        } else if (TextUtils.isEmpty(password)) {
            editTextPassword.setError(getString(R.string.required_field));
            editTextPassword.requestFocus();
        } else if (!checkBox_privacy.isChecked()) {
            text_privacy.requestFocus();
            text_privacy.setError("");
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.agreePrivacyPolicy));
        } else {
            if (mobile.length() == 10 && mobile.startsWith("05")) {
                mobile = mobile.substring(1);
            }
            mobile = "966" + mobile;
            if (TextUtils.isEmpty(newToken)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        newToken = instanceIdResult.getToken();
                        Log.e("newToken", newToken);

                    }
                });
            }

            String lang = AppLanguage.getLanguage(getActivity());
            User user = new User(name, email, mobile, password, lang, newToken, RootManager.DeviceType);

            HashMap<String, RequestBody> map = GetMapData(user);
            connectionManager.createRegistration(map, null, new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    Response<LoginResponse> responseResponse = response;
                    User user = responseResponse.body().getUser();
                    Gson json = new Gson();
                    String userJson = json.toJson(user);
                    AppPreferences.saveString(getActivity(), "userJson", userJson);
                    if (!userJson.isEmpty()) {
                        if (user != null) {
                            PreferenceUser.GoToActivityMain(getActivity(), user);
                        } else {
                            PreferenceUser.GoToActivityMain(getActivity(), null);
                        }
                    }
                }
            });


        }

    }

    private HashMap<String, RequestBody> GetMapData(User user) {
        HashMap<String, RequestBody> map = new HashMap<>();
        if (!TextUtils.isEmpty(user.getName()))
            map.put("name", RequestBody.create(MultipartBody.FORM, user.getName()));
        if (!TextUtils.isEmpty(user.getMobile()))
            map.put("mobile", RequestBody.create(MultipartBody.FORM, user.getMobile()));
        if (!TextUtils.isEmpty(user.getEmail()))
            map.put("email", RequestBody.create(MultipartBody.FORM, user.getEmail()));
        if (!TextUtils.isEmpty(user.getPassword()))
            map.put("password", RequestBody.create(MultipartBody.FORM, user.getPassword()));
        if (!TextUtils.isEmpty(user.getApp_locale() + ""))
            map.put("app_locale", RequestBody.create(MultipartBody.FORM, user.getApp_locale()));

        if (!TextUtils.isEmpty(user.getDevice_token())) {
            map.put("device_token", RequestBody.create(MultipartBody.FORM, user.getDevice_token()));
            if (!TextUtils.isEmpty(user.getDevice_type() + ""))
                map.put("device_type", RequestBody.create(MultipartBody.FORM, user.getDevice_type()));

        }
        return map;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAction:
                createAccountWebService();
                break;
            case R.id.text_privacy:
                GoToPrivacyActivity();
                break;


        }
    }
}