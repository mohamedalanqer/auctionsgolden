package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.HomeActivity;
import goldenauctions.com.Ui.Activities.LoginRegisterActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.LoginResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.isEmailValid;

public class LoginFragment extends Fragment implements View.OnClickListener {
    private AppCompatEditText editTextEmail, editTextPassword;
    private Button btnAction , btnVistor;
    private TextView textForgetPassword, textCreateAccount;
    private ConnectionManager connectionManager;
    private String newToken = "";
    private FrameLayout layout_loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());
        textCreateAccount = view.findViewById(R.id.textCreateAccount);
        editTextEmail = view.findViewById(R.id.editTextEmail);
        editTextPassword = view.findViewById(R.id.editTextPassword);
        btnAction = view.findViewById(R.id.btnAction);
        btnVistor = view.findViewById(R.id.btnVistor);
        textForgetPassword = view.findViewById(R.id.textForgetPassword);
        btnAction.setOnClickListener(this::onClick);
        btnVistor.setOnClickListener(this::onClick);
        textCreateAccount.setOnClickListener(this::onClick);
        textForgetPassword.setOnClickListener(this::onClick);
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(v -> {
            return;
        });
        if (TextUtils.isEmpty(newToken)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    newToken = instanceIdResult.getToken();
                    Log.e("newToken", newToken);

                }
            });
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textCreateAccount:
                ((LoginRegisterActivity) getActivity()).openFragment(new RegisterFragment());
                break;
            case R.id.btnAction:
                CreateLogingWebService();
                break;
            case R.id.textForgetPassword:
                GoToForgetPasswordActivity();
                break;
            case R.id.btnVistor:
               GoToHomeActivity();
                break;


        }
    }

    private void GoToHomeActivity() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         startActivity(intent);
        getActivity().finish();
    }

    private void GoToForgetPasswordActivity() {
        Intent intent = new Intent(getActivity(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, RootManager.ActionForgetPassword);
        startActivity(intent);
    }

    private void CreateLogingWebService() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getResources().getString(R.string.required_field));
            editTextEmail.requestFocus();
        } else if (!isEmailValid(email)) {
            editTextEmail.setError(getString(R.string.valid_email_address));
            editTextEmail.requestFocus();
        }/**else if (mobile.length() == 10 && !mobile.startsWith("05")) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits));
        } else if (mobile.length() > 9 && mobile.length() != 10) {
            if (mobile.startsWith("00966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
            } else if (mobile.startsWith("966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

            } else if (mobile.startsWith("+966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

            } else {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));

            }
            editTextEmail.setError(getResources().getString(R.string.required_field));
            editTextEmail.requestFocus();
        } else if (mobile.length() < 9) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits) + "");
            editTextEmail.setError(getResources().getString(R.string.required_field));
            editTextEmail.requestFocus();

        } */
        else if (TextUtils.isEmpty(password)) {
            editTextPassword.setError(getResources().getString(R.string.required_field));
            editTextPassword.requestFocus();
        } else {
          /**  if (mobile.length() == 10 && mobile.startsWith("05")) {
                mobile = mobile.substring(1);
            }
            mobile = "966" + mobile;
            Log.e("mobile", mobile);*/
          User user = new User(email, password, newToken, RootManager.DeviceType) ;
          if(TextUtils.isEmpty(newToken)){
              user = new User(email, password ) ;
          }else{
              user = new User(email, password, newToken, RootManager.DeviceType) ;
          }
            connectionManager.createLogin(user, new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    Response<LoginResponse> responseResponse = response;
                    User user = responseResponse.body().getUser();
                    Gson json = new Gson();
                    String userJson = json.toJson(user);
                    AppPreferences.saveString(getActivity(), "userJson", userJson);
                    if (!userJson.isEmpty()) {
                        if (user != null) {
                            PreferenceUser.GoToActivityMain(getActivity(), user);
                        } else {
                            PreferenceUser.GoToActivityMain(getActivity(), null);
                        }

                    }
                }
            });
            ;
        }
    }
}