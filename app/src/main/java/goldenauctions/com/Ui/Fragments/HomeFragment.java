package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.gson.Gson;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Hellper.TimerHome;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.Medoles.Install.Category;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Product;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.AuctionsActivity;
import goldenauctions.com.Ui.Activities.AuctionsDetialsActivity;
import goldenauctions.com.Ui.Activities.HomeActivity;
import goldenauctions.com.Ui.Adapters.AdapterCategoryHome;
import goldenauctions.com.Ui.Adapters.AdsPager;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.enterTransition;
import static goldenauctions.com.Manager.RootManager.returnTransition;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ConnectionManager connectionManager;
    private LinearLayout layout_empty;
    private LinearLayout layout_top;
    ViewPager mViewPager;
    private AdsPager mAdapter;
    private WormDotsIndicator dotsIndicator;
    private RecyclerRefreshLayout swipeRefreshLayout;
    private List<Auction> advertisementList = new ArrayList<>();
    private List<Category> categoryList = new ArrayList<>();
    private AdapterCategoryHome adapterCategory;
    private TextView text_empty;
    private RecyclerView recyclerView;
    private int currentPage = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setSharedElementEnterTransition(enterTransition());
            getActivity().getWindow().setSharedElementReturnTransition(returnTransition());
        }
        initSetUp(view);

        return view;
    }

    private void initSetUp(View view) {

        connectionManager = new ConnectionManager(getActivity());
        mViewPager = view.findViewById(R.id.viewpager);
        dotsIndicator = view.findViewById(R.id.worm_dots_indicator);
        layout_top = view.findViewById(R.id.layout_top);

        recyclerView = view.findViewById(R.id.recyclerView);
        layout_empty = view.findViewById(R.id.layout_empty);
        text_empty = view.findViewById(R.id.text_empty);
        text_empty.setText(getResources().getString(R.string.NotHaveData));
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayoutHome);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        InstallClass installClass = InstallPreference.GetInstall(getActivity());
        if (installClass != null) {
            categoryList = installClass.getCategories();
            if (categoryList != null) {
                if (categoryList.size() > 0) {
                    setupRecyclerCategory();
                }
            }
        }

        /**  advertisementList.add(new Product("https://cdn.pixabay.com/photo/2018/05/22/14/00/girl-3421489_960_720.jpg"));
         advertisementList.add(new Product("https://image.freepik.com/free-photo/beautiful-girl-stands-near-walll-with-leaves_8353-5378.jpg"));
         advertisementList.add(new Product("https://image.freepik.com/free-photo/beautiful-woman-face-with-natural-nude-make-up_120960-1153.jpg"));
         advertisementList.add(new Product("https://image.freepik.com/free-psd/autumn-concept-with-woman-holding-mug_23-2147930919.jpg"));
         advertisementList.add(new Product("https://image.freepik.com/free-photo/brunette-girl-pink-sweater-autumn-park_87910-1918.jpg"));
         */

        GetAuctionsWebSevice(1, 50);
        if (advertisementList != null) {
            if (advertisementList.size() > 0)
                setupRecyclerAdvertisement();
        }
    }

    private void setupRecyclerCategory() {
        adapterCategory = new AdapterCategoryHome(getActivity(), categoryList, -1, R.layout.row_category_home, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (TextUtils.equals("view", tag)) {
                            GoToAuctionsListByCategory(categoryList.get(position));
                        }
                    }
                });
        GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterCategory);
    }

    private void GoToAuctionsListByCategory(Category category) {
        Intent intent = new Intent(getActivity(), AuctionsActivity.class);
        intent.putExtra("category_id", category.getId());
        intent.putExtra("category_name", category.getName());
        startActivityForResult(intent, 999);
    }

    private void GetAuctionsWebSevice(int page, int per_page) {
        connectionManager.GetAuctionsAdvertisement(page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                Log.e("urlresponse", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    Response<AuctionsResponse> responseResponse = response;
                    if (response.body() != null) {
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "success")) {
                            if (page == 1) {
                                advertisementList = responseResponse.body().getAuctions();
                                if (advertisementList != null) {
                                    if (advertisementList.size() > 0) {
                                        setupRecyclerAdvertisement();
                                    } else {
                                        layout_top.setVisibility(View.GONE);
                                    }

                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            layout_top.setVisibility(View.GONE);

                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            layout_top.setVisibility(View.GONE);

                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));

                }


            }
        });

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                currentPage = 0;
                GetAuctionsWebSevice(1, 50);
                mViewPager.setCurrentItem(currentPage);
            }
        }, 2000);
    }

    private void setupRecyclerAdvertisement() {
        if (advertisementList.size() > 0) {
            layout_top.setVisibility(View.VISIBLE);
            mViewPager.setVisibility(View.VISIBLE);
            dotsIndicator.setVisibility(View.VISIBLE);
            mAdapter = new AdsPager(getActivity(), advertisementList, new OnItemClickTagListener() {
                @Override
                public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                    if (TextUtils.equals(tag, "view")) {
                        if (advertisementList.get(position).getCode() != null
                                && !advertisementList.get(position).getCode().isEmpty()) {
                            if (!(advertisementList.get(position).getCode()).contains("noshow"))
                                GoToDetailsAuctionActivty(view, advertisementList.get(position));
                        } else {
                            GoToDetailsAuctionActivty(view, advertisementList.get(position));
                        }
                    }
                }
            });
            mViewPager.setAdapter(mAdapter);
            mViewPager.setCurrentItem(currentPage);
            dotsIndicator.setViewPager(mViewPager);
            TimerHome time = new TimerHome(new Runnable() {
                @Override
                public void run() {
                    int numPages = mViewPager.getAdapter().getCount();
                    currentPage = (currentPage + 1) % numPages;
                    mViewPager.setCurrentItem(currentPage);
                }
            }, 5000, true);

            // new Timer().schedule(timer, 0, 5000);
        } else {
            mViewPager.setVisibility(View.GONE);
            dotsIndicator.setVisibility(View.GONE);
        }
    }

    private void GoToDetailsAuctionActivty(View view, Auction auction) {
        String auction_str = new Gson().toJson(auction, Auction.class);
        Intent intent = new Intent(getActivity(), AuctionsDetialsActivity.class);
        intent.putExtra("ref_id", auction.getId());
        intent.putExtra("auction_str", auction_str);
        Pair<View, String> p1 = Pair.create((View) view.findViewById(R.id.img_avatar), "ImageProduct");
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), p1);
        startActivityForResult(intent, 202, options.toBundle());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && resultCode == 202) {
            // GetAuctionsWebSevice(1, 5);
            ((HomeActivity) getActivity()).refreshActivty();
        }
    }

}