package goldenauctions.com.Ui.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Hellper.FilePath;
import goldenauctions.com.Hellper.ImagePicker;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.Medoles.SettingItem;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.AuctionsDetialsActivity;
import goldenauctions.com.Ui.Activities.DocumentsActivity;
import goldenauctions.com.Ui.Activities.HomeActivity;
import goldenauctions.com.Ui.Activities.SettingActivity;
import goldenauctions.com.Ui.Activities.SplashActivity;
import goldenauctions.com.Ui.Activities.TicketDetialsActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.Ui.Adapters.AdapterMySubScriptions;
import goldenauctions.com.Ui.Adapters.AdapterSetting;
import goldenauctions.com.WebService.model.response.LoginResponse;
import okhttp3.MultipartBody;
import retrofit2.Response;

public class ProfileFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final int REQUEST_IMAGE = 100;
    private ConnectionManager connectionManager;
    private AdapterSetting adapter;
    private RecyclerView recycler_view;
    private ImageView img_avatar;
    private TextView textNameProfile, textEmailProfile;
    File fileSchema;
    private RecyclerRefreshLayout swipeRefreshLayout;
    private User user = new User();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());
        user = PreferenceUser.User(getActivity());
        recycler_view = view.findViewById(R.id.recyclerView);
        textNameProfile = view.findViewById(R.id.textNameProfile);
        textEmailProfile = view.findViewById(R.id.textEmailProfile);
        img_avatar = view.findViewById(R.id.img_avatar);
        img_avatar.setOnClickListener(view1 -> {
            if (user != null) {
                onImageOnlyPick();
            }
        });

        setupRecycler();
        img_avatar.setImageResource(R.drawable.ic_person_24);

        if (user != null) {
            textNameProfile.setText("" + user.getName());
            textEmailProfile.setText("" + user.getEmail());
            if (user.getAvatar() != null) {
                if (!TextUtils.isEmpty(user.getAvatar()))
                    Glide.with(getActivity()).load(user.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
            }
        }
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayoutProfile);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
    }


    public void onImageOnlyPick() {
        PackageManager pm = getActivity().getPackageManager();
        int hasPerm2 = pm.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getActivity().getPackageName());
        if (hasPerm2 == PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
            startActivityForResult(chooseImageIntent, REQUEST_IMAGE);
        } else {
            ImagePicker.checkAndRequestPermissions(getActivity());
        }
    }

    public void setupRecycler() {
        List<SettingItem> settingItems = new ArrayList<>();
        String lang = AppLanguage.getLanguage(getActivity());

        if (user != null) {
            settingItems.add(new SettingItem(1, getResources().getString(R.string.my_personal_information), R.drawable.menu_user));
            settingItems.add(new SettingItem(2, getResources().getString(R.string.ChangePassword), R.drawable.menu_pass));
            settingItems.add(new SettingItem(3, getResources().getString(R.string.my_documents), R.drawable.menu_doc));
            settingItems.add(new SettingItem(4, getResources().getString(R.string.MyPackages), R.drawable.ic_baseline_subtitles_24));
        } else {
            settingItems.add(new SettingItem(5, getResources().getString(R.string.PrivacyPolicy), R.drawable.ic_user_profile));
            settingItems.add(new SettingItem(6, getResources().getString(R.string.Contact_us), R.drawable.ic_phone_list));
            settingItems.add(new SettingItem(7, getResources().getString(R.string.Login), R.drawable.ic_logout_list));
        }

        adapter = new AdapterSetting(getActivity(), settingItems, -1, R.layout.row_item_setting, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                SettingItem settingItem = settingItems.get(position);
                if (TextUtils.equals("view", tag)) {
                    int id = settingItem.getId();
                    if (id == 1) {
                        GoToSettingActivity();
                    } else if (id == 2) {
                        GoToChangePassword();
                    } else if (id == 3) {
                        GoToMyDocumentsActivity();
                    } else if (id == 4) {
                        ShowMyPackages();
                    } else if (id == 5) {
                        GoToPrivacyActivity();
                    } else if (id == 6) {
                        GoToinfoActivity();
                    } else if (id == 7) {
                        logOut();
                    }


                }

                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GoToPrivacyActivity() {
        Intent intent = new Intent(getActivity(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.PrivacyPolicy));
        startActivity(intent);
    }

    private void GoToinfoActivity() {
        Intent intent = new Intent(getActivity(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.AboutApp));
        startActivity(intent);
    }

    private AdapterMySubScriptions adapterMySubScriptions;

    public void ShowMyPackages() {
        user = PreferenceUser.User(getActivity());
        if (user != null) {
            if (user.isIs_subscribed()) {
                if (user.getSubscriptions().size() > 0) {
                    adapterMySubScriptions = new AdapterMySubScriptions(getActivity(), user.getSubscriptions(), R.layout.row_item_my_sub);
                    AppErrorsManager.showMySubSubScriptionsDialog(getActivity(), adapterMySubScriptions);
                } else {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.MyPackages),
                            getResources().getString(R.string.DontHavePackagesActive));
                }
            } else {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.MyPackages),
                        getResources().getString(R.string.DontHavePackagesActive));
            }
        } else {
            AppErrorsManager.showLoginToContinueDialog(getActivity(), new InstallCallback() {
                @Override
                public void onStatusDone(String status) {
                    if (TextUtils.equals("yes", status)) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }


    }

    private void GoToMyDocumentsActivity() {
        Intent intent = new Intent(getActivity(), DocumentsActivity.class);
        startActivity(intent);
    }

    private void GoToSettingActivity() {
        Intent intent = new Intent(getActivity(), SettingActivity.class);
        //intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.ChangePassword));
        startActivity(intent);
    }

    private void GoToChangePassword() {
        Intent intent = new Intent(getActivity(), UserActionActivity.class);
        intent.putExtra(RootManager.ActionIntent, getResources().getString(R.string.ChangePassword));
        startActivity(intent);
    }

    public void logOut() {
        User user = PreferenceUser.User(getActivity());
        if (user != null) {
            AppErrorsManager.showSuccessDialog(getActivity(), getString(R.string.logout), getString(R.string.do_you_want_logout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    connectionManager.logout(new CallbackConncation() {
                        @Override
                        public void onResponse(Response response) {
                            Gson json = new Gson();
                            AppPreferences.saveString(getActivity(), "userJson", "0");
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    });

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        } else {
            Intent intent = new Intent(getActivity(), SplashActivity.class);
            startActivity(intent);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getImageFromGalleryFile(Intent data) {
        if (data == null) return;
        Uri uri = data.getData();
        Toast.makeText(getActivity(), ""+uri.toString(), Toast.LENGTH_SHORT).show();

        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            Random r = new Random();
            fileSchema = FilePath.converBitmaptoFile(bitmap, "avatar" + r.nextInt(10000 - 65) + 65, getActivity());
            Glide.with(this).load(uri).into(img_avatar);
            UplodeAvatar();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void UplodeAvatar() {

        if (fileSchema != null) {
            MultipartBody.Part image_File = FilePath.GetFilePartFromFile(fileSchema, "avatar");
            connectionManager.UpdateAvatar(image_File, new CallbackConncation() {
                @Override
                public void onResponse(Response response) {

                    Response<LoginResponse> responseResponse = response;
                    User user = responseResponse.body().getUser();
                    Gson json = new Gson();
                    String userJson = json.toJson(user);
                    AppPreferences.saveString(getActivity(), "userJson", userJson);
                    AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(), getResources().getString(R.string.info),
                            responseResponse.body().getStatus().getMessageList(), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    if (!userJson.isEmpty()) {
                                        ((HomeActivity) getActivity()).refreshActivty();

                                    }
                                }
                            });


                }
            });
        } else {
            AppErrorsManager.showErrorDialog(getActivity(), getResources().getString(R.string.pleaseAddProfileImage));
        }

    }


    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                getImageFromGalleryFile(data);
            }
        } else if (requestCode == 202 && resultCode == 202) {
            if (user != null) {
                textNameProfile.setText("" + user.getName());
                if (user.getAvatar() != null) {
                    if (!TextUtils.isEmpty(user.getAvatar()))
                        Glide.with(getActivity()).load(user.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
                    textNameProfile.setText("" + user.getName());


                }
            }

        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                GetUserProfileWebService();

            }
        }, 2000);
    }

    private void GetUserProfileWebService() {
        connectionManager.GetProfile(new CallbackConncation() {
            @Override
            public void onResponse(Response response) {
                Response<LoginResponse> responseResponse = response;
                if (RootManager.RESPONSE_CODE_OK == response.code()) {

                    if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "success")) {
                        User user = responseResponse.body().getUser();
                        Gson json = new Gson();
                        String userJson = json.toJson(user);
                        AppPreferences.saveString(getActivity(), "userJson", userJson);
                        if (!userJson.isEmpty()) {
                            if (user != null) {
                                adapter.notifyDataSetChanged();
                                User user_pref = PreferenceUser.User(getActivity());
                                if (user_pref != null) {
                                    textNameProfile.setText("" + user_pref.getName());
                                    textEmailProfile.setText("" + user_pref.getEmail());
                                    if (user_pref.getAvatar() != null) {
                                        if (!TextUtils.isEmpty(user_pref.getAvatar()))
                                            Glide.with(getActivity()).load(user_pref.getAvatar()).error(R.drawable.ic_person_24).into(img_avatar);
                                    }
                                }

                            }
                        }
                    }
                }

            }
        });
    }
}