package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.HashMap;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.SplashActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.LoginResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.isEmailValid;

public class EditProfileFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private TextView toolbarNameTxt;
    private User user = new User();
    private FrameLayout layout_loading;
    private EditText editTextUserName, editTextEmail, editTextMobile;
    private Button btn_action;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConnectionManager connectionManager;
    private String newToken = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());
        toolbarNameTxt = getActivity().findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setText("" + getResources().getString(R.string.EditProfile));
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setTextColor(getResources().getColor(R.color.colorBlack));
        ((UserActionActivity) getActivity()).SetStyleWhite(false);

        user = PreferenceUser.User(getActivity());

        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_action = view.findViewById(R.id.btn_action);
        editTextUserName = view.findViewById(R.id.editTextUserName);
        editTextEmail = view.findViewById(R.id.editTextEmail);
        editTextMobile = view.findViewById(R.id.editTextMobile);
        layout_loading.setOnClickListener(view1 -> {
            return;
        });

        if (user != null) {
            if (!TextUtils.isEmpty(user.getEmail()))
                editTextEmail.setText("" + user.getEmail());
            if (!TextUtils.isEmpty(user.getMobile())) {
                editTextMobile.setText("" + user.getMobile());
                String mobile = user.getMobile() ;
                if (mobile.length() > 9  && mobile.startsWith("966")) {
                    mobile = mobile.substring(3);
                    if(mobile.length() == 9 &&  mobile.startsWith("5")){
                        mobile ="0"+mobile ;
                    }
                    editTextMobile.setText("" + mobile);
                }


            } if (!TextUtils.isEmpty(user.getName()))
                editTextUserName.setText("" + user.getName());




        }

        btn_action.setOnClickListener(view1 -> {
            EditProfileWebService();
        });
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });
        swipeRefreshLayout = getActivity().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);

    }


    private void EditProfileWebService() {
        String name = editTextUserName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String mobile = editTextMobile.getText().toString().trim();


        editTextUserName.clearFocus();
        editTextUserName.setError(null);
        editTextEmail.clearFocus();
        editTextEmail.setError(null);
        editTextMobile.clearFocus();
        editTextMobile.setError(null);

        if (TextUtils.isEmpty(name)) {
            editTextUserName.setError(getString(R.string.required_field));
            editTextUserName.requestFocus();
        } else if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.required_field));
            editTextEmail.requestFocus();
        } else if (!isEmailValid(email)) {
            editTextEmail.setError(getString(R.string.valid_email_address));
            editTextEmail.requestFocus();
        } else if (TextUtils.isEmpty(mobile)) {
            editTextMobile.setError(getString(R.string.required_field));
            editTextMobile.requestFocus();
        } else if (mobile.length() == 10 && !mobile.startsWith("05")) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits));
        } else if (mobile.length() > 9 && mobile.length() != 10) {
            if (mobile.startsWith("00966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
            } else if (mobile.startsWith("966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

            } else if (mobile.startsWith("+966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

            } else {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));

            }
            editTextMobile.setError(getResources().getString(R.string.required_field));
            editTextMobile.requestFocus();
        } else if (mobile.length() < 9) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits) + "");
            editTextMobile.setError(getResources().getString(R.string.required_field));
            editTextMobile.requestFocus();

        } else {
            if (mobile.length() == 10 && mobile.startsWith("05")) {
                mobile = mobile.substring(1);
            }
            mobile = "966" + mobile;
            if (TextUtils.isEmpty(newToken)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        newToken = instanceIdResult.getToken();
                        Log.e("newToken", newToken);

                    }
                });
            }

            String lang = AppLanguage.getLanguage(getActivity());
            User user = new User(name, email, mobile, lang, newToken, RootManager.DeviceType);

            HashMap<String, RequestBody> map = GetMapData(user);
            connectionManager.editProfile(map, new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    Response<LoginResponse> responseResponse = response;
                    User user = responseResponse.body().getUser();
                    Gson json = new Gson();
                    String userJson = json.toJson(user);
                    AppPreferences.saveString(getActivity(), "userJson", userJson);
                    if (!userJson.isEmpty()) {
                        ((UserActionActivity) getActivity()).setUpdate(true);
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                                responseResponse.body().getStatus().getMessageList() + "");
                    }
                }
            });


        }

    }

    private HashMap<String, RequestBody> GetMapData(User user) {
        HashMap<String, RequestBody> map = new HashMap<>();
        if (!TextUtils.isEmpty(user.getName()))
            map.put("name", RequestBody.create(MultipartBody.FORM, user.getName()));
        if (!TextUtils.isEmpty(user.getMobile()))
            map.put("mobile", RequestBody.create(MultipartBody.FORM, user.getMobile()));
        if (!TextUtils.isEmpty(user.getEmail()))
            map.put("email", RequestBody.create(MultipartBody.FORM, user.getEmail()));
        if (!TextUtils.isEmpty(user.getApp_locale() + ""))
            map.put("app_locale", RequestBody.create(MultipartBody.FORM, user.getApp_locale()));

        if (!TextUtils.isEmpty(user.getDevice_token())) {
            map.put("device_token", RequestBody.create(MultipartBody.FORM, user.getDevice_token()));
            if (!TextUtils.isEmpty(user.getDevice_type() + ""))
                map.put("device_type", RequestBody.create(MultipartBody.FORM, user.getDevice_type()));

        }
        return map;
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                editTextUserName.setText(null);
                editTextEmail.setText(null);
                editTextMobile.setText(null);

                editTextUserName.clearFocus();
                editTextUserName.setError(null);
                editTextEmail.clearFocus();
                editTextEmail.setError(null);
                editTextMobile.clearFocus();
                editTextMobile.setError(null);

            }
        }, 2000);
    }

}