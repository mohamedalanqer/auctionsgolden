package goldenauctions.com.Ui.Fragments;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.Hellper.PinEditText;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppPreferences;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.WebService.model.response.LoginResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class VerifyFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private TextView text_name_fragment ;
    private ImageView img_logo ;
    private static final int REQ_USER_CONSENT = 200;
    private PinEditText pinEntry;
    private TextView text_new_code, text_your_phone;
    private Button btn_action, btn_skip;
    private FrameLayout layout_loading;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConnectionManager connectionManager;
    private int TypeVerify = RootManager.VerificationTypeMobile;
    private TextView   text_body, text_tag_new_send;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        registerBroadcastReceiver();
        View view  = inflater.inflate(R.layout.fragment_verify, container, false);
        initSetUp(view);
        return view ;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());
        if (getArguments() != null) {
            TypeVerify = getArguments().getInt("TypeVerify", RootManager.VerificationTypeMobile);
        }
        text_name_fragment = view.findViewById(R.id.text_name_fragment);
        text_name_fragment.setText(""+getResources().getString(R.string.VerifyEmail));
        pinEntry = view.findViewById(R.id.txt_pin_entry);
        text_new_code = view.findViewById(R.id.text_new_code);
        img_logo = getActivity().findViewById(R.id.img_logo);
        img_logo.setImageResource(R.drawable.ic_verify);

        btn_action = view.findViewById(R.id.btn_action);
        btn_skip = view.findViewById(R.id.btn_skip);

        layout_loading = getActivity().findViewById(R.id.layout_loading);
        text_your_phone = view.findViewById(R.id.text_your_phone);

        text_body = view.findViewById(R.id.text_body);
        text_tag_new_send = view.findViewById(R.id.text_tag_new_send);


        User user = PreferenceUser.User(getActivity());
        if (user != null) {
            text_your_phone.setText(getResources().getString(R.string.your_phone)+" " +"" + user.getMobile());
        }
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {

                }
            });
        }
        pinEntry.setOnClickListener(view1 -> {
            pinEntry.setText(null);
        });

        text_new_code.setOnClickListener(view1 -> {
            if (TextUtils.equals(getResources().getString(R.string.Request_verification_code), text_new_code.getText().toString())) {
                startTimer(getActivity(), 60000, text_new_code);
                ResendVerifyWebService();
            }
        });
        btn_action.setOnClickListener(view1 -> {
            pinEntry.clearFocus();
            pinEntry.setError(null);
            String code = "" + pinEntry.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                pinEntry.setError(getResources().getString(R.string.required_field));
                pinEntry.requestFocus();
            } else {
                code = "" + pinEntry.getText().toString().trim();
                VerifyWebService(code);
            }
        });

        btn_skip.setOnClickListener(v ->
        {
            if (user != null) {
                PreferenceUser.GoToActivityMainSKip(getActivity(), user);
            } else {
                PreferenceUser.GoToActivityMainSKip(getActivity(), null);
            }
        });
        swipeRefreshLayout = getActivity().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        if (TypeVerify == RootManager.VerificationTypeEmail) {
            if (user != null) {
                text_your_phone.setText(getResources().getString(R.string.your_email)+" " + user.getEmail());
            }
            text_name_fragment.setText("" + getResources().getString(R.string.Confirm_Email));
            text_body.setText("" + getResources().getString(R.string.Confirm_Email_message));
            text_tag_new_send.setText("" + getResources().getString(R.string.Did_receive_code_Email));




        }else if (TypeVerify == RootManager.verifyAccount) {

            btn_skip.setVisibility(View.VISIBLE);
            TypeVerify = RootManager.VerificationTypeEmail;
            if (user != null) {
                text_your_phone.setText(getResources().getString(R.string.your_email)+" " +"" + user.getEmail());
            }
            text_name_fragment.setText("" + getResources().getString(R.string.Confirm_Email));
            text_body.setText("" + getResources().getString(R.string.Confirm_Email_message));
            text_tag_new_send.setText("" + getResources().getString(R.string.Did_receive_code_Email));



        } else{
            startSmsUserConsent();

        }


    }

    private void ResendVerifyWebService() {
        connectionManager.ResendVerify(TypeVerify, new CallbackConncation() {
            @Override
            public void onResponse(Response res) {
                if (res != null) {
                    Response<RootResponse> response = res;
                    String message_str = getResources().getString(R.string.create_success);
                    if (response.body().getStatus() != null) {
                        if (response.body().getStatus().getMessage() != null)
                            message_str = response.body().getStatus().getMessageList();
                    }

                    if ("success".equals(response.body().getStatus().getStatus())) {
                        AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                getResources().getString(R.string.info), message_str, new InstallCallback() {
                                    @Override
                                    public void onStatusDone(String status) {
                                    }
                                });
                    }
                }
            }
        });

    }

    public static void startTimer(final Context context, long time, final TextView textView) {
        textView.setEnabled(false);
        CountDownTimer counter = new CountDownTimer(time, 1) {
            public void onTick(long millisUntilDone) {

                Log.d("counter_label", "Counter text should be changed");
                long Secand = millisUntilDone / 1000;
                long minutes = (Secand % 3600) / 60;
                long seconds = Secand % 60;

                String timeString = String.format("%02d:%02d", minutes, seconds );
                textView.setText("" + timeString + "");
            }

            public void onFinish() {
                textView.setText(context.getResources().getString(R.string.request_now));
                textView.setEnabled(true);

            }
        }.start();
    }

    private void VerifyWebService(String code) {
        connectionManager.Verify(TypeVerify, code, new CallbackConncation() {
            @Override
            public void onResponse(Response res) {
                if (res != null) {
                    Response<LoginResponse> response = res;
                    String message_str = response.body().getStatus().getMessageList();
                    if ("success".equals(response.body().getStatus().getStatus())) {
                        User user = response.body().getUser();
                        Gson json = new Gson();
                        String userJson = json.toJson(user);
                        AppPreferences.saveString(getActivity(), "userJson", userJson);
                        AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(),
                                getResources().getString(R.string.info), message_str, new InstallCallback() {
                                    @Override
                                    public void onStatusDone(String status) {

                                        if (!userJson.isEmpty()) {
                                            if (user != null) {
                                                PreferenceUser.GoToActivityMain(getActivity(), user);

                                            } else {
                                                PreferenceUser.GoToActivityMain(getActivity(), null);
                                            }
                                        }


                                    }
                                });
                    }
                }

            }
        });

    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                pinEntry.clearFocus();
                pinEntry.setError(null);
                pinEntry.setText(null);
            }
        }, 2000);
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{5}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            pinEntry.setText(matcher.group(0));
        }
    }

    private BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get consent intent
                        Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                        try {
                            // Start activity to show consent dialog to user, activity must be started in
                            // 5 minutes, otherwise you'll receive another TIMEOUT intent
                            startActivityForResult(consentIntent, REQ_USER_CONSENT);
                        } catch (ActivityNotFoundException e) {
                            // Handle the exception ...
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Time out occurred, handle the error.
                        break;
                }
            }
        }
    };

    private void registerBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        getActivity().registerReceiver(smsVerificationReceiver, intentFilter);
    }


    @Override
    public void onStart() {
        super.onStart();
        registerBroadcastReceiver();

    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(smsVerificationReceiver);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}