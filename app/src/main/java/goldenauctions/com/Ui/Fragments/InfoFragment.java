package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import goldenauctions.com.Hellper.InstallPreference;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Install.InstallClass;
import goldenauctions.com.Medoles.Install.Settings;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.UserActionActivity;

import static goldenauctions.com.Manager.RootManager.ConncationSocial;

public class InfoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private TextView text_name_fragment;
    private TextView text_body;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout layout_contact;
    private ImageView img_phone, img_email, img_instagram, img_fb;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        text_name_fragment = view.findViewById(R.id.text_name_fragment);
        swipeRefreshLayout = getActivity().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        layout_contact = view.findViewById(R.id.layout_contact);
        layout_contact.setVisibility(View.GONE);


        img_phone = view.findViewById(R.id.img_phone);
        img_email = view.findViewById(R.id.img_email);
        img_instagram = view.findViewById(R.id.img_instagram);
        img_fb = view.findViewById(R.id.img_fb);
        img_phone.setOnClickListener(this::onClick);
        img_email.setOnClickListener(this::onClick);
        img_instagram.setOnClickListener(this::onClick);
        img_fb.setOnClickListener(this::onClick);


        text_name_fragment.setText("" + getResources().getString(R.string.PrivacyPolicy));
        ((UserActionActivity) getActivity()).SetStyleWhite(false);

        text_body = view.findViewById(R.id.text_body);
        InstallClass installClass = InstallPreference.GetInstall(getActivity());
        if (installClass != null) {
            String privacys= installClass.getSettings().getPrivacy();
            String ters= installClass.getSettings().getTerms();
            Log.e("ppppp",privacys +" pr");
            Log.e("ppppp",ters +" ters");

            if (getArguments() != null) {
                String action = getArguments().getString(RootManager.ActionIntent);
                if (TextUtils.equals(action, getResources().getString(R.string.PrivacyPolicy))) {
                    text_name_fragment.setText("" + getResources().getString(R.string.PrivacyPolicy));
                    String privacy = installClass.getSettings().getPrivacy();
                    text_body.setText(Html.fromHtml(privacy +""));
                } else if (TextUtils.equals(action, getResources().getString(R.string.Termsofuse))) {
                    text_name_fragment.setText("" + getResources().getString(R.string.Termsofuse));
                    String privacy = installClass.getSettings().getTerms();
                    text_body.setText(Html.fromHtml(privacy +""));
                } else if (TextUtils.equals(action, getResources().getString(R.string.AboutApp))) {
                    text_name_fragment.setText("" + getResources().getString(R.string.AboutApp));
                    String privacy = installClass.getSettings().getAbout();
                    text_body.setText(Html.fromHtml(privacy +""));
                    layout_contact.setVisibility(View.VISIBLE);
                }

            }

        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                InstallClass installClass = InstallPreference.GetInstall(getActivity());
                if (installClass != null) {
                    if (getArguments() != null) {
                        String action = getArguments().getString(RootManager.ActionIntent);
                        if (TextUtils.equals(action, getResources().getString(R.string.PrivacyPolicy))) {
                            text_name_fragment.setText("" + getResources().getString(R.string.PrivacyPolicy));
                            String privacy = installClass.getSettings().getPrivacy();
                            text_body.setText(Html.fromHtml(privacy +""));
                        } else if (TextUtils.equals(action, getResources().getString(R.string.Termsofuse))) {
                            text_name_fragment.setText("" + getResources().getString(R.string.Termsofuse));
                            String privacy = installClass.getSettings().getTerms();
                            text_body.setText(Html.fromHtml(privacy +""));
                        }

                    }

                }
            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_phone:
                ContactUs(1);
                break;
            case R.id.img_email:
                ContactUs(2);
                break;
            case R.id.img_instagram:
                ContactUs(3);
                break;
            case R.id.img_fb:
                ContactUs(4);
                break;

        }
    }

    private void ContactUs(int i) {
        InstallClass installClass = InstallPreference.GetInstall(getActivity());
        if (installClass != null) {
            if(installClass.getSettings() != null) {
                Settings settings = installClass.getSettings() ;
                if (i == 1) {
                    if (settings.getMobile() != null){
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+settings.getMobile()));
                        this.startActivity(intent);
                    }
                } else if (i == 2) {
                    if (settings.getEmail() != null){
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto",settings.getEmail(), null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + " :  "+getResources().getString(R.string.NeedHellp));
                        startActivity(Intent.createChooser(emailIntent, "Send email..."));
                    }
                } else if (i == 3) {
                    if (settings.getInstagram() != null){
                        Intent   intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("" + ConncationSocial(settings.getInstagram(), "instagram.com/")));
                        this.startActivity(intent);
                    }
                } else if (i == 4) {
                    if (settings.getFacebook() != null){
                        Intent   intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("" + ConncationSocial(settings.getFacebook(), "facebook.com/")));
                        this.startActivity(intent);
                    }
                }
            }
        }
    }
}