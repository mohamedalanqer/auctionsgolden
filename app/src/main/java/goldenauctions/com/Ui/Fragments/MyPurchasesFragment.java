package goldenauctions.com.Ui.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.OnItemClickTagListener;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.ConnectionManager;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.Medoles.Auction;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.AuctionsDetialsActivity;
import goldenauctions.com.Ui.Activities.HomeActivity;
import goldenauctions.com.Ui.Adapters.AdapterFollowUp;
import goldenauctions.com.Ui.Adapters.RecyclerViewScrollListener;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.enterTransition;
import static goldenauctions.com.Manager.RootManager.returnTransition;

public class MyPurchasesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ConnectionManager connectionManager;
    private LinearLayout layout_empty;
    private RecyclerRefreshLayout swipeRefreshLayout;
    private TextView text_empty;
    private RecyclerView recycler_view;
    AdapterFollowUp adapter;
    List<Auction> auctionList = new ArrayList<>();
    boolean IsLodeMore = false;
    private RecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    int page = 1;
    int per_page = 10;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_purchases, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setSharedElementEnterTransition(enterTransition());
            getActivity().getWindow().setSharedElementReturnTransition(returnTransition());
        }
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        connectionManager = new ConnectionManager(getActivity());

        recycler_view = view.findViewById(R.id.recycler_view);
        layout_empty = view.findViewById(R.id.layout_empty);
        text_empty = view.findViewById(R.id.text_empty);
        text_empty.setText(getResources().getString(R.string.NotHaveMyPurchases));

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayoutPurchases);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);

        page = 1;
        GetMyPurchasesWebSevice(page, per_page);
    }

    private void GetMyPurchasesWebSevice(int page, int per_page) {
        connectionManager.GetMyPurchases(true, page, per_page, new CallbackConncation() {
            @Override
            public void onResponse(Response response) {

                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {

                        Response<AuctionsResponse> responseResponse = response;
                        String message_str = responseResponse.body().getStatus().getMessageList();
                        if ("success".equals(responseResponse.body().getStatus().getStatus())) {
                            if (page == 1) {
                                auctionList = responseResponse.body().getAuctions();
                                if (auctionList != null) {
                                    if (auctionList.size() > 0) {
                                        setupRecycler();
                                    } else {
                                        layout_empty.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }
                            } else {
                                auctionList.addAll(responseResponse.body().getAuctions());
                                adapter.notifyDataSetChanged();
                            }

                            if (auctionList.size() == 0) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                IsLodeMore = false;
                            } else {
                                recycler_view.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);

                                if (responseResponse.body().getPaging() != null) {
                                    if (auctionList.size() >= responseResponse.body().getPaging().getTotal()) {
                                        IsLodeMore = false;
                                    } else {
                                        IsLodeMore = true;
                                    }
                                } else {
                                    IsLodeMore = false;
                                }
                            }
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "error")) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + message_str);
                            IsLodeMore = false;
                        } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), "fail")) {
                            text_empty.setText("" + message_str);
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            IsLodeMore = false;
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                        text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                        recycler_view.setVisibility(View.GONE);
                        layout_empty.setVisibility(View.VISIBLE);
                        IsLodeMore = false;
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error), getResources().getString(R.string.InternalServerError));
                    text_empty.setText("" + getResources().getString(R.string.InternalServerError));
                    recycler_view.setVisibility(View.GONE);
                    layout_empty.setVisibility(View.VISIBLE);
                    IsLodeMore = false;
                }


            }
        });

    }

    public void setupRecycler() {
        adapter = new AdapterFollowUp(getActivity(), auctionList , "my_purchases",R.layout.row_my_purchase, new
                OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        if (TextUtils.equals(tag, "view")) {
                            String ref_id = auctionList.get(position).getId() + "";
                            if (!TextUtils.isEmpty(ref_id)) {
                                GoToDetailsAuctionActivty(view, auctionList.get(position));
                            }
                        }
                    }
                });
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

        SetLodeMore();
    }


    private void FavouriteAuctionsToggle(Auction auction, int pos) {
        if (auction != null) {
            connectionManager.FavouriteAuctionsToggle(auction.getId(), new CallbackConncation() {
                @Override
                public void onResponse(Response response) {
                    if (response.body() != null) {
                        Response<AuctionResponse> auctionResponse = response;
                        if (auctionResponse.body() != null) {
                            String message = auctionResponse.body().getStatus().getMessageList();
                            Toasty.success(getActivity(), getResources().getString(R.string.info) + " : " + message);
                            if (auctionResponse.body().getAuction() != null) {
                                auctionList.remove(pos);
                                adapter.notifyItemRemoved(pos);

                                if (auctionList.size() == 0) {
                                    layout_empty.setVisibility(View.VISIBLE);
                                }


                            }
                        }
                    }
                }
            });
        }

    }


    private void GoToDetailsAuctionActivty(View view, Auction auction) {
        String auction_str = new Gson().toJson(auction, Auction.class);
        Intent intent = new Intent(getActivity(), AuctionsDetialsActivity.class);
        intent.putExtra("ref_id", auction.getId());
        intent.putExtra("auction_str", auction_str);
        Pair<View, String> p1 = Pair.create((View) view.findViewById(R.id.image_row), "ImageProduct");
        Pair<View, String> p2 = Pair.create((View) view.findViewById(R.id.text_title), "NameProduct");
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), p1, p2);
        startActivityForResult(intent, 202, options.toBundle());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && resultCode == 202) {
        /**    page = 1;
            GetMyPurchasesWebSevice(page, per_page);*/
            ((HomeActivity)getActivity()).refreshActivty( );
        }
    }

    private void SetLodeMore() {
        scrollListener = new RecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page_, int totalItemsCount) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (IsLodeMore) {
                            page++;
                            GetMyPurchasesWebSevice(page, per_page);

                        }

                    }
                }, 1000);

            }

            @Override
            public void onReachTop(boolean isFirst) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        recycler_view.addOnScrollListener(scrollListener);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                page = 1;
                GetMyPurchasesWebSevice(page, per_page);

            }
        }, 2000);
    }


}