package goldenauctions.com.ChatKit;

import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.utils.DateFormatter;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.UUID;

import goldenauctions.com.App.MyApplication;
import goldenauctions.com.R;

/**
 * Created by mohammedk.abuhalib on 12/26/17.
 */


public class OutcomingVoiceMessageViewHolder
        extends MessageHolders.OutcomingTextMessageViewHolder<Message> {

    private TextView tvDuration;
    private TextView tvTime;
    private ImageButton playImageButton;
    private ImageButton pauseImageButton;
    private SeekBar voiceSeekBar;
    private ProgressBar progressBarLoading;
    private String sessionId;
    private boolean isTouching = false;
    private PlayerStatus playerStatus = null;

    public OutcomingVoiceMessageViewHolder(View itemView) {
        super(itemView);
        tvDuration = itemView.findViewById(R.id.duration);
        tvTime = itemView.findViewById(R.id.time);
        playImageButton = itemView.findViewById(R.id.playImageButton);
        pauseImageButton = itemView.findViewById(R.id.pauseImageButton);
        voiceSeekBar = itemView.findViewById(R.id.voiceSeekBar);
        progressBarLoading = itemView.findViewById(R.id.progressBarLoading);
        sessionId = UUID.randomUUID().toString();
        initSeekBar();
    }

    @Override
    public void onBind(final Message message) {
        super.onBind(message);
        tvDuration.setText(Utilities.getDurationString(0));
        tvTime.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
        playImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(true){
                    sessionId = UUID.randomUUID().toString();
                    EventBus.getDefault().post(sessionId);
                    downloadAudio(message.getVoice().getUrl()+""); //+".m4a"
                }else {
                    downloadAudio(message.getVoice().getUrl()+""); //+".m4a"
                    statusPlaying();
                }
                Log.e("getVoice",message.getVoice().getUrl()+"");
            }
        });

        pauseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusPaused();
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14){
                    retriever.setDataSource(message.getVoice().getUrl()+"", new HashMap<String, String>());
                }
                else {
                    retriever.setDataSource(message.getVoice().getUrl() + "");
                }
                String mVideoDuration =  retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                final int mTimeInMilliseconds= (int) (Long.parseLong(mVideoDuration)/1000);

                tvDuration.post(new Runnable() {
                    @Override
                    public void run() {
                        tvDuration.setText(
                                Utilities.getDurationString(mTimeInMilliseconds));
                    }
                });
            }
        }).start();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                if (Build.VERSION.SDK_INT >= 14){
//                    retriever.setDataSource(message.getVoice().getUrl()+".m4a", new HashMap<String, String>());
//                }
//                else {
//                    retriever.setDataSource(message.getVoice().getUrl() + ".m4a");
//                }
//                String mVideoDuration =  retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                final int mTimeInMilliseconds= (int) (Long.parseLong(mVideoDuration)/1000);
//
//                tvDuration.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        tvDuration.setText(
//                                FormatUtils.getDurationString(mTimeInMilliseconds));
//                    }
//                });
//            }
//        }).start();

    }

    private File mediaFile;

    private void downloadAudio(final String mediaUrl) {
        handlePlayClick();
        String filePath = ChatActivity.cachedVoices.get(mediaUrl);
        if(filePath !=  null) {
            playVoice(filePath);
            return;
        }
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        URLConnection cn = new URL(mediaUrl).openConnection();
                        cn.connect();
                        InputStream is = cn.getInputStream();
                        mediaFile = new File(MyApplication.getInstance().getCacheDir(),"mediafile");
                        FileOutputStream fos = new FileOutputStream(mediaFile);
                        byte buf[] = new byte[16 * 1024];
                        do {
                            int numread = is.read(buf);
                            if (numread <= 0)
                                break;
                            fos.write(buf, 0, numread);
                        } while (true);
                        fos.flush();
                        fos.close();
                        ChatActivity.cachedVoices.put(mediaUrl,mediaFile.getAbsolutePath());
                        voiceSeekBar.post(new Runnable() {
                            @Override
                            public void run() {
                                playVoice(mediaFile.getAbsolutePath());
                            }
                        });
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playVoice(String voiceUri) {
        if (ChatActivity.mediaPlayer != null) {
            initMediaPlayer();
            if (ChatActivity.mediaPlayer.isPlaying())
                ChatActivity.mediaPlayer.stop();
            ChatActivity.mediaPlayer.reset();
            try {
                ChatActivity.mediaPlayer.setDataSource(voiceUri);
                ChatActivity.mediaPlayer.prepare();
                ChatActivity.mediaPlayer.seekTo((voiceSeekBar.getProgress()*50));
                ChatActivity.mediaPlayer.start();
                handleDownloadComplete();
                handleSeekBar(ChatActivity.mediaPlayer.getDuration());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initMediaPlayer(){
        ChatActivity.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                playerCompleted();
            }
        });
    }


    private void initSeekBar(){
        voiceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isTouching = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(ChatActivity.mediaPlayer.isPlaying()){
                    ChatActivity.mediaPlayer.seekTo((seekBar.getProgress()*50));
                    progressCounter = seekBar.getProgress();
                }
                isTouching = false;
            }
        });
    }

    private int progressCounter = 0;
    private void handleSeekBar(final int duration){
        final int netDuration = duration/50;
        voiceSeekBar.setMax(netDuration);
        progressCounter = voiceSeekBar.getProgress();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (progressCounter = voiceSeekBar.getProgress();
                     progressCounter <netDuration ; progressCounter++) {
                    Log.d("Test", "run: position: "+sessionId);
                    Log.d("Test", "run: activity position: "+ChatActivity.currentSession);
                    if(sessionId.equalsIgnoreCase(ChatActivity.currentSession)) {
                        SystemClock.sleep(50);
                        final int finalI = progressCounter;
                        voiceSeekBar.post(new Runnable() {
                            @Override
                            public void run() {
                                if(!isTouching) {
                                    voiceSeekBar.setProgress(finalI);
                                }
                            }
                        });

                    }else {
                        voiceSeekBar.post(new Runnable() {
                            @Override
                            public void run() {
                                if(pauseImageButton.getVisibility() == View.VISIBLE){
                                    playImageButton.setVisibility(View.VISIBLE);
                                    pauseImageButton.setVisibility(View.GONE);
                                    progressBarLoading.setVisibility(View.GONE);
                                    playerStatus = PlayerStatus.paused;
                                    isTouching = false;
                                }else{

                                }
                            }
                        });
                        return;
                    }
                }
            }
        }).start();

    }

    private void handlePlayClick(){
        playImageButton.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    private void handleDownloadComplete(){
        statusPlaying();
    }
    private void statusPlaying(){
        playImageButton.setVisibility(View.GONE);
        pauseImageButton.setVisibility(View.VISIBLE);
        progressBarLoading.setVisibility(View.GONE);
        playerStatus = PlayerStatus.playing;
        isTouching = false;
    }

    private void statusPaused(){

        ChatActivity.mediaPlayer.pause();
        isTouching = true;
        playImageButton.setVisibility(View.VISIBLE);
        pauseImageButton.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.GONE);
        playerStatus = PlayerStatus.playing;

    }

    private void playerCompleted(){

        voiceSeekBar.setProgress(0);
        playImageButton.setVisibility(View.VISIBLE);
        pauseImageButton.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.GONE);

    }

    enum PlayerStatus{
        playing, paused
    }

}
