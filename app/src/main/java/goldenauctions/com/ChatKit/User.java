package goldenauctions.com.ChatKit;

import com.stfalcon.chatkit.commons.models.IUser;

/**
 * Created by mohammedk.abuhalib on 12/26/17.
 */

public class User implements IUser {

    private String id;
    private String name;
    private String avatar;
    private String typeUser ;

    public User(String id, String name, String avatar, String typeUser) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.typeUser = typeUser;
    }

    public User(String id, String name, String avatar) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }
}
