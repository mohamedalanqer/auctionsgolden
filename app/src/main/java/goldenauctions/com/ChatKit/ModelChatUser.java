package goldenauctions.com.ChatKit;

import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;

import goldenauctions.com.Manager.RootManager;

public class ModelChatUser {

    String chatRoomId;
    String chatRoomName;
    String chatRoomPic;
    String userId;
    String displayName;
    String userPic;
    String LastMsg;
    Timestamp creationDate;

    public String getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(String chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getChatRoomName() {
        return chatRoomName;
    }

    public void setChatRoomName(String chatRoomName) {
        this.chatRoomName = chatRoomName;
    }

    public String getChatRoomPic() {
        return chatRoomPic;
    }

    public void setChatRoomPic(String chatRoomPic) {
        this.chatRoomPic = chatRoomPic;
    }

    public String getLastMsg() {

        return LastMsg;
    }

    public void setLastMsg(String lastMsg) {
        LastMsg = lastMsg;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}
