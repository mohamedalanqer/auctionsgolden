package goldenauctions.com.ChatKit;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

import goldenauctions.com.CallBack.OnItemClickListener;
import goldenauctions.com.Hellper.UtilitiesDate;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.R;

public class AdapterAllChat extends RecyclerView.Adapter<AdapterAllChat.CustomViewHolder> {
    private List<ModelChatUser> data;
    private Context mContext;
    private OnItemClickListener clickListener ;

    public AdapterAllChat(Context context, List<ModelChatUser> data) {
        this.data = data;
        this.mContext = context;
    }
    public AdapterAllChat(Context context, List<ModelChatUser> data , OnItemClickListener onItemClickListener) {
        this.data = data;
        this.mContext = context;
        this.clickListener = onItemClickListener ;
    }
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.row_all_chat, viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, final int position) {

        customViewHolder.text_title.setText(data.get(position).getChatRoomName() + "");
        String LastMsg = data.get(position).getLastMsg()+"" ;
        customViewHolder.text_body.setText(LastMsg);
       /** if(LastMsg != null){
            if(TextUtils.equals(RootManager.CHATIMAGEKEY, LastMsg)){
                customViewHolder.text_body.setText(mContext.getResources().getString(R.string.Image));
            }else if(TextUtils.equals(RootManager.CHATVOICEKEY, LastMsg)){
                customViewHolder.text_body.setText(mContext.getResources().getString(R.string.Voice));
            }
        }*/
        long time = (data.get(position).getCreationDate().getSeconds() * 1000);
        customViewHolder.text_date.setText("" + UtilitiesDate.getDate(time));
        customViewHolder.image_row.setImageResource(R.drawable.person_placeholder);

        Glide.with(mContext).load(data.get(position).getChatRoomPic())
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).error(R.drawable.no_image)
                .into(customViewHolder.image_row);

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    clickListener.onItemClick(view ,position);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
             }
        });

    }


    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }

    public List<ModelChatUser> getItems() {

        return data;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView text_title, text_body;
        ImageView image_row;
        TextView text_date;

        public CustomViewHolder(View view) {
            super(view);
            text_title = view.findViewById(R.id.text_title);
            image_row = view.findViewById(R.id.image_row);
            text_body = view.findViewById(R.id.text_body);
            text_date = view.findViewById(R.id.text_date);
        }
    }
}
