package goldenauctions.com.ChatKit;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import goldenauctions.com.App.MyApplication;
import goldenauctions.com.Hellper.FilePath;
import goldenauctions.com.Hellper.PathUtil;
import goldenauctions.com.Hellper.PreferenceUser;
import goldenauctions.com.Manager.AppErrorsManager;
import goldenauctions.com.Manager.AppLanguage;
import goldenauctions.com.Manager.RootManager;
import goldenauctions.com.R;
import goldenauctions.com.WebService.RetrofitWebService;
import goldenauctions.com.WebService.model.response.RootResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static goldenauctions.com.Manager.RootManager.ActionKeyLocalBroadcastManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatActivity extends AppCompatActivity implements
        MessageHolders.ContentChecker<Message>, MessagesListAdapter.OnLoadMoreListener,
        View.OnTouchListener, View.OnClickListener, MessagesListAdapter.OnMessageClickListener<Message> {

    private RelativeLayout layout_back;
    private TextView toolbarNameTxt;
    private ImageView img_avatar_toolbar;

    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final int PERMISSION_RECORDING_REQUEST_CODE = 300;
    private static final int PICK_IMAGE_ID = 200;
    MessagesList messagesList;
    FrameLayout frameImageCapture, frameVoiceRecording, frameSendMessage;
    EditText editTextMessage;
    ImageView imageViewSend;
    private LinearLayout linearRecording;
    private TextView textViewRecordingText;
    private TextView textViewRecordingCounter;
    private View viewLoading;
    ConstraintLayout constraintLayoutContainer;
    AVLoadingIndicatorView loadingIndicator;
    public static MediaPlayer mediaPlayer = new MediaPlayer();
    private static final byte CONTENT_TYPE_VOICE = 1;
    public static HashMap<String, String> cachedVoices = new HashMap<>();
    public static String currentSession = "";
    boolean isEnabledToSend = false;
    FirebaseFirestore db;
    FirebaseStorage storage;
    CollectionReference ref;
    MessagesListAdapter<Message> adapter;
    private String displayName = "";
    private String cimpanyId = "500";
    private DocumentSnapshot lastDocument = null;
    private static final byte VIEW_TYPE_TEXT_MESSAGE = 1;
    private String chatRoomId = "ROOM_ID";

    private String getCollectionName() {
        String name = "";
        if (user != null)
            name = chatRoomId + "::" + user.getId();
        return name;
    }

    private String userPic, user_id;
    boolean firstTime = false;
    Map<String, Object> map = new HashMap<>();
    String share_message = "";
    private goldenauctions.com.Medoles.Install.User user = new goldenauctions.com.Medoles.Install.User();

    private int SizeChat = 1;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        setContentView(R.layout.activity_chat);

        FirebaseApp.initializeApp(this);
        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        chatRoomId = getIntent().getStringExtra(RootManager.ROOM_ID);
        boolean ROOM_ISEnable = getIntent().getBooleanExtra(RootManager.ROOM_ISEnable, true);
        if (ROOM_ISEnable == false) {
            LinearLayout messageInputChat = findViewById(R.id.messageInputChat);
            messageInputChat.setVisibility(View.GONE);
        }


        user = PreferenceUser.User(this);
        if (user != null) {
            user_id = user.getId() + "";
            userPic = user.getAvatar();
            displayName = user.getName() + "";
        }

        String ROOM_IMAGE = getIntent().getStringExtra(RootManager.ROOM_IMAGE);
        String ROOM_NAME = getIntent().getStringExtra(RootManager.ROOM_NAME);

        initUIMainComponents();
        ref = db.collection("chat::").document(getCollectionName()).collection(getCollectionName());
        map.put("chatRoomId", getCollectionName());
        map.put("chatRoomName", ROOM_NAME);
        map.put("chatRoomPic", ROOM_IMAGE);

        map.put("userId", user_id);
        map.put("displayName", displayName);
        map.put("userPic", userPic);


        db.collection("usersChat::").document(user.getId() + "").collection("chat::")
                .whereEqualTo("chatRoomId", getCollectionName()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.size() == 0) {
                    firstTime = true;
                }
                SizeChat = queryDocumentSnapshots.size();
            }
        });


        fetchMessages(false);
        fetchRealTimeMessages();

        MessageHolders holders = new MessageHolders()
                .registerContentType(
                        CONTENT_TYPE_VOICE,
                        IncomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_incoming_voice_message,
                        OutcomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_outcoming_voice_message,
                        this);

        adapter = new MessagesListAdapter<>(user_id, holders, imageLoader);
        adapter.setLoadMoreListener(this);
        adapter.setOnMessageClickListener(this);
        messagesList.setAdapter(adapter);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);

    }

    //mohamed alanqer
    private int GetCounterMessages() {
        final int[] count = {0};
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    int i = 0;
                    for (DocumentSnapshot document : task.getResult()) {
                        i++;
                    }
                    Log.d("TAGref", "count : " + count[0], task.getException());
                    count[0] = i;
                } else {
                    Log.d("TAGref", "Error getting documents: ", task.getException());
                    count[0] = 0;
                }
            }
        });
        return count[0];
    }


    private void initUIMainComponents() {

        layout_back = findViewById(R.id.end_main);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        messagesList = findViewById(R.id.messagesList);
        frameImageCapture = findViewById(R.id.frameImageCapture);
        frameVoiceRecording = findViewById(R.id.frameVoiceRecording);
        frameSendMessage = findViewById(R.id.frameSendMessage);
        editTextMessage = findViewById(R.id.editTextMessage);
        imageViewSend = findViewById(R.id.imageViewSend);
        linearRecording = findViewById(R.id.linearRecording);
        textViewRecordingText = findViewById(R.id.textViewRecordingText);
        textViewRecordingCounter = findViewById(R.id.textViewRecordingCounter);
        viewLoading = findViewById(R.id.viewLoading);
        frameVoiceRecording.setOnTouchListener(this);
        frameImageCapture.setOnClickListener(this);
        frameSendMessage.setOnClickListener(this);
        constraintLayoutContainer = findViewById(R.id.constraintLayoutContainer);
        loadingIndicator = findViewById(R.id.loadingIndicator);
        loadingIndicator.setVisibility(View.VISIBLE);
        constraintLayoutContainer.setVisibility(View.INVISIBLE);
        loadMoreMessages = new ArrayList<>();
        if (share_message != null && !TextUtils.isEmpty(share_message)) {
            editTextMessage.setText("" + share_message);
            if (editTextMessage.getText().toString().trim().length() > 0) {
                isEnabledToSend = true;
                frameSendMessage.setBackground(ActivityCompat.getDrawable(MyApplication.getInstance(), R.drawable.corner_reduis_selected));
                imageViewSend.setImageDrawable(ActivityCompat.getDrawable(MyApplication.getInstance(), R.drawable.ic_chat_send_white));
            }

        }
        handleMessageTextChange();
        toolbarNameTxt.setText(" " + getResources().getString(R.string.Chat) + "");
        toolbarNameTxt.setVisibility(View.VISIBLE);

        img_avatar_toolbar = findViewById(R.id.img_avatar_toolbar);
        String ROOM_IMAGE = getIntent().getStringExtra(RootManager.ROOM_IMAGE);
        String ROOM_NAME = getIntent().getStringExtra(RootManager.ROOM_NAME);
        if (ROOM_IMAGE != null) {
            Glide.with(this).load(ROOM_IMAGE + "").error(R.drawable.ic_person_24).into(img_avatar_toolbar);
        }
        if (ROOM_NAME != null) {
            toolbarNameTxt.setText(" " + ROOM_NAME + "");
        }


        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void hideIndicator() {
        loadingIndicator.setVisibility(View.INVISIBLE);
        constraintLayoutContainer.setVisibility(View.VISIBLE);
    }

    ObjectAnimator objectAnimator = null;

    private void showUploadingLoading() {
        viewLoading.setVisibility(View.VISIBLE);
        objectAnimator = ObjectAnimator
                .ofFloat(viewLoading, "x", -Utilities.convertDpToPixel(150),
                        Utilities.convertDpToPixel(Utilities.getScreenWidthInDPs(this)));
        objectAnimator.setDuration(1000);
        objectAnimator.setRepeatMode(ValueAnimator.RESTART);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.start();
    }

    private void hideUploadingLoading() {
        viewLoading.setVisibility(View.INVISIBLE);
        if (objectAnimator != null) {
            objectAnimator = null;
        }

    }

    private void handleMessageTextChange() {
        editTextMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editTextMessage.getText().toString().trim().length() > 0) {
                    isEnabledToSend = true;
                    frameSendMessage.setBackground(ActivityCompat.getDrawable(MyApplication.getInstance(), R.drawable.corner_reduis_selected));
                    imageViewSend.setImageDrawable(ActivityCompat.getDrawable(MyApplication.getInstance(), R.drawable.ic_chat_send_white));
                } else {
                    isEnabledToSend = false;
                    imageViewSend.setImageDrawable(ActivityCompat.getDrawable(MyApplication.getInstance(), R.drawable.ic_chat_send_black));
                    frameSendMessage.setBackground(ActivityCompat.getDrawable(MyApplication.getInstance(), R.drawable.corner_reduis_selected));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    ImageLoader imageLoader = new ImageLoader() {
        @Override
        public void loadImage(ImageView imageView, String url) {
            Glide.with(getApplicationContext())
                    .load(url)
                    .error(R.drawable.person_placeholder)
                    .into(imageView);

            // Picasso.with(getApplicationContext()).load(url).into(imageView);
        }
    };

    @Override
    public boolean hasContentFor(Message message, byte type) {
        switch (type) {
            case CONTENT_TYPE_VOICE:
                return message.getVoice() != null
                        && message.getVoice().getUrl() != null
                        && !message.getVoice().getUrl().isEmpty();
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAdapterSoundPlay(String session) {
        currentSession = session;
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void onSubmit() {
        if (isEnabledToSend) {
            sendTextMessage(editTextMessage.getText().toString().trim());
            SaveChatRoomData(editTextMessage.getText().toString().trim() + "");
            editTextMessage.setText("");

        }
    }

    private MediaRecorder recorder;
    private String voiceMediaPath = null;

    private void recordingVoice() {
        if (checkRecordingPermission()) {

            try {
                recorder = new MediaRecorder();
                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
                recorder.setAudioChannels(1);
                recorder.setAudioSamplingRate(44100);
                recorder.setAudioEncodingBitRate(96000);
                voiceMediaPath = MyApplication.getInstance().getCacheDir() + File.separator + "mediafile";
                recorder.setOutputFile(voiceMediaPath);
                recorder.prepare();
                recorder.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            requestRecordingPermissions();
        }
    }

    private void onImagePick() {
        if (checkPermission()) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent chooseImageIntent = ImagePicker.getPickImageIntentMalti(this);
            startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);

            SaveChatRoomData(RootManager.CHATIMAGEKEY);


        } else {
            requestReadAndWritePermissions();
        }
    }

    boolean hasAttachment = false;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_ID:
                if (data != null) {
                    Uri uri = data.getData();

                    hasAttachment = true;
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    uploadImage(bitmap);

                } else {
                    hasAttachment = true;
                    File file = new File(this.getExternalCacheDir(), "tempImage");
                    if (file.exists()) {
                        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
                        //   uploadImage(bitmap);
                        uploadImage(bitmap);
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    Handler handler;


    private void requestReadAndWritePermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
    }

    private void requestRecordingPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_RECORDING_REQUEST_CODE);
    }

    private boolean checkPermission() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkRecordingPermission() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
    }


    /* MARK: Firestore
     * */

    private boolean isFirstTimeToLoad = true;

    private void fetchMessages(final boolean isLoadMore) {
        Query query = ref.orderBy("creationDate", Query.Direction.DESCENDING).limit(6);
        if (lastDocument != null) {
            //    query = query.startAfter(lastDocument.getTimestamp("creationDate").getSeconds());
            if (lastDocument.get("creationDate") + "" != null) {
                boolean digitsOnly = TextUtils.isDigitsOnly(lastDocument.getTimestamp("creationDate").toString());
                if (digitsOnly) {
                    query = query.startAfter(lastDocument.getTimestamp("creationDate"));
                } else {
                    query = query.startAfter(lastDocument.getTimestamp("creationDate").getSeconds());
                }
            }
        }

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    List<DocumentSnapshot> documentSnapshots = task.getResult().getDocuments();
                    if (!isLoadMore)
                        Collections.reverse(documentSnapshots);

                    for (DocumentSnapshot document : documentSnapshots) {
                        extractMessage(document, isLoadMore);
                    }
                    if (documentSnapshots.size() > 0) {
                        if (!isLoadMore) {
                            lastDocument = documentSnapshots.get(0);
                        } else {
                            lastDocument = documentSnapshots.get(documentSnapshots.size() - 1);
                        }
                    }

                    if (loadMoreMessages.size() > 0) {
                        adapter.addToEnd(loadMoreMessages, false);
                        loadMoreMessages.clear();
                        documentSnapshots.clear();
                    }

                    if (isFirstTimeToLoad) {
                        hideIndicator();
                        isFirstTimeToLoad = false;
                    }
                }
            }
        });
    }

    private void fetchRealTimeMessages() {

        ref.whereGreaterThanOrEqualTo("creationDate", Timestamp.now()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
                    if (documentChange.getType() == DocumentChange.Type.ADDED) {
                        extractMessage(documentChange.getDocument(), false);
                    }
                }
            }
        });
    }


    private void sendTextMessage(String text) {
        FieldValue creationDate = FieldValue.serverTimestamp();//System.currentTimeMillis();
        HashMap<String, Object> messageHashMap = new HashMap<>();
        messageHashMap.put("type", "TEXT");
        messageHashMap.put("creationDate", creationDate);
        messageHashMap.put("senderId", user_id);
        messageHashMap.put("displayName", displayName);
        messageHashMap.put("text", text);
        messageHashMap.put("userPic", userPic);
        ref.add(messageHashMap);

        isUplode = true;

        /**if (isFirstNotifcation)
         SendNotfication(chatRoomId, text);*/


    }

    private void sendTextMessageHi(String text, String senderId) {
        FieldValue creationDate = FieldValue.serverTimestamp(); // System.currentTimeMillis();
        HashMap<String, Object> messageHashMap = new HashMap<>();
        messageHashMap.put("type", "TEXT");
        messageHashMap.put("creationDate", creationDate);
        messageHashMap.put("senderId", senderId);
        messageHashMap.put("displayName", displayName);
        messageHashMap.put("text", text);
        messageHashMap.put("userPic", userPic);
        ref.add(messageHashMap);

        /** if (isFirstNotifcation)
         SendNotfication(chatRoomId, text);*/
    }

    private void sendImageMessage(String downloadUrl) {
        FieldValue creationDate = FieldValue.serverTimestamp(); // System.currentTimeMillis();
        HashMap<String, Object> messageHashMap = new HashMap<>();
        messageHashMap.put("type", "IMAGE");
        messageHashMap.put("creationDate", creationDate);
        messageHashMap.put("senderId", user_id);
        messageHashMap.put("displayName", displayName);
        messageHashMap.put("downloadUrl", downloadUrl);
        messageHashMap.put("userPic", userPic);
        ref.add(messageHashMap);
        /**if (isFirstNotifcation)
         SendNotfication(chatRoomId, "مرفق صورة");*/
        isUplode = true;
    }

    private void sendImageMessageFile(String downloadUrl) {
        FieldValue creationDate = FieldValue.serverTimestamp(); //System.currentTimeMillis();
        HashMap<String, Object> messageHashMap = new HashMap<>();
        messageHashMap.put("type", "IMAGE");
        messageHashMap.put("creationDate", creationDate);
        messageHashMap.put("senderId", user_id);
        messageHashMap.put("displayName", displayName);
        messageHashMap.put("downloadUrl", downloadUrl);
        messageHashMap.put("userPic", userPic);
        ref.add(messageHashMap);
        /** if (isFirstNotifcation)
         SendNotfication(chatRoomId, "مرفق ملف");*/
        isUplode = true;
    }

    private void sendVoiceMessage(String downloadUrl) {
        FieldValue creationDate = FieldValue.serverTimestamp(); //System.currentTimeMillis();
        HashMap<String, Object> messageHashMap = new HashMap<>();
        messageHashMap.put("type", "VOICE");
        messageHashMap.put("creationDate", creationDate);
        messageHashMap.put("senderId", user_id);
        messageHashMap.put("displayName", displayName);
        messageHashMap.put("downloadUrl", downloadUrl);
        messageHashMap.put("userPic", userPic);
        ref.add(messageHashMap);
        isUplode = true;
        /** if (isFirstNotifcation)
         SendNotfication(chatRoomId, "مقطع صوتي");*/

    }


    private void extractMessage(DocumentSnapshot document, Boolean isLoadMore) {
        long time = System.currentTimeMillis();
        if (document.get("creationDate") != null) {
            boolean digitsOnly = TextUtils.isDigitsOnly(document.get("creationDate").toString());
            if (digitsOnly) {
                time = document.getLong("creationDate");
            } else {
                Timestamp timestamp = document.getTimestamp("creationDate");
                if (timestamp != null) {
                    time = (timestamp.getSeconds() * 1000);
                }
            }
        }


        if (document.getString("type").equalsIgnoreCase("TEXT")) {
            addTextMessage(document.getId(), document.getString("senderId"),
                    document.getString("displayName"), document.getString("text"), time, isLoadMore, document.getString("userPic"), document.getString("typeUser"));
        } else if (document.getString("type").equalsIgnoreCase("IMAGE")) {
            addImageMessage(document.getId(), document.getString("senderId"),
                    document.getString("displayName"), document.getString("downloadUrl"), time, isLoadMore, document.getString("userPic"), document.getString("typeUser"));

        } else if (document.getString("type").equalsIgnoreCase("VOICE")) {
            addVoiceMessage(document.getId(), document.getString("senderId"),
                    document.getString("displayName"), document.getString("downloadUrl"), time, isLoadMore, document.getString("userPic"), document.getString("typeUser"));
        }
    }


    List<Message> loadMoreMessages;

    private void addTextMessage(String id, String senderId,
                                String displayName, String text, long creationDate, Boolean isLoadMore, String userPic, String typeUser) {

        String avatar = "http://socialventurechallenge.asia/wp-content/uploads/2016/06/person-placeholder.jpg";
        if (userPic != null) {
            avatar = userPic;
        }

        Log.e("avatar", avatar);
        Log.e("avatar", userPic + "");
        User user = new User(senderId, displayName, avatar, typeUser);
        Message message = new Message(id, text, user, Utilities.getDateChat(creationDate));
        if (!isLoadMore) {
            adapter.addToStart(message, true);
        } else {
            loadMoreMessages.add(message);
        }
    }

    private void addImageMessage(String id, String senderId, String displayName,
                                 String downloadUrl, long creationDate, Boolean isLoadMore, String userPic, String typeUser) {
        Message.Image image = new Message.Image(downloadUrl);

        String avatar = "http://socialventurechallenge.asia/wp-content/uploads/2016/06/person-placeholder.jpg";
        if (userPic != null) {
            avatar = userPic;
        }

        Log.e("avatar", avatar);
        Log.e("avatar", userPic + "");
        User user = new User(senderId, displayName, avatar, typeUser);
        Message message = new Message(id, image, user, Utilities.getDateChat(creationDate));
        if (!isLoadMore) {
            adapter.addToStart(message, true);
        } else {
            loadMoreMessages.add(message);
        }
    }

    private void addVoiceMessage(String id, String senderId, String displayName,
                                 String downloadUrl, long creationDate, Boolean isLoadMore, String userPic, String typeUser) {
        Message.Voice voice = new Message.Voice(downloadUrl, 0);

        String avatar = "http://socialventurechallenge.asia/wp-content/uploads/2016/06/person-placeholder.jpg";
        if (userPic != null) {
            avatar = userPic;
        }
        Log.e("avatar", avatar);
        Log.e("avatar", userPic + "");

        User user = new User(senderId, displayName, avatar, typeUser);
        Message message = new Message(id, voice, user, Utilities.getDateChat(creationDate));
        if (!isLoadMore) {
            adapter.addToStart(message, true);
        } else {
            loadMoreMessages.add(message);
        }
    }

    private boolean isUplode = false;


    private void uploadImage(Bitmap bitmap) {
        showUploadingLoading();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] data = baos.toByteArray();
        final StorageReference imageRef = storage.getReference("android::" + user_id + "::" + System.currentTimeMillis());
        UploadTask uploadTask = imageRef.putBytes(data);

        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return imageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String uri = downloadUri.toString();
                    sendImageMessage(uri);
                    hideUploadingLoading();
                } else {
                    // Handle failures
                    // ...
                }
            }
        });
    }

    private void SaveChatRoomData(String value) {
        if (firstTime) {
            FieldValue creationDate = FieldValue.serverTimestamp();
            map.put("creationDate", creationDate);
            map.put("LastMsg", value);
            db.collection("usersChat::").document(user.getId() + "").collection("chat::")
                    .document(getCollectionName()).set(map)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                            }

                        }
                    });
        } else {
            FieldValue creationDate = FieldValue.serverTimestamp();
            map.put("creationDate", creationDate);
            map.put("LastMsg", value);
            db.collection("usersChat::").document(user.getId() + "").collection("chat::")
                    .document(getCollectionName()).update("LastMsg", value, "creationDate", creationDate)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                            }
                        }
                    });
        }

    }

    private void uploadVoice() {
        showUploadingLoading();
        final StorageReference voiceRef = storage.getReference("android::" + user_id + "::" + System.currentTimeMillis() + ".m4a");
        Uri file = Uri.fromFile(new File(voiceMediaPath));
        final UploadTask uploadTask = voiceRef.putFile(file);


        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return voiceRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String uri = downloadUri.toString();
                    sendVoiceMessage(uri);
                    hideUploadingLoading();
                    SaveChatRoomData(RootManager.CHATVOICEKEY);


                } else {
                    // Handle failures
                    // ...
                }
            }
        });
    }


    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (firstTime != true) {
            fetchMessages(true);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int a = event.getAction();

        if (frameVoiceRecording == view) {
            if (a == MotionEvent.ACTION_DOWN) {
                if (checkRecordingPermission()) {
                    handleDownRecording();
                    Vibrator vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                } else {
                    requestRecordingPermissions();
                    return false;
                }
            } else if (a == MotionEvent.ACTION_UP) {
                handleUpRecording();
            }
        }
        return true;
    }

    private void handleDownRecording() {
        recordingVoice();
        handleRecordingCounter();
    }

    private void handleUpRecording() {
        if (recorder != null) {
            try {
                recorder.stop();
                recorder.release();
                recorder = null;
                uploadVoice();
                hideRecordingView();
            } catch (RuntimeException stopException) {
                displayRecordingViews();
                hideRecordingView();
                sec = 0;
            }

        }

        if (downTimer != null)
            downTimer.cancel();
    }

    int sec = 0, min = 0, hr = 0;

    private void displayRecordingViews() {
        editTextMessage.setVisibility(View.GONE);
        frameSendMessage.setVisibility(View.GONE);
        frameImageCapture.setVisibility(View.GONE);
        linearRecording.setVisibility(View.VISIBLE);
    }

    private void hideRecordingView() {
        editTextMessage.setVisibility(View.VISIBLE);
        frameSendMessage.setVisibility(View.VISIBLE);
        frameImageCapture.setVisibility(View.VISIBLE);
        linearRecording.setVisibility(View.GONE);
    }


    CountDownTimer downTimer;

    private void handleRecordingCounter() {
        displayRecordingViews();
        downTimer = new CountDownTimer(300000000, 1000) {
            public void onTick(long millisUntilFinished) {
                sec++;
                if (sec == 59) {
                    min++;
                    sec = 0;
                }
                if (min == 59) {
                    min = 0;
                    hr++;
                }
                if (hr == 23) {
                    hr = 00;
                }
                String counter = String.format("%02d:%02d:%02d", hr, min, sec);
                textViewRecordingCounter.setText(counter);
                if (textViewRecordingText.getText().toString().equalsIgnoreCase(getString(R.string.recording) + "...")) {
                    textViewRecordingText.setText(getString(R.string.recording) + ".");
                } else {
                    textViewRecordingText.setText(textViewRecordingText.getText().toString() + ".");
                }

            }

            public void onFinish() {

            }
        }.start();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.frameSendMessage) {
            onSubmit();
        } else if (view.getId() == R.id.frameImageCapture) {
            onImagePick();
        }
    }


    @Override
    public void onMessageClick(Message message) {
        if (message.getImageUrl() != null && !message.getImageUrl().isEmpty()) {
            String extension = message.getImageUrl().substring(message.getImageUrl().lastIndexOf(".") + 1);
            Log.e("extension", extension);
            if (TextUtils.equals("pdf", extension) || TextUtils.equals("doc", extension)) {
                DownlodeFile(message.getImageUrl());
            } else {
                imagePreview(message.getImageUrl());
            }
        }
    }

    private void DownlodeFile(String bath) {
        if (!bath.isEmpty()) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bath + ""));
            startActivity(browserIntent);
        }
    }

    public void imagePreview(String imageUrl) {
        AppErrorsManager.showImage(ChatActivity.this, imageUrl + "");

    }


    boolean isFirstNotifcation = true;

    @Override
    public void onBackPressed() {
        if (isUplode) {
            Intent intent = new Intent();
            intent.putExtra("update", "update");
            setResult(202, intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }


    /**
     * private void SendNotfication(String id, String message) {
     * isFirstNotifcation = false;
     * RetrofitWebService.getService(this).SendNotification(id, "" + message)
     * .enqueue(new Callback<RootResponse>() {
     *
     * @Override public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
     * Log.e("response", response.toString());
     * <p>
     * }
     * @Override public void onFailure(Call<RootResponse> call, Throwable t) {
     * Log.e("response", t.getMessage().toString());
     * <p>
     * }
     * });
     * <p>
     * }
     */

    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            return;
        }
    };

}
