package goldenauctions.com.ChatKit;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by mohammedk.abuhalib on 12/1/17.
 */

public class Utilities
{
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate(long milliSeconds)
    {
        String dateFormat = "dd MMM yyyy";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Date getDateChat(long milliSeconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return calendar.getTime();
    }

    public static String getDate(String fullDate, Locale locale){
        String dateFormatFrom = "dd/MM/yyyy HH:mm:ss aa";
        SimpleDateFormat fromformatter = new SimpleDateFormat(dateFormatFrom, locale);
        String newDateString = "";
        String dateFormatTo = "dd MMM yyyy";
        SimpleDateFormat formatterTo = new SimpleDateFormat(dateFormatTo);

        try {
            Date date = fromformatter.parse(fullDate);
            newDateString = formatterTo.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDateString;
    }

    public static long convertJsonDate(String jsonDate){
        Pattern pattern = Pattern
                .compile("^\\/date\\((-?\\d++)(?:([+-])(\\d{2})(\\d{2}))?\\)\\/$", Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(jsonDate);
        if(m.find())
            return Long.parseLong(m.group(1));
        return System.currentTimeMillis();
    }



    public static String convertNumberToMonthText(int month){
        return new DateFormatSymbols().getShortMonths()[month-1];
    }

    public static String trimAfterLastOccurrenceOfSymbol(String text, String symbol){
        String newstr = text;
        if (null != text && text.length() > 0 )
        {
            int endIndex = text.lastIndexOf(symbol);
            if (endIndex != -1)
            {
                 newstr = text.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
            }
        }
        return newstr;
    }

    public static List<Integer> convertHexColorsToIntColors(List<String> colorsHex){
        List<Integer> colorsInt = new ArrayList<>();

        for (String colorHex : colorsHex){
            colorsInt.add(Color.parseColor(colorHex));
        }
        return colorsInt;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if(activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static String covertImageToBase64(Bitmap imageBitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

    }

    public static int getScreenWidthInDPs(Context context){
        DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int widthInDP = Math.round(dm.widthPixels / dm.density);
        return widthInDP;
    }

    public static int getScreenHeightInDPs(Context context){
        DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int heightInDP = Math.round(dm.heightPixels / dm.density);
        return heightInDP;
    }

    public static float convertPxToDp(float px){
        DisplayMetrics displaymetrics = Resources.getSystem().getDisplayMetrics();
        return TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, px, displaymetrics );

    }

    public static float convertDpToPixel(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static boolean isArabic(){
        return Locale.getDefault().getLanguage().equalsIgnoreCase("ar");
    }


    public static String getDurationString(int seconds) {
        Date date = new Date(seconds * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat(seconds >= 3600 ? "HH:mm:ss" : "mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

}
