package goldenauctions.com.CallBack;

import android.view.View;

import org.json.JSONException;

import java.io.FileNotFoundException;

public interface OnItemClickTagListener {
    void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException;}
