package goldenauctions.com.CallBack;

import android.view.View;

import org.json.JSONException;

import java.io.FileNotFoundException;

public interface OnItemClickListener {
    void onItemClick(View view, int position) throws JSONException, FileNotFoundException;}
