package goldenauctions.com.CallBack;

public interface InternetAvailableCallback {

	void onInternetAvailable(boolean isAvailable);
}
