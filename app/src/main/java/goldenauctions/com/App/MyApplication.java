package goldenauctions.com.App;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import es.dmoral.toasty.Toasty;
import goldenauctions.com.Hellper.TypefaceUtil;


public class MyApplication extends Application {
    private static MyApplication sInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/roboto_regular.ttf");
        sInstance = this;
        Toasty.Config.getInstance().allowQueue(true).apply();
    }
    public static MyApplication getInstance() {
        return MyApplication.sInstance;
    }
    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(this);
        super.attachBaseContext(base);
    }



}
