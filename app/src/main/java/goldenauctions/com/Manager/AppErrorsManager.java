package goldenauctions.com.Manager;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.InstallTwoStringCallback;
import goldenauctions.com.Hellper.NumberTextWatcher;
import goldenauctions.com.Medoles.Media;
import goldenauctions.com.R;
import goldenauctions.com.Ui.Activities.AuctionsDetialsActivity;
import goldenauctions.com.Ui.Activities.UserActionActivity;
import goldenauctions.com.Ui.Adapters.AdapterAuctionDetails;
import goldenauctions.com.Ui.Adapters.AdapterBankAccount;
import goldenauctions.com.Ui.Adapters.AdapterCategoryHome;
import goldenauctions.com.Ui.Adapters.AdapterMySubScriptions;


public class AppErrorsManager {


    public static void showErrorDialog(Context context, String error) {
        if (context != null) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.error))
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();


            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }


        }
    }


    public static void showErrorDialog(Activity context, String error, DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(R.string.error).setMessage(error).setPositiveButton(R.string.ok, okClickListener).show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showSuccessDialog(Activity context, String title, String error,
                                         DialogInterface.OnClickListener okClickListener,
                                         DialogInterface.OnClickListener cancelClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.confirm), okClickListener).setNegativeButton(context.getString(R.string.cancel), cancelClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }


        }
    }

    public static void showDescriptionDialog(Activity context, String title, String error,
                                             DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.close), okClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showSuccessDialog(Activity context, String error,
                                         DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.info))
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.ok), okClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showCustomErrorDialog(Activity context, String title, String error) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showTwoActionDialog(Activity context, String title, String error, String actionName, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_two_action_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView accept_dialog = dialoglayout.findViewById(R.id.accept_dialog);
        accept_dialog.setText(actionName);
        accept_dialog.setTextColor(context.getResources().getColor(R.color.white));

        accept_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showChangeStatusStoreDialog(Activity context, String title, String error, String actionName, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_two_action_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView accept_dialog = dialoglayout.findViewById(R.id.accept_dialog);
        accept_dialog.setText(actionName);
        accept_dialog.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        accept_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                    alertDialogBuilder.setCancelable(false);
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showCustomErrorDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);


        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showImage(Activity context, String imageUri) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_view_image_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        ImageView imageView = dialoglayout.findViewById(R.id.imageView);
        Glide.with(context).load(imageUri).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }
    public static void showImage(Activity context, List<Media> mediaList , int pos  ) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_view_image_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final int[] newpos = {pos};
        alertDialogBuilder.setView(dialoglayout);
        ImageView imageView = dialoglayout.findViewById(R.id.imageView);
        ImageView img_start = dialoglayout.findViewById(R.id.img_start);
        ImageView img_end = dialoglayout.findViewById(R.id.img_end);
        RelativeLayout layout_option = dialoglayout.findViewById(R.id.layout_option);
        layout_option.setVisibility(View.VISIBLE);
        img_start.setOnClickListener(view -> {
            if((mediaList.size() -1) >  newpos[0]){
                newpos[0] = newpos[0] +1 ;
                String imageUri  = mediaList.get(newpos[0]).getFile() ;
                Glide.with(context).load(imageUri).into(imageView);
            }else {
                newpos[0] = 0 ;
                String imageUri  = mediaList.get(newpos[0]).getFile() ;
                Glide.with(context).load(imageUri).into(imageView);
            }

        });
        img_end.setOnClickListener(view -> {

            if(newpos[0] > 0){
                newpos[0] = newpos[0] - 1 ;
                String imageUri  = mediaList.get(newpos[0]).getFile() ;
                Glide.with(context).load(imageUri).into(imageView);
            }else {
                newpos[0] = mediaList.size() -1 ;
                String imageUri  = mediaList.get(newpos[0]).getFile() ;
                Glide.with(context).load(imageUri).into(imageView);
            }
        });
        String imageUri  = mediaList.get(pos).getFile() ;
        Glide.with(context).load(imageUri).into(imageView);
    /**    imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });*/
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void InternetUnAvailableDialog(Activity context, String title, String error, InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_no_internet_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView retry_dialog = dialoglayout.findViewById(R.id.retry_dialog);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("close");
            }
        });

        retry_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("retry");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showAddTicketDialog(Activity context, InstallTwoStringCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_add_ticket_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        EditText edit_dialog_title = dialoglayout.findViewById(R.id.edit_dialog_title);
        EditText edit_dialog_body = dialoglayout.findViewById(R.id.edit_dialog_body);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_title.getText().toString().trim())) {
                    edit_dialog_title.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_title.requestFocus();
                }
                if (TextUtils.isEmpty(edit_dialog_body.getText().toString().trim())) {
                    edit_dialog_body.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_body.requestFocus();
                } else {
                    String title = edit_dialog_title.getText().toString().trim();
                    String body = edit_dialog_body.getText().toString().trim();
                    installCallback.onStatusDone(title, body);
                    alertDialogBuilder.dismiss();
                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showAddDocumentsDialog(Activity context, AdapterCategoryHome categoryHome, InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_add_document_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView toolbarNameTxt = dialoglayout.findViewById(R.id.toolbarNameTxt);
        RelativeLayout layout_back = dialoglayout.findViewById(R.id.end_main);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recyclerView);

        TextView btn_next = dialoglayout.findViewById(R.id.btn_next);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(context, 2);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(categoryHome);


        btn_next.setOnClickListener(view -> {
            installCallback.onStatusDone("yes");
            alertDialogBuilder.dismiss();
        });


        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();

            }
        });

        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setText(context.getResources().getString(R.string.AddNewDocument));
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    Window window = alertDialogBuilder.getWindow();
                    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showBanksListDialog(Activity context, AdapterBankAccount adapter, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_filter_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        TextView text_no = dialoglayout.findViewById(R.id.text_no);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        text_body_title_dialog.setText("" + context.getResources().getString(R.string.BankAccounts));
        text_no.setText("" + context.getResources().getString(R.string.Done));
        text_yes.setVisibility(View.GONE);
        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });


        text_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showLogOutsDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_logout_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView close_logout = dialoglayout.findViewById(R.id.close_logout);
        close_logout.setText(context.getResources().getText(R.string.Exit));

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });
        close_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Exit");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showCustomErrorDialogTop(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_notfication_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);


        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    WindowManager.LayoutParams wmlp = alertDialogBuilder.getWindow().getAttributes();

                    wmlp.gravity = Gravity.TOP;
                    wmlp.x = 0;   //x position
                    wmlp.y = 0;   //y position
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showFilterCodetDialog(Activity context, InstallTwoStringCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_code_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        EditText edit_dialog_title = dialoglayout.findViewById(R.id.edit_dialog_title);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_title.getText().toString().trim())) {
                    edit_dialog_title.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_title.requestFocus();
                } else {
                    String title = edit_dialog_title.getText().toString().trim();
                    installCallback.onStatusDone("code", title);
                    alertDialogBuilder.dismiss();
                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showFilterPricetDialog(Activity context, InstallTwoStringCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_price_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        EditText edit_dialog_from = dialoglayout.findViewById(R.id.edit_dialog_from);
        EditText edit_dialog_to = dialoglayout.findViewById(R.id.edit_dialog_to);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_from.getText().toString().trim())) {
                    edit_dialog_from.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_from.requestFocus();
                } else if (TextUtils.isEmpty(edit_dialog_to.getText().toString().trim())) {
                    edit_dialog_to.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_to.requestFocus();
                } else {

                    String from = edit_dialog_from.getText().toString().trim();
                    String to = edit_dialog_to.getText().toString().trim();
                    if (RootManager.IsTextIsDoble(context ,from) && RootManager.IsTextIsDoble(context ,to)) {
                        float from_f = Float.parseFloat(from);
                        float to_f = Float.parseFloat(to);
                        if (from_f >= to_f) {
                            edit_dialog_from.setError(context.getResources().getString(R.string.should_lower_price));
                            edit_dialog_from.requestFocus();
                        } else {
                            installCallback.onStatusDone(from, to);
                            alertDialogBuilder.dismiss();
                        }
                    }


                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showFilterDatetDialog(Activity context, InstallTwoStringCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_date_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView edit_dialog_from = dialoglayout.findViewById(R.id.edit_dialog_from);
        TextView edit_dialog_to = dialoglayout.findViewById(R.id.edit_dialog_to);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);


        Calendar myCalendar;
        Calendar myCalendar2;
        myCalendar = Calendar.getInstance();
        myCalendar2 = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date_from_picker, date_to_picker;



        date_from_picker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                String  date_from = sdf.format(myCalendar.getTime());
                edit_dialog_from.setText(date_from);



            }

        };

        date_to_picker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                String   date_to = sdf.format(myCalendar2.getTime());
                edit_dialog_to.setText(date_to);




            }

        };

        edit_dialog_from.setOnClickListener(view -> {
            DatePickerDialog datePickerDialog =
                    new DatePickerDialog(context, date_from_picker, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });



        edit_dialog_to.setOnClickListener(view -> {
            if (!edit_dialog_from.getText().toString().isEmpty()) {
                DatePickerDialog datePickerDialog_to =
                        new DatePickerDialog(context, date_to_picker, myCalendar2
                          .get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH),
                        myCalendar2.get(Calendar.DAY_OF_MONTH));
                datePickerDialog_to.getDatePicker().setMinDate(myCalendar.getTimeInMillis() + (1000 * 60 * 60 * 24));
                datePickerDialog_to.show();
            } else {
                Toast.makeText(context, "أختر اولا بدء التاريخ", Toast.LENGTH_SHORT).show();
            }
        });



        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_from.getText().toString().trim())) {
                    edit_dialog_from.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_from.requestFocus();
                } else if (TextUtils.isEmpty(edit_dialog_to.getText().toString().trim())) {
                    edit_dialog_to.setError(context.getResources().getString(R.string.required_field));
                    edit_dialog_to.requestFocus();
                } else {
                    String from = edit_dialog_from.getText().toString().trim();
                    String to = edit_dialog_to.getText().toString().trim();
                    installCallback.onStatusDone(from, to);
                    alertDialogBuilder.dismiss();


                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showMySubSubScriptionsDialog(Activity context, AdapterMySubScriptions adapter ) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_mysub_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        TextView text_no = dialoglayout.findViewById(R.id.text_no);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        text_body_title_dialog.setText("" + context.getResources().getString(R.string.MyPackages));
        text_no.setText("" + context.getResources().getString(R.string.Done));
        text_yes.setVisibility(View.GONE);
        text_no.setOnClickListener(view -> {
            alertDialogBuilder.dismiss();
        });

        text_no.setTextColor(context.getResources().getColor(R.color.colorBlack));
        text_body_title_dialog.setTextColor(context.getResources().getColor(R.color.colorBlack));


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    WindowManager.LayoutParams wmlp = alertDialogBuilder.getWindow().getAttributes();
                    wmlp.gravity = Gravity.BOTTOM;
                    wmlp.x = 0;   //x position
                    wmlp.y = 0;   //y position
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }


    }
    public static void showTermUseDialog(Activity context, String title, String error) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_term_use_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_term = dialoglayout.findViewById(R.id.text_term);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });
        text_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                Intent intent = new Intent(context, UserActionActivity.class);
                intent.putExtra(RootManager.ActionIntent, context.getResources().getString(R.string.Termsofuse));
                context.startActivity(intent);
            }
        });
        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showLoginToContinueDialog(Activity context, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_login_continue_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView accept_dialog = dialoglayout.findViewById(R.id.accept_dialog);

        accept_dialog.setTextColor(context.getResources().getColor(R.color.white));

        accept_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }
    public static void showAuctionsDetialsDialog(Activity context, AdapterAuctionDetails adapter ) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_mysub_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        TextView text_no = dialoglayout.findViewById(R.id.text_no);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        text_body_title_dialog.setText("" + context.getResources().getString(R.string.Details));
        text_no.setText("" + context.getResources().getString(R.string.close));
        text_yes.setVisibility(View.GONE);
        text_no.setOnClickListener(view -> {
            alertDialogBuilder.dismiss();
        });

        text_no.setTextColor(context.getResources().getColor(R.color.colorBlack));
        text_body_title_dialog.setTextColor(context.getResources().getColor(R.color.colorBlack));


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }


    }

}
