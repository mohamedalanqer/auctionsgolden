package goldenauctions.com.Manager;


import java.util.HashMap;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import goldenauctions.com.WebService.model.response.DocumentsResponse;
import goldenauctions.com.WebService.model.response.LoginResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetrofitManager {

    public void GetInstall(CallbackConncation Callback);

    public void createLogin(User user, CallbackConncation Callback);

    public void createRegistration(User user, CallbackConncation Callback);

    public void createRegistration(HashMap<String, RequestBody> map, MultipartBody.Part file, CallbackConncation Callback);


    public void UpdateAvatar(MultipartBody.Part image_File, CallbackConncation Callback);


    public void editLanguageProfile(String lang, CallbackConncation Callback);

    public void editProfile(HashMap<String, RequestBody> map, CallbackConncation Callback);

    public void ForgetPassword(String email, CallbackConncation Callback);

    public void ResetPassword(String email, String password, String password_confirmation, String code, CallbackConncation Callback);

    public void logout(CallbackConncation Callback);

    public void UpdateUserPassword(String old_password, String password, String password_confirmation, CallbackConncation Callback);

    public void GetProfile(CallbackConncation Callback);

    public void GetNotifications(int page, int per_page, CallbackConncation Callback);

    public void ReadAllNotifications(CallbackConncation Callback);


    public void Verify(int type, String code, CallbackConncation Callback);

    public void ResendVerify(int type, CallbackConncation Callback);

    public void CreateOrder(HashMap<String, RequestBody> map, CallbackConncation Callback);


    /**
     * public void RatingOrder(int order_id, int rate, String review, List<MultipartBody.Part> ProductReviewList, CallbackConncation Callback);
     * <p>
     * public void RatingOrder(int order_id, int rate, List<MultipartBody.Part> ProductReviewList, CallbackConncation Callback);
     */
    public void GetMyFaqs(int page, int per_page, CallbackConncation Callback);

    public void CreateSubscriptions(int subscription_id, int payment_type, CallbackConncation Callback);

    public void CreateSubscriptions(int subscription_id, int payment_type, MultipartBody.Part payment_document, CallbackConncation Callback);


    public void AddTickets(String title, String message, CallbackConncation Callback);

    public void GetListTickets(int page, int per_page, CallbackConncation Callback);

    public void GetShowTicket(int ticket_id, CallbackConncation Callback);


    public void GetAuctions(int page, int per_page , CallbackConncation Callback);
    public void GetAuctionsAdvertisement( int page , int pre_page , CallbackConncation Callback);



    public void GetAuctionsByCategory(int category_id,int page,  int per_page , CallbackConncation Callback);
    public void GetAuctionsByCode(String code,int page,  int per_page , CallbackConncation Callback);
    public void GetAuctionsByPrice(String StartPrice , String EndPrice,int page,  int per_page , CallbackConncation Callback);
    public void GetAuctionsByDate(String StartDate , String EndDate,int page,  int per_page , CallbackConncation Callback);


    public void GetAuctionsByCode(int category_id,String code,int page,  int per_page , CallbackConncation Callback);
    public void GetAuctionsByPrice(int category_id,String StartPrice , String EndPrice,int page,  int per_page , CallbackConncation Callback);
    public void GetAuctionsByDate(int category_id,String StartDate , String EndDate,int page,  int per_page , CallbackConncation Callback);




    public void GetAuctionById( int auction_id , CallbackConncation Callback);
    public void GetFollowUpAuctions(  int page, int per_page , CallbackConncation Callback);
    public void GetMyPurchases( boolean my_purchases,   int page, int per_page, CallbackConncation Callback);

    public void  GetMyBids( boolean my_bids,  int page,  int per_page, CallbackConncation Callback);


    public void  AddNewBid( int auction_id,  String price  , CallbackConncation Callback);
    public void FavouriteAuctionsToggle( int auction_id , CallbackConncation Callback);




    public void GetMyDocuments(int page, int per_page , CallbackConncation Callback);


    public void UploadNewDocument(String expiry_date ,int document_type_id, MultipartBody.Part front_face, MultipartBody.Part back_face , CallbackConncation Callback);

    public void  AddResponseTickets( int title,  String message , CallbackConncation Callback);
    public void  RequestRefund( CallbackConncation Callback);


}
