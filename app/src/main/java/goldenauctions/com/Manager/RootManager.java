package goldenauctions.com.Manager;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import goldenauctions.com.R;


public class RootManager {

   public static final String URL_Base = "https://app.golden-auctions.com/api/";
    public static final String URL_INSTALL = "https://app.golden-auctions.com/api/";
    public static final String IMAGE_URL = "https://app.golden-auctions.com/";

    /**  public static final String URL_Base = "https://golden-auctions.com/api/";
    public static final String URL_INSTALL = "https://golden-auctions.com/api/";
    public static final String IMAGE_URL = "https://golden-auctions.com/";*/
    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public static final String DeviceType = "android";
    public static final String UserType = "user_type";


    public static final int WaitingStart = 0;
    public static final int BidingTime = 1;
    public static final int WaitingSupplierApproval = 2;
    public static final int WaitingCustomerPayment = 3;
    public static final int Finished = 4;
    public static final int SupplierRejected = 5;
    public static final int CustomerCanceled = 6;



    public static final int NotificationTypeGeneral = 1;
    public static final int NotificationTypeOrder = 2;
    public static final int NotificationTypeTiket = 3;
    public static final long DURATUIN_TRANSITION = 300;
    public static final long DURATUIN_TRANSITION_BACK = 200;


    public static int  payment_type_Cash = 2;
    public static int payment_type_Bank = 1;


    public static final String ROOM_ID = "RoomID";
    public static final String ROOM_ISEnable = "Room_IsEnable";
    public static final String ROOM_IMAGE = "RoomImage";
    public static final String ROOM_NAME = "RoomName";
    public static final String ROOM_TITLE = "RoomTitle";

    public static int Status_Ads_Pending = 0;
    public static int Status_Ads_Active = 1;
    public static int Status_Ads_NotActive = 2;


    public static int Type_Ads_Product = 1;
    public static int Type_Ads_Store = 2;
    public static int Type_Ads_Category = 3;



    public static int Status_NewOrder = 0;
    public static int Status_AcceptOrder = 1;
    public static int Status_Rejected = 2;
    //public static int Status_Processing = 3;
    public static int Status_Completed = 3;
    public static int Status_Cancel = 4;


    public static final String Step_Create_1 = "step_1";
    public static final String Step_Create_2 = "step_2";
    public static String view_steps_select = Step_Create_1;

    public static final int verifyAccount = 3;
    public static final int VerificationTypeMobile = 2;
    public static final int VerificationTypeEmail = 1;


    public static final String IsShowSercivceM = "IsShowSercivceM";
    public static final String IsShowSubscriptionM = "IsShowSubscriptionM";
    public static final String IsHowGuideSwipe = "IsHowGuideSwipe";

    public static final String KeyGuide = "key_guide";
    public static final String ActionIntent = "actionIntent";
    public static final String ActionEditProfile = "edit_profile";
    public static final String ActionChangePassword = "change_password";
    public static final String ActionPrivacyPolicy = "privacy_policy";
    public static final String ActionAboutUs = "about_us";
    public static final String ActionForgetPassword = "forget_password";

    public static final String ActionMyPoints = "my_points";
    public static final String ActionMyExpectations = "my_expectations";
    public static final String ActionMysubscriptions = "my_subscriptions";
    public static final String ActionKeyLocalBroadcastManager = "com.shwbyklwbyk_FCM-MESSAGE";


    public static final String Stats_uncertain = "Uncertain";
    public static final String Stats_certain = "Certain";
    public static final String Stats_ready = "Ready";
    public static final String Stats_updated = "Updated";
    public static final String Stats_recevied = "Recevied";

    public static final String Transaction_Deposit = "Deposit";
    public static final String Transaction_Withdraw = "Withdraw";
    public static final String Transaction_Archives = "Archives";


    public static final String PaymentType_cash = "Cash";
    public static final String PaymentType_visa = "Visa";
    public static final String PaymentType_wallet = "Wallet";


    public static final String DeliveryType_Cash = "Cash";
    public static final String DeliveryType_OnHand = "OnHand";


    public static final String Info_Tag_Privacy = "privacy";
    public static final String Info_Tag_About = "about";
    public static final String Info_Tag_Branch = "branch";
    public static final String Info_Tag_Contact = "contact";


    public static final int TransactionStatusesPending = 1;
    public static final int TransactionStatusesPaid = 2;

    public static final String CHATVOICEKEY = "::VOICE::" ;
    public static final String CHATIMAGEKEY = "::IMAGE::" ;




    public final static int RESPONSE_CODE_OK = 200;
    public final static String RESPONSE_STATUS_OK = "OK";
    public final static int RESPONSE_CODE_CREATED = 201;
    public final static String RESPONSE_STATUS_CREATED = "Created";
    public final static int RESPONSE_CODE_NO_CONTENT = 204;
    public final static String RESPONSE_STATUS_NO_CONTENT = "No Content";
    public final static int RESPONSE_CODE_BAD_REQUEST = 400;
    public final static String RESPONSE_STATUS_BAD_REQUEST = "Bad Request";
    public final static int RESPONSE_CODE_UNAUTHORIZED = 401;
    public final static String RESPONSE_STATUS_UNAUTHORIZED = "Unauthorized";
    public final static int RESPONSE_CODE_FORBIDDEN = 403;
    public final static String RESPONSE_STATUS_FORBIDDEN = "Forbidden";
    public final static int RESPONSE_CODE_NOT_FOUND = 404;
    public final static String RESPONSE_STATUS_NOT_FOUND = "Not Found";
    public final static int RESPONSE_CODE_CONFLICT = 409;
    public final static String RESPONSE_STATUS_CONFLICT = "Conflict";
    public final static int RESPONSE_CODE_UNPROCESSABLE_ENTITY = 422;
    public final static String RESPONSE_STATUS_UNPROCESSABLE_ENTITY = "Unprocessable Entity";

    public final static int RESPONSE_ORDER_STATUS_PENDING = 0;
    public final static int RESPONSE_ORDER_STATUS_ACCEPTED = 1;
    public final static int RESPONSE_ORDER_STATUS_START_TRIP = 2;
    public final static int RESPONSE_ORDER_STATUS_END_TRIP = 3;
    public final static int RESPONSE_ORDER_STATUS_DECLINED = 4;

    public static void SetImageYoutube(Context context, String path, ImageView imageView) {
        if (!TextUtils.isEmpty(path)) {
            String regex = "v=([^\\s&#]*)";
            Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(path);

            if (matcher.find()) {
                System.out.println(matcher.group(1));
                String url = "https://img.youtube.com/vi/" + matcher.group(1) + "/0.jpg";
                Glide.with(context).load(url).into(imageView);
            }
        }
    }

    public static String GetImageYoutube(Context context, String path) {
        if (!TextUtils.isEmpty(path)) {
            String regex = "v=([^\\s&#]*)";
            Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(path);

            if (matcher.find()) {
                System.out.println(matcher.group(1));
                String url = "https://img.youtube.com/vi/" + matcher.group(1) + "/0.jpg";
                return url;
            } else
                return null;
        }
        return null;
    }

    public static String GetIDYoutube(String path) {
        if (!TextUtils.isEmpty(path)) {
            String regex = "v=([^\\s&#]*)";
            Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(path);
            if (matcher.find()) {
                return matcher.group(1);

            }
        }
        return null;
    }

    static public void shareImageProfile(String url, String message, final Context context) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Picasso.get().load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.putExtra(Intent.EXTRA_TEXT, " " +
                        context.getString(R.string.app_name) + " " + message + " " +
                        " https://play.google.com/store/apps/details?id=goldenauctions.com ");
                i.putExtra(Intent.EXTRA_STREAM, RootManager.getLocalBitmapUri(bitmap, context));
                i.setType("image/*");
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }


            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });

    }

    private static Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void ClearErrorOnView(List<View> view) {
        for (int i = 0; i < view.size(); i++) {
            view.get(i).clearFocus();
            if (view instanceof EditText) {
                ((EditText) view).setError(null);
            } else if (view instanceof TextView) {
                ((TextView) view).setError(null);
            }
        }
    }

    public static boolean IsTextIsDoble(Context context ,String str){
        str = str.replace(context.getResources().getString(R.string.dollar_code),"");
        str = str.replace(",","");
        //check if float
        try{
            Double.parseDouble(str);
            return true ;
        }catch(NumberFormatException e){
            //not float
        }

        return false ;
    }

    public static final boolean GetStatusById(Context context, int tagName,  TextView text_status) {
        boolean IsEnabel = true;
        if (tagName == RootManager.WaitingStart) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses0));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus0));
            IsEnabel = false ;
        } else if (tagName == RootManager.BidingTime) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses1));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus1));
            IsEnabel = true ;
        } else if (tagName == RootManager.WaitingSupplierApproval) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses2));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus2));
            IsEnabel = false ;
        }else if (tagName == RootManager.WaitingCustomerPayment) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses3));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus3));
            IsEnabel = false ;
        }else if (tagName == RootManager.Finished) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses4));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus4));
            IsEnabel = false ;
        }else if (tagName == RootManager.SupplierRejected) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses5));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus5));
            IsEnabel = false ;
        }else if (tagName == RootManager.CustomerCanceled) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses6));
            text_status.setTextColor(ContextCompat.getColor(context, R.color.colorstatus6));
            IsEnabel = false ;
        }
        return IsEnabel;
    }
    public static final void SetMessageStatusById(Context context, int tagName,  TextView text_status) {

        if (tagName == RootManager.WaitingStart) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses0));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorstatus0));
            text_status.setVisibility(View.GONE);
        } else if (tagName == RootManager.BidingTime) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses1));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorstatus1));
            text_status.setVisibility(View.GONE);

        } else if (tagName == RootManager.WaitingSupplierApproval) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses2));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorstatus0));
            text_status.setVisibility(View.VISIBLE);

        }else if (tagName == RootManager.WaitingCustomerPayment) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses3));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorstatus3));
            text_status.setVisibility(View.VISIBLE);

        }else if (tagName == RootManager.Finished) {
            text_status.setText(context.getResources().getString(R.string.YourWin));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.greenDark));
            text_status.setVisibility(View.VISIBLE);

        }else if (tagName == RootManager.SupplierRejected) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses5));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorstatus5));
            text_status.setVisibility(View.VISIBLE);

        }else if (tagName == RootManager.CustomerCanceled) {
            text_status.setText(context.getResources().getString(R.string.AuctionStatuses6));
            text_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorstatus6));
            text_status.setVisibility(View.VISIBLE);

        }

    }

    public static String ConncationSocial(String url, String UrlRoot) {
        if (url.contains(UrlRoot)) {
            url = url.replace(UrlRoot, "");
        }
        if(url.contains("www.")){
            url = url.replace("www.", "");
        }
        if(url.contains("https://")){
            url =  url.replace("https://", "");
        }
        if(url.contains("http://")){
            url =   url.replace("http://", "");
        }

        Log.e("ConncationSocial","https://"+UrlRoot+url);
        return "https://"+UrlRoot+url;
    }

    public static Transition enterTransition() {
        ChangeBounds bounds = new ChangeBounds();
        bounds.setDuration(RootManager.DURATUIN_TRANSITION);

        return bounds;
    }

    public static Transition returnTransition() {
        ChangeBounds bounds = new ChangeBounds();
        bounds.setInterpolator(new DecelerateInterpolator());
        bounds.setDuration(RootManager.DURATUIN_TRANSITION_BACK);

        return bounds;
    }
}
