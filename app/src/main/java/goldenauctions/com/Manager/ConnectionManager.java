package goldenauctions.com.Manager;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;


import androidx.appcompat.app.AlertDialog;

import java.util.HashMap;
import java.util.List;

import goldenauctions.com.CallBack.CallbackConncation;
import goldenauctions.com.CallBack.InstallCallback;
import goldenauctions.com.CallBack.InternetAvailableCallback;
import goldenauctions.com.Hellper.InternetConnectionUtils;
import goldenauctions.com.Medoles.Install.User;
import goldenauctions.com.R;
import goldenauctions.com.WebService.RetrofitWebService;
import goldenauctions.com.WebService.model.response.AuctionResponse;
import goldenauctions.com.WebService.model.response.AuctionsResponse;
import goldenauctions.com.WebService.model.response.DocumentResponse;
import goldenauctions.com.WebService.model.response.DocumentsResponse;
import goldenauctions.com.WebService.model.response.FaqsResponse;
import goldenauctions.com.WebService.model.response.InstallResponse;
import goldenauctions.com.WebService.model.response.LoginResponse;
import goldenauctions.com.WebService.model.response.NotificationsResponse;
import goldenauctions.com.WebService.model.response.RootResponse;
import goldenauctions.com.WebService.model.response.TicketResponse;
import goldenauctions.com.WebService.model.response.TicketsResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectionManager implements RetrofitManager {
    private FrameLayout layout_loading;
    private Activity mContext;
    private Handler handler;
    private String StatusSuccess = "success";
    private String StausFail = "fail";
    private String StausError = "error";

    public ConnectionManager(Activity mContext) {
        this.mContext = mContext;
        this.layout_loading = mContext.findViewById(R.id.layout_loading);
        this.layout_loading.setOnClickListener(view -> {
            return;
        });
        handler = new Handler(Looper.getMainLooper());
    }

    private void OpenDialogError(String message) {
        AppErrorsManager.showCustomErrorDialog(mContext, mContext.getResources().getString(R.string.error), message);

    }

    private void OpenDialogErrorAction(String message, CallbackConncation Callback) {
        AppErrorsManager.showTwoActionDialog(mContext, mContext.getResources().getString(R.string.error), message, mContext.getResources().getString(R.string.retry)
                , new InstallCallback() {
                    @Override
                    public void onStatusDone(String status) {
                        GetInstall(Callback);

                    }
                });
    }


    private void InternetUnAvailableDialog(String message, InstallCallback installCallback) {
        AppErrorsManager.InternetUnAvailableDialog(mContext, mContext.getResources().getString(R.string.error), message, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                layout_loading.setVisibility(View.GONE);
                if (TextUtils.equals("retry", status))
                    installCallback.onStatusDone(status);
            }
        });

    }

    private void getResponse(Response response, CallbackConncation Callback) {
        Response<RootResponse> responseResponse = response;
        Log.e("response", response.toString());
        if (RootManager.RESPONSE_CODE_OK == response.code()) {
            if (response.body() != null) {
                Log.e("response", responseResponse.body().getStatus().getStatus());
                if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), StatusSuccess)) {
                    Callback.onResponse(response);
                } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), StausFail)) {
                    OpenDialogError(responseResponse.body().getStatus().getMessageList());
                } else if (TextUtils.equals(responseResponse.body().getStatus().getStatus(), StausError)) {
                    OpenDialogError(responseResponse.body().getStatus().getMessageList());
                }
            } else {
                OpenDialogError(mContext.getResources().getString(R.string.InternalServerError));
            }
            layout_loading.setVisibility(View.GONE);
        } else {
            OpenDialogError(mContext.getResources().getString(R.string.InternalServerError));

        }
    }


    @Override
    public void GetInstall(CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetInstall().enqueue(new Callback<InstallResponse>() {
                        @Override
                        public void onResponse(Call<InstallResponse> call, Response<InstallResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                                        if (response.body() != null) {
                                            Log.e("response", response.body().toString());
                                            if (TextUtils.equals(response.body().getStatus().getStatus(), StatusSuccess)) {
                                                Callback.onResponse(response);
                                            } else if (TextUtils.equals(response.body().getStatus().getStatus(), StausFail)) {
                                                OpenDialogErrorAction(response.body().getStatus().getMessageList(), Callback);
                                            } else if (TextUtils.equals(response.body().getStatus().getStatus(), StausError)) {
                                                OpenDialogError(response.body().getStatus().getMessageList());
                                            }
                                        } else {
                                            OpenDialogErrorAction(mContext.getResources().getString(R.string.InternalServerError), Callback);
                                        }
                                    } else {
                                        OpenDialogErrorAction(mContext.getResources().getString(R.string.InternalServerError), Callback);

                                    }
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<InstallResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogErrorAction(t.getMessage() + "", Callback);

                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetInstall(Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetProfile(CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetProfile().enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetProfile(Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });


    }

    @Override
    public void createLogin(User user, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).createLogin(user).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    createLogin(user, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }


    @Override
    public void createRegistration(User user, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).createRegistration(user).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    createRegistration(user, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void createRegistration(HashMap<String, RequestBody> map, MultipartBody.Part file, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).createRegistration(map, file).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    createRegistration(map, file, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void UpdateAvatar(MultipartBody.Part image_File, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplication().getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                Log.e("isAvailable", isAvailable + "");
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).UpdateAvatar(image_File).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    UpdateAvatar(image_File, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });

    }


    @Override
    public void editLanguageProfile(String lang, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext, new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).editLanguageProfile(lang).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            Log.e("response", response.toString());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(null);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    editLanguageProfile(lang, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }


    @Override
    public void editProfile(HashMap<String, RequestBody> map, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).editProfile(map).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    editProfile(map, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });

    }

    @Override
    public void ForgetPassword(String email, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).ForgetPassword(email).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    ForgetPassword(email, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });


    }

    @Override
    public void ResetPassword(String email, String password, String
            password_confirmation, String code, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).ResetPassword(email, password, password_confirmation, code).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    ResetPassword(email, password, password_confirmation, code, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void logout(CallbackConncation Callback) {
        RetrofitWebService.getService(mContext).logout().enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.e("resp_logout", response.toString() + "");
                Callback.onResponse(response);
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("resp_logout", t.toString() + "");
                Callback.onResponse(null);
                layout_loading.setVisibility(View.GONE);

            }
        });

    }

    @Override
    public void UpdateUserPassword(String old_password, String password, String
            password_confirmation, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).UpdateUserPassword(old_password, password, password_confirmation).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    UpdateUserPassword(old_password, password, password_confirmation, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });

    }

    @Override
    public void GetNotifications(int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetNotifications(page, per_page).enqueue(new Callback<NotificationsResponse>() {
                        @Override
                        public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetNotifications(page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void ReadAllNotifications(CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).ReadAllNotifications().enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    ReadAllNotifications(Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });


    }

    @Override
    public void Verify(int type, String code, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).Verify(type, code).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    Verify(type, code, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });


    }

    @Override
    public void ResendVerify(int type, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext, new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).ResendVerify(type).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            Log.e("response", response.toString());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                    // Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    ResendVerify(type, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });

    }


    @Override
    public void CreateOrder(HashMap<String, RequestBody> map, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).CreateOrder(map).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    CreateOrder(map, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }


    @Override
    public void GetMyFaqs(int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetMyFaqs().enqueue(new Callback<FaqsResponse>() {
                        @Override
                        public void onResponse(Call<FaqsResponse> call, Response<FaqsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<FaqsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetMyFaqs(page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }


    @Override
    public void CreateSubscriptions(int subscription_id, int payment_type, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).CreateSubscriptions(subscription_id, payment_type).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            Log.e("response", response.toString());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    CreateSubscriptions(subscription_id, payment_type, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void CreateSubscriptions(int subscription_id, int payment_type, MultipartBody.Part payment_document, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).CreateSubscriptions(subscription_id, payment_type, payment_document).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            Log.e("response", response.toString());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    CreateSubscriptions(subscription_id, payment_type, payment_document, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }


    @Override
    public void AddTickets(String title, String message, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).AddTickets(title, message).enqueue(new Callback<RootResponse>() {
                        @Override
                        public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                            Log.e("response", response.toString());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<RootResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    AddTickets(title, message, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetListTickets(int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetListTickets(page, per_page).enqueue(new Callback<TicketsResponse>() {
                        @Override
                        public void onResponse(Call<TicketsResponse> call, Response<TicketsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<TicketsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetListTickets(page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetShowTicket(int ticket_id, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext, new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetShowTicket(ticket_id).enqueue(new Callback<TicketResponse>() {
                        @Override
                        public void onResponse(Call<TicketResponse> call, Response<TicketResponse> response) {
                            Log.e("response", response.toString());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<TicketResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetShowTicket(ticket_id, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }


    @Override
    public void GetAuctions(int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctions(page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                    // getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctions(page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsAdvertisement(int page , int pre_page ,  CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsAdvertisement(true ,page , pre_page  ).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                    // getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsAdvertisement(page , pre_page ,Callback );
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsByCategory(int category_id, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByCategory(category_id, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByCategory(category_id, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsByCode(String code, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByCode(code, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByCode(code, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsByPrice(String StartPrice, String EndPrice, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByPrice(StartPrice, EndPrice, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByPrice(StartPrice, EndPrice, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsByDate(String StartDate, String EndDate, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByDate(StartDate, EndDate, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByDate(StartDate, EndDate, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }




    @Override
    public void GetAuctionsByCode(int category_id,String code, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByCode(category_id ,code, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByCode(category_id ,code, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsByPrice(int category_id,String StartPrice, String EndPrice, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByPrice(category_id ,StartPrice, EndPrice, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByPrice(category_id ,StartPrice, EndPrice, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetAuctionsByDate(int category_id,String StartDate, String EndDate, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionsByDate(category_id ,StartDate, EndDate, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    //   getResponse(response, Callback);
                                    Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionsByDate(category_id ,StartDate, EndDate, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }















    @Override
    public void GetAuctionById(int auction_id, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetAuctionById(auction_id).enqueue(new Callback<AuctionResponse>() {
                        @Override
                        public void onResponse(Call<AuctionResponse> call, Response<AuctionResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    getResponse(response, Callback);
                                    // Callback.onResponse(response);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetAuctionById(auction_id, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void FavouriteAuctionsToggle(int auction_id, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(mContext).FavouriteAuctionsToggle(auction_id).enqueue(new Callback<AuctionResponse>() {
            @Override
            public void onResponse(Call<AuctionResponse> call, Response<AuctionResponse> response) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getResponse(response, Callback);

                    }
                });
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<AuctionResponse> call, Throwable t) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OpenDialogError(t.getMessage() + "");
                        layout_loading.setVisibility(View.GONE);
                    }
                });
            }
        });


    }


    @Override
    public void GetFollowUpAuctions(int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetFollowUpAuctions(page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                    // getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetFollowUpAuctions(page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetMyPurchases(boolean my_purchases, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetMyPurchases(my_purchases, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                    // getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetMyPurchases(my_purchases, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void GetMyBids(boolean my_bids, int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetMyBids(my_bids, page, per_page).enqueue(new Callback<AuctionsResponse>() {
                        @Override
                        public void onResponse(Call<AuctionsResponse> call, Response<AuctionsResponse> response) {
                            Log.d("GetMyBids" , my_bids + " "+response.toString() );
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                    // getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<AuctionsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetMyBids(my_bids, page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });

    }

    @Override
    public void AddNewBid(int auction_id, String price, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(mContext).AddNewBid(auction_id, price).enqueue(new Callback<AuctionResponse>() {
            @Override
            public void onResponse(Call<AuctionResponse> call, Response<AuctionResponse> response) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getResponse(response, Callback);

                    }
                });
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<AuctionResponse> call, Throwable t) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OpenDialogError(t.getMessage() + "");
                        layout_loading.setVisibility(View.GONE);
                    }
                });
            }
        });


    }

    @Override
    public void GetMyDocuments(int page, int per_page, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        InternetConnectionUtils.isInternetAvailable(mContext.getApplicationContext(), new InternetAvailableCallback() {
            @Override
            public void onInternetAvailable(boolean isAvailable) {
                if (isAvailable) {
                    RetrofitWebService.getService(mContext).GetMyDocuments(page, per_page).enqueue(new Callback<DocumentsResponse>() {
                        @Override
                        public void onResponse(Call<DocumentsResponse> call, Response<DocumentsResponse> response) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Callback.onResponse(response);
                                    // getResponse(response, Callback);
                                }
                            });
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<DocumentsResponse> call, Throwable t) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    OpenDialogError(t.getMessage() + "");
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InternetUnAvailableDialog(mContext.getString(R.string.no_internet_connection), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    GetMyDocuments(page, per_page, Callback);
                                    layout_loading.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void UploadNewDocument(String expiry_date, int document_type_id, MultipartBody.Part front_face, MultipartBody.Part back_face, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(mContext).UploadNewDocument(expiry_date, document_type_id, front_face, back_face).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getResponse(response, Callback);

                    }
                });
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OpenDialogError(t.getMessage() + "");
                        layout_loading.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    @Override
    public void AddResponseTickets(int title, String message, CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(mContext).AddResponseTickets(title, message).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getResponse(response, Callback);

                    }
                });
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OpenDialogError(t.getMessage() + "");
                        layout_loading.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    @Override
    public void RequestRefund(CallbackConncation Callback) {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(mContext).RequestRefund().enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getResponse(response, Callback);

                    }
                });
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        OpenDialogError(t.getMessage() + "");
                        layout_loading.setVisibility(View.GONE);
                    }
                });
            }
        });

    }
}
